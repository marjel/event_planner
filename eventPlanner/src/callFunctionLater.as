package
{
import pl.pkpik.epdp7.eventtimeline.util.CallLaterProxy;

    public function callFunctionLater( action:*=null ):void
    {
        var clp:CallLaterProxy = CallLaterProxy.GetInstance();
        (action != null) ? clp._callFunctionLater(action) : clp.removeAllActions();
    }
}
package pl.pkpik.epdp7.eventtimeline.model.proxy
{
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	import pl.pkpik.epdp7.eventtimeline.model.vo.ResourceVO;
	
	public class ResourcesMonitorProxy extends Proxy implements IProxy
	{
		public static const NAME:String = "ResourcesMonitorProxy";

		public static const LOADING_STEP:String	= NAME + "loadingStep";			
		public static const LOADING_COMPLETE:String	= NAME + "loadingComplete";		
		
		private var resourceList:Array;
		private var loadedReources:int = 0;		
		
		
		
		public function ResourcesMonitorProxy(data:Object=null)
		{
			super(NAME, data);
			resourceList = new Array();
		}
		public function addResource( proxyName:String, blockChain:Boolean = false ):void
		{
			resourceList.push( new ResourceVO( proxyName, blockChain ) );
		}

		public function loadResources():void
		{
			
			for( var i:int = 0; i < resourceList.length; i++){
				var r:ResourceVO = resourceList[i] as ResourceVO;
				if ( !r.loaded ){
					var proxy:* = facade.retrieveProxy( r.proxyName ) as Proxy;
					proxy.load();
					if ( r.blockChain ){
						break;
					}
				}
			}
		}

		public function resourceComplete( proxyName:String ):void
		{
			var l:uint = resourceList.length;
			for( var i:int = 0; i < l; i++){
				var r:ResourceVO = resourceList[i] as ResourceVO;
				if ( r.proxyName == proxyName ){
					r.loaded = true;
					loadedReources++;
					sendNotification( ResourcesMonitorProxy.LOADING_STEP, (loadedReources * 100) / resourceList.length );
					if ( !checkResources() && r.blockChain ) {
						loadResources();
					}
					break;
				}
			}
			
		}
		private function checkResources():Boolean
		{
			
			for( var i:int = 0; i < resourceList.length; i++){
				var r:ResourceVO = resourceList[i] as ResourceVO;
				if ( !r.loaded ){
					return false;
				}
			}
			sendNotification( ResourcesMonitorProxy.LOADING_COMPLETE );
			return true;
		}
	}
}
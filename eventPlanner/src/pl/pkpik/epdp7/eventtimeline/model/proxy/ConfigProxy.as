package pl.pkpik.epdp7.eventtimeline.model.proxy
{
	
	import mx.rpc.IResponder;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	import pl.pkpik.epdp7.eventtimeline.model.delgate.LoadXMLDelegate;
	import pl.pkpik.epdp7.eventtimeline.model.helper.ConfigVOParser;
	import pl.pkpik.epdp7.eventtimeline.model.vo.ConfigVO;

	

	public class ConfigProxy extends Proxy implements IProxy, IResponder
	{
		public static const NAME:String = "ConfigProxy";									// Proxy name
		public static const SEPARATOR:String = "/";
		
		public static const LOAD_SUCCESSFUL:String 	= NAME + "loadSuccessful";				// Successful notification
		public static const LOAD_FAILED:String 		= NAME + "loadFailed";					// Failed notification
		
		public static const ERROR_LOAD_FILE:String	= "Could Not Load the Config File!";	// Error message
		
		private var resourcesMonitorProxy:ResourcesMonitorProxy;							// StartupMonitorProxy instance
		
		public static const FILE_PATH:String = "assets/xml/config.xml";
		
		public function ConfigProxy ( data:Object = null ) 
		{
			super ( NAME, data );
		}
		
		override public function onRegister():void
		{		
			resourcesMonitorProxy = facade.retrieveProxy( ResourcesMonitorProxy.NAME ) as ResourcesMonitorProxy;
			resourcesMonitorProxy.addResource( ConfigProxy.NAME, true );
			setData( new ConfigVO() );			
		}
		
		public function load():void
		{
			var delegate : LoadXMLDelegate = new LoadXMLDelegate(this, FILE_PATH);
			delegate.load();
			
			
		}
		public function result( rpcEvent : Object ) : void
		{

			ConfigVOParser.parse(configVO, XML(rpcEvent.result).param);
			resourcesMonitorProxy.resourceComplete( ConfigProxy.NAME );
			sendNotification( ConfigProxy.LOAD_SUCCESSFUL );
		}

		public function fault( rpcEvent : Object ) : void 
		{

			sendNotification( ConfigProxy.LOAD_FAILED, ConfigProxy.ERROR_LOAD_FILE );
		}
		
		public function get configVO():ConfigVO
		{
			return data as ConfigVO;
		}

		public function getValue(key:String):String
		{
			return data[key];
		}
		
		public function getInt(key:String):int
		{
			return int( data[key] );
		}

		public function getNumber(key:String):Number
		{
			return Number( data[key] );
		}

		public function getBoolean(key:String):Boolean
		{
			return data[key] ? data[key].toLowerCase() == "true" : false;
		}

		public function setDefaultValue( key:String, value:Object ):void
		{
			if ( !data[key] )
			{
				data[key] = value;
			}
		}
	}
}
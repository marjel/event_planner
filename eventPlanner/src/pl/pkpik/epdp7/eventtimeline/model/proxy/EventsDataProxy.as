package pl.pkpik.epdp7.eventtimeline.model.proxy
{
    import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.patterns.proxy.Proxy;

    import pl.pkpik.epdp7.eventtimeline.model.vo.DayOffVO;

	import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;
	import pl.pkpik.epdp7.eventtimeline.model.vo.FreeDayVO;
	import pl.pkpik.epdp7.eventtimeline.model.vo.PlannerSolutionInfoVO;
	import pl.pkpik.epdp7.eventtimeline.model.vo.PlannerSolutionVO;
    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftAssignmentVO;
    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftDateVO;
	import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftVO;
	
	public class EventsDataProxy extends Proxy
	{

		public static const NAME:String = "EventsDataProxy";
		
		public function EventsDataProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		public function get eventsData():PlannerSolutionVO
		{
			return this.getData() as PlannerSolutionVO;
		}
		
		public function get solutionInfo():PlannerSolutionInfoVO
		{
			return eventsData.plannerSolutionInfo;
		}

        public function getShiftAssignmentByShiftID(id:uint, byShifAssignmentId:Boolean=false):ShiftAssignmentVO
        {
            var shift:ShiftAssignmentVO;
            var shifts:VectorCollection = eventsData.shiftAssignments;
            for each(shift in shifts){
                if(shift.shiftReference == id){
                    return shift;
                }
            }
            if(byShifAssignmentId){
                for each(shift in shifts){
                    if(shift.id == id){
                        return shift;
                    }
                }
            }
            return null;
        }

        public function getShiftAssignmentByID(id:uint):ShiftAssignmentVO
        {
            var shift:ShiftAssignmentVO;
            var shifts:VectorCollection = eventsData.shiftAssignments;
            for each(shift in shifts){
                if(shift.id == id){
                    return shift;
                }
            }
            return null;
        }
		
		public function getShiftVOById(id:uint):ShiftVO
		{
			var shift:ShiftVO;
			var shifts:VectorCollection = eventsData.shifts;
			for each(shift in shifts){
				if(shift.id == id){
					return shift;
				}
			}
			return null;
		}

        public function getShiftDateVOByDate(value:Date):ShiftDateVO
        {

            var shiftDate:ShiftDateVO;
            var shiftDates:VectorCollection = eventsData.shiftDates;
            for each(shiftDate in shiftDates){
                if((shiftDate.date.getFullYear() == value.getFullYear()) && (shiftDate.date.getMonth() == value.getMonth())
                        && (shiftDate.date.getDate() == value.getDate())){
                    return shiftDate;
                }
            }
            return null;
        }
		
		public function getShiftDateVOById(id:int):ShiftDateVO
		{
			var shiftDate:ShiftDateVO;
			var shiftDates:VectorCollection = eventsData.shiftDates;
			for each(shiftDate in shiftDates){
				if(shiftDate.id == id){
					return shiftDate;
				}
			}
			return null;
		}

		
		public function getEmployeeVOByIndex(value:int):EmployeeVO
		{
			var employee:EmployeeVO;
			var employees:VectorCollection = eventsData.employees;
			for each(employee in employees){
				if(employee.index == value){
					return employee;
				}
			}
			return null;
		}

        public function getEmployeeVOByReferenceId(value:int):EmployeeVO
        {
            var employee:EmployeeVO;
            var employees:VectorCollection = eventsData.employees;
            for each(employee in employees){
                if(employee.referenceId == value){
                    return employee;
                }
            }
            return null;
        }
		
		public function getEmployeeVOById(value:int):EmployeeVO
		{
			var employee:EmployeeVO;
			var employees:VectorCollection = eventsData.employees;
			for each(employee in employees){
				if(employee.id == value){
					return employee;
				}
			}
			return null;
		}
		
		public function getFreeDayVOById(value:int):FreeDayVO
		{
			var freeDayVO:FreeDayVO;
			var employee:EmployeeVO;
			var freeDays:VectorCollection;
			var employees:VectorCollection = eventsData.employees;
			for each(employee in employees){
				freeDays = employee.freeDays;
				for each(freeDayVO in freeDays){
					if(freeDayVO.id == value){
						return freeDayVO;
					}
				}

			}
			return null;
		}


		public function getBlockedDatesIndices():Vector.<int>
		{
			var vec:Vector.<int> = new <int>[];
			var item:ShiftDateVO;
			var data:VectorCollection = eventsData.shiftDates;
			for each(item in data){
				if(item.isBlocked){
					vec.push(item.index);
				}
			}
			return vec;
		}
		public function getBlockedEmployeesIndices():Vector.<int>
		{
			var vec:Vector.<int> = new <int>[];
			var item:EmployeeVO;
			var data:VectorCollection = eventsData.employees;
			for each(item in data){
				if(item.isBlocked){
					vec.push(item.index);
				}
			}
			return vec;
		}

        public function getDayOffVOById(value:int):DayOffVO
        {
            var dayOffVO:DayOffVO;
            var employee:EmployeeVO;
            var daysOff:VectorCollection;
            var employees:VectorCollection = eventsData.employees;
            for each(employee in employees){
                daysOff = employee.daysOff;
                for each(dayOffVO in daysOff){
                    if(dayOffVO.id == value){
                        return dayOffVO;
                    }
                }

            }
            return null;
        }
		
		public function get startDate():Date
		{
			return (eventsData.shiftDates.getItemAt(0) as ShiftDateVO).date as Date;
		}
		
		public function get startMinute():Number
		{
			return startDate.hours * 60 + startDate.minutes;
		}
    }
}
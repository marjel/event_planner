package pl.pkpik.epdp7.eventtimeline.model.proxy {

    import org.apache.flex.collections.VectorCollection;

    import org.puremvc.as3.patterns.proxy.Proxy;

    import pl.pkpik.epdp7.eventtimeline.view.vo.ContextMenuVO;

    import pl.pkpik.epdp7.eventtimeline.view.vo.ContextMenuCollection;

    public class ContextMenuDataProxy extends Proxy {

        public static const NAME:String = "ContextMenuDataProxy";

        public function ContextMenuDataProxy()
        {
            super(NAME, new ContextMenuCollection(new Vector.<ContextMenuVO>()));
        }

        public function setContextMenuData(h:String, mouseX:Number, mouseY:Number, data:Array, collection:VectorCollection=null):ContextMenuCollection
        {
            typedData.setData.apply(this, arguments);
            return typedData;
        }

        public function get typedData():ContextMenuCollection
        {
            return this.data as ContextMenuCollection;
        }
    }
}

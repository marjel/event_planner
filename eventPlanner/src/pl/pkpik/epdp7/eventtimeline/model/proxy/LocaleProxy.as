package pl.pkpik.epdp7.eventtimeline.model.proxy
{
	import mx.rpc.IResponder;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	import pl.pkpik.epdp7.eventtimeline.model.delgate.LoadXMLDelegate;
	import pl.pkpik.epdp7.eventtimeline.model.helper.LocaleVOParser;
	import pl.pkpik.epdp7.eventtimeline.model.vo.LocaleVO;
	

	
	public class LocaleProxy extends Proxy implements IProxy, IResponder
	{
		public static const NAME:String = "LocaleProxy";						// Proxy name
		
		// Notifications constansts
		public static const LOAD_SUCCESSFUL:String 	= NAME + "loadSuccessful";	// Successful notification
		public static const LOAD_FAILED:String 		= NAME + "loadFailed";		// Failed notification
		
		// Messages
		public static const ERROR_LOAD_FILE:String	= "Could Not Load the Config File!";
		
		private var resourcesMonitorProxy:ResourcesMonitorProxy;
		
		public function LocaleProxy ( data:Object = null ) 
		{
			super ( NAME, data );
		}
		
		override public function onRegister():void
		{		

			resourcesMonitorProxy = facade.retrieveProxy( ResourcesMonitorProxy.NAME ) as ResourcesMonitorProxy;

			resourcesMonitorProxy.addResource( LocaleProxy.NAME, true );

			setData (new LocaleVO());			
		}

		public function load():void
		{
			var configProxy:ConfigProxy = facade.retrieveProxy( ConfigProxy.NAME ) as ConfigProxy;

			var language:String = configProxy.getValue('language');
			
			// check if the language is defined
			if ( language && language != "" ){
				var url:String = "assets/xml/locale/" + language + '.xml';
				var delegate : LoadXMLDelegate = new LoadXMLDelegate(this, url);
				delegate.load();
			}else{
				resourceLoaded();
			}
		}

		public function result( rpcEvent : Object ) : void
		{
			LocaleVOParser.parse(localeVO, XML(rpcEvent.result).item);
			resourceLoaded();
		}

		public function fault( rpcEvent : Object ) : void 
		{
			sendNotification( LocaleProxy.LOAD_FAILED, LocaleProxy.ERROR_LOAD_FILE );
		}

		public function getText(key:String):String
		{
			return data[key.toLowerCase()];
		}
		
		public function get localeVO():LocaleVO
		{
			return data as LocaleVO;
		}

		
		private var alreadyLoaded:Boolean = false;
		
		private function resourceLoaded():void
		{
			
			if(!alreadyLoaded){
				resourcesMonitorProxy.resourceComplete( LocaleProxy.NAME );
				alreadyLoaded = true;
			}
			sendNotification( LocaleProxy.LOAD_SUCCESSFUL );
		}
	}
}
package pl.pkpik.epdp7.eventtimeline.model.proxy {
    import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.patterns.proxy.Proxy;

    import pl.pkpik.epdp7.eventtimeline.model.vo.AdditionalPlannerSolutionVO;
    import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;

    public class AdditionalEventsDataProxy extends Proxy {

        private var _pointer:int = -1;
        public static const NAME:String = "AdditionalEventsDataProxy";

        public function AdditionalEventsDataProxy() {
            super(NAME, new VectorCollection(new Vector.<AdditionalPlannerSolutionVO>()));
        }

        public function get eventsCollection():VectorCollection
        {
            return this.getData() as VectorCollection;
        }

        public function clearData():void
        {
            eventsCollection.removeAll();
            eventsCollection.refresh();
            _pointer = -1;
        }
        public function getEmployeeVOByReferenceId(dataIndex:uint, value:int):EmployeeVO
        {
            var employee:EmployeeVO;
            var employees:VectorCollection = getEventsData(dataIndex).employees;
            for each(employee in employees){
                if(employee.referenceId == value){
                    return employee;
                }
            }
            return null;
        }

        public function getEventsData(id:int = 0):AdditionalPlannerSolutionVO
        {
            _pointer = id;
            return eventsCollection.getItemAt(_pointer) as AdditionalPlannerSolutionVO

        }

        public function get pointer():int
        {
            return _pointer;
        }
    }
}

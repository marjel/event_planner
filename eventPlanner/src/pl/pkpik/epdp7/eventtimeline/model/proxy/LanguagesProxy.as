package pl.pkpik.epdp7.eventtimeline.model.proxy
{
	import mx.collections.ArrayCollection;
	import mx.rpc.IResponder;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	import pl.pkpik.epdp7.eventtimeline.model.delgate.LoadXMLDelegate;
	import pl.pkpik.epdp7.eventtimeline.model.vo.LanguageVO;
	
	public class LanguagesProxy extends Proxy implements IProxy, IResponder
	{
		
		
		public static const NAME:String = "LanguagesProxy";
		
		public static const FILE_PATH:String = "assets/xml/locale/languages.xml";
		public static const LOAD_SUCCESSFUL:String 	= NAME + "loadSuccessful";	// Successful notification
		public static const LOAD_FAILED:String 		= NAME + "loadFailed";		// Failed notification
		
		// Messages
		public static const ERROR_LOAD_FILE:String	= "Could Not Load the Languages File!";
		
		private var resourcesMonitorProxy:ResourcesMonitorProxy;
		
		public function LanguagesProxy(data:Object=null)
		{
			super(NAME, data);
		}
		
		override public function onRegister():void
		{		

			resourcesMonitorProxy = facade.retrieveProxy( ResourcesMonitorProxy.NAME ) as ResourcesMonitorProxy;
			resourcesMonitorProxy.addResource( LanguagesProxy.NAME, true );
			setData (new ArrayCollection());			
		}

		public function load():void
		{
			var delegate : LoadXMLDelegate = new LoadXMLDelegate(this, FILE_PATH);
			delegate.load();
			
			
		}
		
		public function result(rpcEvent:Object):void
		{
			parse(XML(rpcEvent.result).item as XMLList);
			resourceLoaded();
		}
		
		private function parse(data:XMLList):void
		{
			var languageVO:LanguageVO;
			for each(var item:XML in data){
				languageVO = new LanguageVO();
				languageVO.id = item.@id;
				languageVO.name = item.@name;
				languageVO.flag = item.@flag; 
				languages.addItem(languageVO);
			}
		}
		
		public function getLanguageVO(key:String):LanguageVO
		{
			
			for each(var item:LanguageVO in languages){
				if(item.id == key){
					return item;
				}
			}
			
			return null;
		}
		
		public function fault(info:Object):void
		{
			
			sendNotification( LanguagesProxy.LOAD_FAILED, LanguagesProxy.ERROR_LOAD_FILE );
			
		}
		
		public function get languages():ArrayCollection
		{
			return data as ArrayCollection;
		}
		
		private function resourceLoaded():void
		{
			// call the StartupMonitorProxy for notify that the resource is loaded
			resourcesMonitorProxy.resourceComplete( LanguagesProxy.NAME );
			sendNotification( LanguagesProxy.LOAD_SUCCESSFUL );
		}
		
	}
}
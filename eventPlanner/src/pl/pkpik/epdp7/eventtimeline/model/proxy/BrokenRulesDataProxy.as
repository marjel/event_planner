package pl.pkpik.epdp7.eventtimeline.model.proxy {

    import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.patterns.proxy.Proxy;

    import pl.pkpik.epdp7.eventtimeline.model.vo.BrokenRuleVO;


    public class BrokenRulesDataProxy extends Proxy{


        public static const NAME:String = "BrokenRulesDataProxy";

        public function BrokenRulesDataProxy() {
            super(NAME, null);
        }

        public function getBrokenRuleById(value:int):BrokenRuleVO
        {
            for each(var item:BrokenRuleVO in typedData){
                if(value==item.id){
                    return item;
                }
            }
            return null;
        }

        public function get typedData():VectorCollection
        {
            return this.data as VectorCollection;
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.model.vo
{
    import org.apache.flex.collections.VectorCollection;

    [Bindable]
	public final class AdditionalPlannerSolutionVO {

        private var _id: int;

        public function toString():String
        {
            return "AdditionalPlannerSolutionVO{_id=" + String(_id) + "}";
        }

        private var _employees:VectorCollection = new VectorCollection(new Vector.<EmployeeVO>());
        private var _shifts:VectorCollection = new VectorCollection(new Vector.<ShiftVO>());
        private var _shiftAssignments:VectorCollection = new VectorCollection(new Vector.<ShiftAssignmentVO>());

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get shifts():VectorCollection {
            return _shifts;
        }

        public function get shiftAssignments():VectorCollection {
            return _shiftAssignments;
        }


        public function AdditionalPlannerSolutionVO() {
        }

        public function get employees():VectorCollection {
            return _employees;
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.model.vo
{
    import org.apache.flex.collections.VectorCollection;
	[Bindable]
	public final class EmployeeVO {

		private var _id: int;
		private var _referenceId: int;
        private var _name: String;
        private var _isBlocked: Boolean;
        private var _shifts:VectorCollection = new VectorCollection(new Vector.<ShiftAssignmentVO>());
        private var _daysOff:VectorCollection = new VectorCollection(new Vector.<DayOffVO>());
        private var _freeDays:VectorCollection = new VectorCollection(new Vector.<FreeDayVO>());
        private var _contracts:VectorCollection = new VectorCollection(new Vector.<ContractVO>());
		private var _measurements:VectorCollection = new VectorCollection(new Vector.<MeasurementVO>());
		
		private var _index:int = -1;
		
		public function get index():int
		{
			return _index;
		}

		public function set index(value:int):void
		{
			_index = value;
		}

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get name():String {
            return _name;
        }

        public function set name(value:String):void {
            _name = value;
        }

        public function get isBlocked():Boolean {
            return _isBlocked;
        }

        public function set isBlocked(value:Boolean):void {
            _isBlocked = value;
        }

        public function get shifts():VectorCollection {
            return _shifts;
        }

        public function get daysOff():VectorCollection {
            return _daysOff;
        }

        public function get freeDays():VectorCollection {
            return _freeDays;
        }

        public function get contracts():VectorCollection {
            return _contracts;
        }

        public function get measurements():VectorCollection {
            return _measurements;
        }

        public function init(id:int, referenceId:int, isBlocked:Boolean):void {

            _id = id;
            _referenceId = referenceId;
            _isBlocked = isBlocked;
        }
        public function get referenceId():int {
            return _referenceId;
        }
        public function EmployeeVO() {
        }

        public function toString():String {
            return "EmployeeVO{_id=" + String(_id) + ",_name=" + String(_name) + ",_isBlocked=" + String(_isBlocked) + ",_index=" + String(_index) + "}";
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.model.vo
{

    import org.apache.flex.collections.VectorCollection;

    import pl.pkpik.epdp7.eventtimeline.util.DateUtils;

	[Bindable]
	public final class ShiftDateVO {

        private var _id: int;
        private var _dte: String;
        private var _isHoliday: Boolean;
        private var _isBlocked: Boolean;
        private var _measurements:VectorCollection = new VectorCollection(new Vector.<MeasurementVO>);
		
		private var _index:int = -1;
		
		public function get index():int
		{
			return _index;
		}
		
		public function set index(value:int):void
		{
			_index = value;
		}

		public function get minutesShift():Number
        {
            return DateUtils.getMinutesShiftFromDateString(_dte);
        }
		public function get date():Date
		{
			return DateUtils.getFullDateFromDateString(_dte, "T");
		}

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get dte():String {
            return _dte;
        }

        public function set dte(value:String):void {
            _dte = value;
        }

        public function get isHoliday():Boolean {
            return _isHoliday;
        }

        public function set isHoliday(value:Boolean):void {
            _isHoliday = value;
        }

        public function get isBlocked():Boolean {
            return _isBlocked;
        }

        public function set isBlocked(value:Boolean):void {
            _isBlocked = value;
        }

        public function get measurements():VectorCollection {
            return _measurements;
        }

        public function init(id:int, dte:String, isHoliday:Boolean, isBlocked:Boolean):void {
            _id = id;
            _dte = dte;
            _isHoliday = isHoliday;
            _isBlocked = isBlocked;
        }
        public function ShiftDateVO() {
        }

        public function toString():String {
            return "ShiftDateVO{_id=" + String(_id) + ",_dte=" + String(_dte) + ",_isHoliday=" + String(_isHoliday) + ",_isBlocked=" + String(_isBlocked) + ",_index=" + String(_index) + "}";
        }
    }
}

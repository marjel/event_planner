package pl.pkpik.epdp7.eventtimeline.model.vo
{
	
	[Bindable]
	public final class PlannerSolutionInfoVO
	{
		private var _totalShiftTimePrecalculate: int;
        private var _planningDateStartMinute: int;

        public function get totalShiftTimePrecalculate():int {
            return _totalShiftTimePrecalculate;
        }

        public function set totalShiftTimePrecalculate(value:int):void {
            _totalShiftTimePrecalculate = value;
        }

        public function get planningDateStartMinute():int {
            return _planningDateStartMinute;
        }

        public function set planningDateStartMinute(value:int):void {
            _planningDateStartMinute = value;
        }

        public function PlannerSolutionInfoVO(totalShiftTimePrecalculate:int, planningDateStartMinute:int) {
            _totalShiftTimePrecalculate = totalShiftTimePrecalculate;
            _planningDateStartMinute = planningDateStartMinute;
        }

        public function toString():String {
            return "PlannerSolutionInfoVO{_totalShiftTimePrecalculate=" + String(_totalShiftTimePrecalculate) + ",_planningDateStartMinute=" + String(_planningDateStartMinute) + "}";
        }
    }
}
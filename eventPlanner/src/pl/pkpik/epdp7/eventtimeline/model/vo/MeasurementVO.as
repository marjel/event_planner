package pl.pkpik.epdp7.eventtimeline.model.vo
{
	[Bindable]
	public final class MeasurementVO
	{
		public var id:String;
		public var name:String;
		public var value:String;
		public var color:int;
		public var shortcut:String;

        public function toString():String {
            return "MeasurementVO{name=" + String(name) + ",value=" + String(value) + ",shortcut=" + String(shortcut) + "}";
        }
    }
}
package pl.pkpik.epdp7.eventtimeline.model.vo
{

import pl.pkpik.epdp7.eventtimeline.util.DateUtils;

[Bindable]
	public final class DayOffVO
	{
		private var _id: int;
        private var _startMinute: int;
        private var _endMinute: int;
        private var _startDte: String;
        private var _endDte: String;

        private var _reasonName: String;
        private var _reasonShortcut: String;
		
		public function get startDate():Date
		{
			return DateUtils.getFullDateFromDateString(_startDte, "T");
		}
		public function get endDate():Date
		{
			return DateUtils.getFullDateFromDateString(_endDte, "T");
		}
		
		public function get timeOffset():int
		{
			return DateUtils.getTimeOffset(_startDte);
		}

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get startMinute():int {
            return _startMinute;
        }

        public function set startMinute(value:int):void {
            _startMinute = value;
        }

        public function get endMinute():int {
            return _endMinute;
        }

        public function set endMinute(value:int):void {
            _endMinute = value;
        }

        public function get startDte():String {
            return _startDte;
        }

        public function set startDte(value:String):void {
            _startDte = value;
        }

        public function get endDte():String {
            return _endDte;
        }

        public function set endDte(value:String):void {
            _endDte = value;
        }

        public function init(id:int, startMinute:int, endMinute:int, startDte:String, endDte:String, reasonName: String, reasonShortcut: String):void
        {
            if(id != _id){
                _id = id;
            }
            if(!isNaN(startMinute)){
                _startMinute = startMinute;
            }
            if(!isNaN(endMinute)){
                _endMinute = endMinute;
            }
            if(startDte){
                _startDte = startDte;
            }
            if(endDte){
                _endDte = endDte;
            }
            if(reasonName){
                _reasonName = reasonName;
            }
            if(reasonShortcut){
                _reasonShortcut = reasonShortcut;
            }

        }

        public function DayOffVO()
        {
        }

        public function toString():String {
            return "DayOffVO{_id=" + String(_id) + ",_startDte=" + String(_startDte) + ",_endDte=" + String(_endDte) + "}";
        }

        public function get reasonShortcut():String {
            return _reasonShortcut || "*";
        }

        public function set reasonShortcut(value:String):void {
            _reasonShortcut = value;
        }

        public function get reasonName():String {
            return _reasonName || "Nieznany";
        }

        public function set reasonName(value:String):void {
            _reasonName = value;
        }
    }
}

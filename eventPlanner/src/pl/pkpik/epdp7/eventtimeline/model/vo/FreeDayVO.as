package pl.pkpik.epdp7.eventtimeline.model.vo
{
	
	import pl.pkpik.epdp7.eventtimeline.util.DateUtils;
	[Bindable]
	public final class FreeDayVO
	{
		private var _id: int;
        private var _reason: int;
        private var _dte: String;
        private var _isBlocked: Boolean;
        private var _isAssigned: Boolean;
        private var _assignedDateStartMinute: int;
        private var _assignedDte: String;
		
		public function get date():Date
		{
			return DateUtils.getFullDateFromDateString(_dte, "T");
		}
		public function get assignedDate():Date
		{
			return _assignedDte ? DateUtils.getFullDateFromDateString(_assignedDte, "T") : null;
		}

		
		
		public function get timeOffset():int
		{
			return DateUtils.getTimeOffset(_dte);
		}

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get reason():int {
            return _reason;
        }

        public function set reason(value:int):void {
            _reason = value;
        }

        public function get dte():String {
            return _dte;
        }

        public function set dte(value:String):void {
            _dte = value;
        }

        public function get isBlocked():Boolean {
            return _isBlocked;
        }

        public function set isBlocked(value:Boolean):void {
            _isBlocked = value;
        }

        public function get isAssigned():Boolean {
            return _isAssigned;
        }

        public function set isAssigned(value:Boolean):void {
            _isAssigned = value;
        }

        public function get assignedDateStartMinute():int {
            return _assignedDateStartMinute;
        }

        public function set assignedDateStartMinute(value:int):void {
            _assignedDateStartMinute = value;
        }

        public function get assignedDte():String {
            return _assignedDte;
        }

        public function set assignedDte(value:String):void {
            _assignedDte = value;
        }

        public function init(id:int, reason:int, dte:String, isBlocked:Boolean, isAssigned:Boolean, assignedDateStartMinute:int, assignedDte:String):void
        {
            _id = id;
            _reason = reason;
            _dte = dte;
            _isBlocked = isBlocked;
            _isAssigned = isAssigned;
            _assignedDateStartMinute = assignedDateStartMinute;
            _assignedDte = assignedDte;
        }

        public function FreeDayVO()
        {
        }

        public function toString():String {
            return "FreeDayVO{_id=" + String(_id) + ",_reason=" + String(_reason) + ",_assignedDte=" + String(_assignedDte) + "}";
        }
    }
}
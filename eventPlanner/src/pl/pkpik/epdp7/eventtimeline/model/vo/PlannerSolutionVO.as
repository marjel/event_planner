package pl.pkpik.epdp7.eventtimeline.model.vo
{
    import org.apache.flex.collections.VectorCollection;

	[Bindable]
	public final class PlannerSolutionVO {


        private var _id:int;
        private var _plannerSolutionInfo:PlannerSolutionInfoVO;
		private var _employees:VectorCollection = new VectorCollection(new Vector.<EmployeeVO>());
        private var _shifts:VectorCollection = new VectorCollection(new Vector.<ShiftVO>());
        private var _shiftAssignments:VectorCollection = new VectorCollection(new Vector.<ShiftAssignmentVO>());
        private var _shiftDates:VectorCollection = new VectorCollection(new Vector.<ShiftDateVO>());

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get plannerSolutionInfo():PlannerSolutionInfoVO {
            return _plannerSolutionInfo;
        }

        public function set plannerSolutionInfo(value:PlannerSolutionInfoVO):void {
            _plannerSolutionInfo = value;
        }

        public function get employees():VectorCollection {
            return _employees;
        }

        public function get shifts():VectorCollection {
            return _shifts;
        }

        public function get shiftAssignments():VectorCollection {
            return _shiftAssignments;
        }

        public function get shiftDates():VectorCollection {
            return _shiftDates;
        }

        public function PlannerSolutionVO() {

        }

        public function toString():String {
            return "PlannerSolutionVO{_id=" + String(_id) + ",_shifts=" + String(_shifts) + "}";
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.model.vo
{
	public final class ResourceVO
	{
		public var proxyName:String;
		public var loaded:Boolean;
		public var blockChain:Boolean;
		
		function ResourceVO( proxyName:String, blockChain:Boolean )
		{
			this.proxyName = proxyName;
			this.loaded = false;
			this.blockChain = blockChain;
		}
	}
}
package pl.pkpik.epdp7.eventtimeline.model.vo
{
	import pl.pkpik.epdp7.eventtimeline.util.DateUtils;
	[Bindable]
	public final class ShiftVO {

		private var _id: int;
		private var _referenceId: int;
        private var _startMinute: int;
        private var _endMinute: int;
        private var _startDte: String;
        private var _endDte: String;
        private var _name: String;
        private var _shiftDateReference: int;
		
		public function get startDate():Date
		{
			return DateUtils.getFullDateFromDateString(_startDte, "T");
		}
		public function get endDate():Date
		{
			return DateUtils.getFullDateFromDateString(_endDte, "T");
		}
		
		
		public function get timeOffset():int
		{
			return DateUtils.getTimeOffset(_startDte);
		}

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get startMinute():int {
            return _startMinute;
        }

        public function set startMinute(value:int):void {
            _startMinute = value;
        }

        public function get endMinute():int {
            return _endMinute;
        }

        public function set endMinute(value:int):void {
            _endMinute = value;
        }

        public function get startDte():String {
            return _startDte;
        }

        public function set startDte(value:String):void {
            _startDte = value;
        }

        public function get endDte():String {
            return _endDte;
        }

        public function set endDte(value:String):void {
            _endDte = value;
        }

        public function get name():String {
            return _name;
        }

        public function set name(value:String):void {
            _name = value;
        }

        public function get shiftDateReference():int {
            return _shiftDateReference;
        }

        public function set shiftDateReference(value:int):void {
            _shiftDateReference = value;
        }

        public function init(id:int, referenceId:int, startMinute:int, endMinute:int, startDte:String, endDte:String, name:String, shiftDateReference:int):void
        {
            _id = id;
            _referenceId = referenceId;
            _startMinute = startMinute;
            _endMinute = endMinute;
            _startDte = startDte;
            _endDte = endDte;
            _name = name;
            _shiftDateReference = shiftDateReference;
        }

        public function ShiftVO() {
        }

        public function toString():String {
            return "ShiftVO{_id=" + String(_id) + ",_startDte=" + String(_startDte) + ",_endDte=" + String(_endDte) + ",_name=" + String(_name) + "}";
        }

        public function get referenceId():int {
            return _referenceId;
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.model.vo {
    [Bindable]
    public final class ContractVO {
        private var _id: int;
        private var _employeeReference: int;
        private var _nightStart: int;
        private var _nightEnd: int;
        private var _contractStartMinute: int;
        private var _contractEndMinute: int;
        private var _minutesInNight: int;


        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get employeeReference():int {
            return _employeeReference;
        }

        public function set employeeReference(value:int):void {
            _employeeReference = value;
        }

        public function get nightStart():int {
            return _nightStart;
        }

        public function set nightStart(value:int):void {
            _nightStart = value;
        }

        public function get nightEnd():int {
            return _nightEnd;
        }

        public function set nightEnd(value:int):void {
            _nightEnd = value;
        }

        public function get contractStartMinute():int {
            return _contractStartMinute;
        }

        public function set contractStartMinute(value:int):void {
            _contractStartMinute = value;
        }

        public function get contractEndMinute():int {
            return _contractEndMinute;
        }

        public function set contractEndMinute(value:int):void {
            _contractEndMinute = value;
        }

        public function get minutesInNight():int {
            return _minutesInNight;
        }

        public function set minutesInNight(value:int):void {
            _minutesInNight = value;
        }

        public function toString():String {
            return "ContractVO{_id=" + String(_id) + ",_employeeReference=" + String(_employeeReference) + ",_nightStart=" + String(_nightStart) + ",_nightEnd=" + String(_nightEnd) + "}";
        }
        public function init(id:int, employeeReference:int, nightStart:int, nightEnd:int, contractStartMinute:int, contractEndMinute:int, minutesInNight:int):void
        {
            _id = id;
            _employeeReference = employeeReference;
            _nightStart = nightStart;
            _nightEnd = nightEnd;
            _contractStartMinute = contractStartMinute;
            _contractEndMinute = contractEndMinute;
            _minutesInNight = minutesInNight;
        }
        public function ContractVO() {
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.model.vo {
    import org.apache.flex.collections.VectorCollection;

    [Bindable]
    public final class BrokenRuleVO {

        public var id:int;
        public var name:String;
        public var weight:int;
        public var isHard:Boolean;
        public var employeeList:VectorCollection;
        public var shiftAssignmentList:VectorCollection;


        public function toString():String {
            return "BrokenRuleVO{name=" + String(name) + ",shiftAssignmentList.length=" + String(shiftAssignmentList.length) + ",employeeList.length=" + String(employeeList.length) + "}";
        }

        public function BrokenRuleVO() {
            employeeList = new VectorCollection(new Vector.<uint>());
            shiftAssignmentList = new VectorCollection(new Vector.<uint>());
        }
    }
}

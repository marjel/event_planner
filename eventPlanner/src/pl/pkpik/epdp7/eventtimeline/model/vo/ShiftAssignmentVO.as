package pl.pkpik.epdp7.eventtimeline.model.vo
{
	[Bindable]
	public final class ShiftAssignmentVO {

		private var _id: int;
        private var _employeeReference: int;
        private var _shiftReference: int;
        private var _isShiftNight: Boolean;
        private var _isShiftBlocked: Boolean;
        private var _extMinuteAfter: int;
        private var _extMinuteBefore: int;

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get employeeReference():int {
            return _employeeReference;
        }

        public function set employeeReference(value:int):void {
            _employeeReference = value;
        }

        public function get shiftReference():int {
            return _shiftReference;
        }

        public function set shiftReference(value:int):void {
            _shiftReference = value;
        }

        public function get isShiftNight():Boolean {
            return _isShiftNight;
        }

        public function set isShiftNight(value:Boolean):void {
            _isShiftNight = value;
        }

        public function get isShiftBlocked():Boolean {
            return _isShiftBlocked;
        }

        public function set isShiftBlocked(value:Boolean):void {
            _isShiftBlocked = value;
        }

        public function get extMinuteAfter():int {
            return _extMinuteAfter;
        }

        public function set extMinuteAfter(value:int):void {
            _extMinuteAfter = value;
        }

        public function get extMinuteBefore():int {
            return _extMinuteBefore;
        }

        public function set extMinuteBefore(value:int):void {
            _extMinuteBefore = value;
        }

        public function init(id:int, employeeReference:int, shiftReference:int, isShiftNight:Boolean, isShiftBlocked:Boolean, extMinuteAfter:int, extMinuteBefore:int):void
        {
            _id = id;
            _employeeReference = employeeReference;
            _shiftReference = shiftReference;
            _isShiftNight = isShiftNight;
            _isShiftBlocked = isShiftBlocked;
            _extMinuteAfter = extMinuteAfter;
            _extMinuteBefore = extMinuteBefore;
        }

        public function ShiftAssignmentVO() {
        }

        public function toString():String {
            return "ShiftAssignmentVO{_extMinuteBefore=" + String(_extMinuteBefore) + ",_employeeReference=" + String(_employeeReference) + ",_shiftReference=" + String(_shiftReference) + "}";
        }
    }
}

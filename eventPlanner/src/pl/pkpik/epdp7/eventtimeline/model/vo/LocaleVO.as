package pl.pkpik.epdp7.eventtimeline.model.vo
{
	
	[Bindable]
	final public class LocaleVO
	{
		// Change language
		private var _changeLanguageLabel:String;

		public function get changeLanguageLabel():String
		{
			return _changeLanguageLabel;
		}

		public function set changeLanguageLabel(value:String):void
		{
			_changeLanguageLabel = value;
		}

	}
}
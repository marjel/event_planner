package pl.pkpik.epdp7.eventtimeline.model.vo
{
	[Bindable]
	final public class LanguageVO
	{
		public var id:String;
		public var name:String;
		public var flag:String;
	}
}
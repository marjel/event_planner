package pl.pkpik.epdp7.eventtimeline.model.factory {
    import pl.pkpik.epdp7.eventtimeline.model.vo.DayOffVO;

    public final class DayOffVOFactory {

        private var vo:DayOffVO;

        public static function GetVO(data:XML):DayOffVO
        {

            var dayOffParser:DayOffVOFactory = new DayOffVOFactory();
            dayOffParser.vo.init(
                    data.id,
                    data.startMinute,
                    data.endMinute,
                    data.startDate,
                    data.endDate,
                    data.reasonName,
                    data.reasonShortcut
            );
            return dayOffParser.vo;

        }

        public function DayOffVOFactory() {
           vo = new DayOffVO();
        }
    }
}

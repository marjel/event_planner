package pl.pkpik.epdp7.eventtimeline.model.factory {

    import flash.system.System;

    import org.puremvc.as3.patterns.observer.Notifier;
    import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
    import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
    import pl.pkpik.epdp7.eventtimeline.model.vo.AdditionalPlannerSolutionVO;

    public final class AdditionalPlannerSolutionDataFactory extends Notifier {

        private var psvo:AdditionalPlannerSolutionVO;
        private static const INSTANCE:AdditionalPlannerSolutionDataFactory = new AdditionalPlannerSolutionDataFactory();
        private var shiftsList:XMLList;
        private var shiftAssignmentsList:XMLList;
        private var employeesList:XMLList;
        private var xml:XML;

        private function getQueue():Array
        {
            return  [
                new ActionData(parseEmployees, this, null, 2, -1, "Parsing Additional Employees..."),
                new ActionData(parseShifts, this, null, 2, -1, "Parsing Additional Shifts..."),
                new ActionData(parseShiftAssignments, this, null, 2, -1, "Set Additional Assignments..."),
                new ActionData(sendNotification, this, [ApplicationCommands.ADDITIONAL_EVENTS_DATA_LOADED], 2, -1, "Start render the additional plan...")
            ];
        }

        private function parseShifts():void
        {
            var xmlItm:XML;
            for each(xmlItm in shiftsList){
                psvo.shifts.addItem(ShiftVOFactory.GetVO(xmlItm));
            }
        }

        private function parseShiftAssignments():void
        {
            var xmlItm:XML;
            for each(xmlItm in shiftAssignmentsList){
                psvo.shiftAssignments.addItem(ShiftAssignmentVOFactory.GetVO(xmlItm));
            }
           System.disposeXML(xml);
        }

        private function parseEmployees():void
        {
            var xmlItm:XML;
            for each(xmlItm in employeesList){
                psvo.employees.addItem(EmployeeVOFactory.GetVO(xmlItm));
            }
        }

        public static function GetVO(data:XML):AdditionalPlannerSolutionVO
        {
            INSTANCE.xml = data;
            INSTANCE.psvo = new AdditionalPlannerSolutionVO();
            INSTANCE.init();
            return INSTANCE.psvo;

        }

        private function init():void {
            psvo.id = xml.id;
            shiftsList = xml..Shift as XMLList;
            shiftAssignmentsList = xml..ShiftAssignment as XMLList;
            employeesList = xml..Employee as XMLList;
            callFunctionLater(getQueue());

        }

        function AdditionalPlannerSolutionDataFactory() {

        }
    }
}

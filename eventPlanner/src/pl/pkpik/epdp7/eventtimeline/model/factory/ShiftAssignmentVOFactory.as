package pl.pkpik.epdp7.eventtimeline.model.factory {
    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftAssignmentVO;

    public final class ShiftAssignmentVOFactory {

        private var vo:ShiftAssignmentVO;

        public static function GetVO(data:XML):ShiftAssignmentVO
        {

            var shiftAssParser:ShiftAssignmentVOFactory = new ShiftAssignmentVOFactory();
            shiftAssParser.vo.init(
                    data.id,
                    data.employee.@reference,
                    data.shift.@reference,
                    data.isShiftNight=="true" ? true : false,
                    data.isShiftBlocked=="true" ? true : false,
                    data.extMinuteAfter,
                    data.extMinuteBefore
            );
            return shiftAssParser.vo;

        }

        public function ShiftAssignmentVOFactory() {
            vo = new ShiftAssignmentVO();
        }
    }
}

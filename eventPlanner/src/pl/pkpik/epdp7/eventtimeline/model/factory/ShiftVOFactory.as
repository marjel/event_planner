package pl.pkpik.epdp7.eventtimeline.model.factory {
    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftVO;

    public final class ShiftVOFactory {

        private var vo:ShiftVO;

        public static function GetVO(data:XML):ShiftVO
        {
            var shiftParser:ShiftVOFactory = new ShiftVOFactory();
            shiftParser.vo.init(
                    data.id,
                    data.@id,
                    data.startMinute,
                    data.endMinute,
                    data.startDate,
                    data.endDate,
                    data.shiftName,
                    data.shiftDate.@reference
            );
            return shiftParser.vo;

        }

        public function ShiftVOFactory() {
            vo = new ShiftVO();
        }
    }
}

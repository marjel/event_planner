package pl.pkpik.epdp7.eventtimeline.model.factory {
    import pl.pkpik.epdp7.eventtimeline.model.vo.ContractVO;

    public final class ContractVOFactory {

        private var vo:ContractVO;

        public static function GetVO(data:XML):ContractVO
        {

            var contractParser:ContractVOFactory = new ContractVOFactory();
            contractParser.vo.init(
                    data.id,
                    data.employee.@reference,
                    data.nightStart,
                    data.nightEnd,
                    data.contractStartMinute,
                    data.contractEndMinute,
                    data.minutesInNight
            );
            return contractParser.vo;

        }

        public function ContractVOFactory() {
            vo = new ContractVO();
        }
    }
}

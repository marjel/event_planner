package pl.pkpik.epdp7.eventtimeline.model.factory {
    import pl.pkpik.epdp7.eventtimeline.model.vo.FreeDayVO;

    public final class FreeDayVOFactory {

        private var vo:FreeDayVO;

        public static function GetVO(data:XML):FreeDayVO
        {

            var freeDayarser:FreeDayVOFactory = new FreeDayVOFactory();
            freeDayarser.vo.init(
                    data.id,
                    data.reason,
                    data.date,
                    data.isBlocked=="true" ? true : false,
                    data.isAssigned=="true" ? true : false,
                    data.assignedDateStartMinute,
                    data.assignedDate
            );
            return freeDayarser.vo;

        }

        public function FreeDayVOFactory() {
            vo = new FreeDayVO();
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.model.factory {
    import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;

    public final class EmployeeVOFactory {

        private var vo:EmployeeVO;


        private function parseFreeDays(xmlList:XMLList):void
        {
            var xmlItm:XML;
             for each(xmlItm in xmlList){
                 vo.freeDays.addItem(FreeDayVOFactory.GetVO(xmlItm));
             }
        }

        private function parseOffDays(xmlList:XMLList):void
        {
            var xmlItm:XML;
            for each(xmlItm in xmlList){
                vo.daysOff.addItem(DayOffVOFactory.GetVO(xmlItm));
            }
        }

        private function parseContracts(xmlList:XMLList):void
        {
            var xmlItm:XML;
            for each(xmlItm in xmlList){
                vo.contracts.addItem(ContractVOFactory.GetVO(xmlItm));
            }
        }

        public static function GetVO(data:XML):EmployeeVO
        {

            var employeeParser:EmployeeVOFactory = new EmployeeVOFactory(data);
            employeeParser.parseFreeDays(data..FreeDay);
            employeeParser.parseOffDays(data..DayOff);
            employeeParser.parseContracts(data..Contract);
            return employeeParser.vo;

        }

        function EmployeeVOFactory(data:XML) {
            vo = new EmployeeVO();
            vo.init(data.id, data.@id, data.isBlocked=="true" ? true : false);
            vo.name = data.name;
        }
    }
}

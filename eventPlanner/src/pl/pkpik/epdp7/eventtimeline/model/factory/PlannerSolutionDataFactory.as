package pl.pkpik.epdp7.eventtimeline.model.factory {


    import flash.system.System;

    import org.puremvc.as3.patterns.observer.Notifier;

    import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
    import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftAssignmentVO;

    import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;

    import pl.pkpik.epdp7.eventtimeline.model.vo.PlannerSolutionInfoVO;
    import pl.pkpik.epdp7.eventtimeline.model.vo.PlannerSolutionVO;

    public final class PlannerSolutionDataFactory extends Notifier {

        private var psvo:PlannerSolutionVO;
        private static const INSTANCE:PlannerSolutionDataFactory = new PlannerSolutionDataFactory();

        private var employeesList:XMLList;
        private var datesList:XMLList;
        private var shiftsList:XMLList;
        private var shiftAssignmentsList:XMLList;
        private var _xml:XML;


        private function getQueue():Array
        {
            return  [
                new ActionData(parseEmployees, this, null, 1, -1, "Parsing Employees..."),
                new ActionData(parseDates, this, null, 2, -1, "Parsing Dates..."),
                new ActionData(parseShifts, this, null, 1, -1, "Parsing Shifts..."),
                new ActionData(parseShiftAssignments, this, null, 1, -1, "Parsing Assignments..."),
                new ActionData(assignShiftAssignmentsToEmployees, this, null, 1, -1, "Assigning shifts to employees..." ),
                new ActionData(sendNotification, this, [ApplicationCommands.EVENTS_DATA_LOADED], 1, -1, "Start arranging new Plan...")
            ];
        }
        private function parseEmployees():void
        {
            var xmlItm:XML;
            for each(xmlItm in employeesList){
                psvo.employees.addItem(EmployeeVOFactory.GetVO(xmlItm));
            }
        }

        private function parseDates():void
        {
            var xmlItm:XML;
            for each(xmlItm in datesList){
                psvo.shiftDates.addItem(ShiftDateVOFactory.GetVO(xmlItm));
            }
        }

        private function parseShifts():void
        {
            var xmlItm:XML;
            for each(xmlItm in shiftsList){
                psvo.shifts.addItem(ShiftVOFactory.GetVO(xmlItm));
            }
        }

        private function parseShiftAssignments():void
        {
            var xmlItm:XML;
            for each(xmlItm in shiftAssignmentsList){
                psvo.shiftAssignments.addItem(ShiftAssignmentVOFactory.GetVO(xmlItm));
            }
        }

        private function assignShiftAssignmentsToEmployees():void
        {
            var employee:EmployeeVO;
            var itm:ShiftAssignmentVO;
            for each(employee in psvo.employees){
                for each(itm in psvo.shiftAssignments){
                    if(employee.referenceId == itm.employeeReference){
                        employee.shifts.addItem(itm);
                    }
                }
            }
            System.disposeXML(_xml);
        }

        public static function GetPlannerSolution(data:XML):PlannerSolutionVO
        {
            return INSTANCE.init(data);
        }

        private function init(data:XML):PlannerSolutionVO {
            _xml = data;
            psvo = new PlannerSolutionVO();
            psvo.id = _xml.id;
            psvo.plannerSolutionInfo = new PlannerSolutionInfoVO(_xml.plannerSolutionInfo.totalShiftTimePrecalculate,
                    _xml.plannerSolutionInfo.planningDateStartMinute);
            employeesList = _xml..Employee as XMLList;
            datesList = _xml..ShiftDate as XMLList;
            shiftsList = _xml..Shift as XMLList;
            shiftAssignmentsList = _xml..ShiftAssignment as XMLList;
            callFunctionLater(getQueue());
            return psvo;
        }

        function PlannerSolutionDataFactory() {
        }
    }
}

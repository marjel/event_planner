package pl.pkpik.epdp7.eventtimeline.model.factory {
    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftDateVO;

    public final class ShiftDateVOFactory {

        private var vo:ShiftDateVO;

        public static function GetVO(data:XML):ShiftDateVO
        {

            var dateParser:ShiftDateVOFactory = new ShiftDateVOFactory();
            dateParser.vo.init(
                    data.id,
                    data.date,
                    data.isHoliday=="true" ? true : false,
                    data.isBlocked=="true" ? true : false
            );
            return dateParser.vo;

        }

        public function ShiftDateVOFactory() {
            vo = new ShiftDateVO();
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.model.helper
{
	
	import pl.pkpik.epdp7.eventtimeline.model.vo.ConfigVO;
	
	public class ConfigVOParser
	{
		static public function parse(data:ConfigVO, dataList:*):void
		{
			for each(var itm:XML in dataList){
				data[itm.@name] = itm.@value != null ? itm.@value : itm;
			}
		}
		
		static public function getXML(data:ConfigVO):XML
		{
			var xml:XML = <config></config>
			for (var prop:String in data) {
				xml.appendChild(<param name={prop}>{data[prop]}</param>);
			}
			return xml;
		}
	}
}
package pl.pkpik.epdp7.eventtimeline.model.helper
{
	import flash.utils.describeType;
	
	import pl.pkpik.epdp7.eventtimeline.model.vo.LocaleVO;
	
	public class LocaleVOParser
	{
		static public function parse(data:LocaleVO, dataList:*):void
		{
			for each(var itm:XML in dataList){
				if(data.hasOwnProperty(itm.@name)){
					data[itm.@name] = itm;
				}
				
			}
		}
		
		static public function getXML(data:LocaleVO):XML
		{
			
			var sourceInfo:XML = describeType(data);
			var xml:XML = <locale></locale>
			for each(var prop:XML in sourceInfo.accessor) {
				if(prop.@access == "readwrite") {
					xml.appendChild(<item name={prop.@name}><![CDATA[{data[prop.@name]}]]></item>);
					
				}
			}
			return xml;
		}
	}
}
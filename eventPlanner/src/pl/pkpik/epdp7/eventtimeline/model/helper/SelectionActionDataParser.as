package pl.pkpik.epdp7.eventtimeline.model.helper {
    import org.apache.flex.collections.VectorCollection;

    import pl.pkpik.epdp7.eventtimeline.view.component.ISelectable;

    public class SelectionActionDataParser {
        public static function GetSelectionData(value:VectorCollection):Array
        {
                var arr:Array = [];
                for each(var itm:ISelectable in value){
                    arr.push(itm.dataId);
                }
                return arr;
        }
    }
}

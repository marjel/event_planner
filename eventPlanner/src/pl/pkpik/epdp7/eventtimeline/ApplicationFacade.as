package pl.pkpik.epdp7.eventtimeline
{
	import org.puremvc.as3.patterns.facade.Facade;

    import pl.pkpik.epdp7.eventtimeline.controller.command.ApplicationButtonCommand;

    import pl.pkpik.epdp7.eventtimeline.controller.command.ApplicationStartupCommand;
	import pl.pkpik.epdp7.eventtimeline.controller.command.ChangeShiftAssignmentCommand;
	import pl.pkpik.epdp7.eventtimeline.controller.command.ContextMenuCommand;
	import pl.pkpik.epdp7.eventtimeline.controller.command.InitializExternalInterfaceCommand;
    import pl.pkpik.epdp7.eventtimeline.controller.command.LoadBrokenRulesCommand;
	import pl.pkpik.epdp7.eventtimeline.controller.command.RefreshDataCommand;
	import pl.pkpik.epdp7.eventtimeline.controller.command.RefreshFreeDaysCommand;
    import pl.pkpik.epdp7.eventtimeline.controller.command.RefreshOffDaysCommand;
    import pl.pkpik.epdp7.eventtimeline.controller.command.SelectObjectCommand;
    import pl.pkpik.epdp7.eventtimeline.controller.command.SetAdditionalEventsDataCommand;
    import pl.pkpik.epdp7.eventtimeline.controller.command.SetBrokenRulesCommand;
	import pl.pkpik.epdp7.eventtimeline.controller.command.SetEventsDataCommand;
	import pl.pkpik.epdp7.eventtimeline.controller.command.SetMeasurementCommand;
    import pl.pkpik.epdp7.eventtimeline.controller.command.UIRefreshedCommand;
    import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
	
	public final class ApplicationFacade extends Facade
	{
		
		public static const NAME:String = "EventPlanner";
		
		public static function getInstance() : ApplicationFacade 
		{
			if (instance == null) {
				instance = new ApplicationFacade();
			}
			return instance as ApplicationFacade;
		}
		
		public function startup( application:EventPlanner ):void
		{
			sendNotification( ApplicationCommands.STARTUP, application );
		}
		
		override protected function initializeController( ) : void 
		{
			
			super.initializeController(); 
			
			registerCommand( ApplicationCommands.STARTUP, ApplicationStartupCommand );
			registerCommand(ApplicationCommands.EXTERNAL_INTERFACE_INIT, InitializExternalInterfaceCommand);
			registerCommand( ApplicationCommands.SET_EVENTS_DATA, SetEventsDataCommand );
			registerCommand( ApplicationCommands.CHANGE_SHIFT_ASSIGNMENT, ChangeShiftAssignmentCommand );

            registerCommand( ApplicationCommands.SET_ADDITIONAL_EVENTS_DATA, SetAdditionalEventsDataCommand );

			registerCommand( ApplicationCommands.REFRESH_DATA, RefreshDataCommand );
			registerCommand( ApplicationCommands.REFRESH_FREE_DAYS, RefreshFreeDaysCommand );
			registerCommand( ApplicationCommands.REFRESH_OFF_DAYS, RefreshOffDaysCommand );
			registerCommand( ApplicationCommands.SET_MEASUREMENT, SetMeasurementCommand );
			
			registerCommand( ApplicationCommands.SET_BROKEN_RULES, SetBrokenRulesCommand );
			registerCommand( ApplicationCommands.LOAD_BROKEN_RULES, LoadBrokenRulesCommand );

			registerCommand( ApplicationCommands.EXECUTE_CONTEXT_MENU, ContextMenuCommand );

			registerCommand( ApplicationCommands.SELECT_OBJECT, SelectObjectCommand);

			registerCommand( ApplicationCommands.UI_REFRESHED, UIRefreshedCommand);

			registerCommand( ApplicationCommands.RUN_APPLICATION_BUTTON_COMMAND, ApplicationButtonCommand);

		}
		
		public function ApplicationFacade()
		{
			super();
		}
	}
}
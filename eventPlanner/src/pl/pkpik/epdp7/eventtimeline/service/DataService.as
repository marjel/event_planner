package pl.pkpik.epdp7.eventtimeline.service
{
    import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.patterns.observer.Notifier;
	import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
    import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
    import pl.pkpik.epdp7.eventtimeline.controller.vo.QueueData;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.AdditionalEventsDataProxy;
    import pl.pkpik.epdp7.eventtimeline.util.CallLaterProxy;

    public final class DataService extends Notifier
	{
		private static const instance:DataService = new DataService();
        private var _actions:VectorCollection;

        private var _current:QueueData;
		
		public static function getInstance():DataService
		{
			return instance;
		}
		
		public function DataService()
		{
            _actions = new VectorCollection(new Vector.<QueueData>());
		}

        public function get actions():VectorCollection
        {
            return _actions;
        }
		
		public function setBrokenRules(value:String, execute:Boolean=false):void
		{
            if(execute){
                sendNotification(ApplicationCommands.SET_BROKEN_RULES,  XML(value));
            }else{
                var qd:QueueData = new QueueData(this, setBrokenRules, value);
                actions.addItem(qd);
                notifyAction(1, "parsing broken rules...", qd);
            }
		}
		
		public function setMeasurement(value:String, execute:Boolean=false):void
		{
            if(execute){
                sendNotification(ApplicationCommands.SET_MEASUREMENT,  XML(value));
            }else{
                var qd:QueueData = new QueueData(this, setMeasurement, value);
                actions.addItem(qd);
                notifyAction(1, "parsing a measurement data...", qd);
            }
		}
		
		public function updatePartialDayOff(value:String, execute:Boolean=false):void
        {
            if(execute){
                sendNotification(ApplicationCommands.REFRESH_OFF_DAYS,  XML(value));
            }else{
                var qd:QueueData = new QueueData(this, updatePartialDayOff, value);
                actions.addItem(qd);
                notifyAction(1, "refreshing off days...", qd);
            }

        }
		public function refreshFreeDays(value:String, execute:Boolean=false):void
		{
            if(execute){
                sendNotification(ApplicationCommands.REFRESH_FREE_DAYS,  XML(value));
            }else{
                var qd:QueueData = new QueueData(this, refreshFreeDays, value);
                actions.addItem(qd);
                notifyAction(1, "refreshing free days", qd);

            }

		}
		
		public function refreshData(value:String, execute:Boolean=false):void
		{
            if(execute){
                sendNotification(ApplicationCommands.REFRESH_DATA,  XML(value));
            }else{
                var qd:QueueData = new QueueData(this, refreshData, value);

                actions.addItem(qd);
                notifyAction(1, "Data refreshing...", qd);

            }
		}
		
		public function setAdditionalData(value:String, execute:Boolean=false):void
        {

            if(execute){
                sendNotification(ApplicationCommands.SET_ADDITIONAL_EVENTS_DATA,  XML(value));
            }else{
                var edp:AdditionalEventsDataProxy = facade.retrieveProxy(AdditionalEventsDataProxy.NAME) as AdditionalEventsDataProxy;
                if(edp.eventsCollection.length==2){
                    sendNotification(ApplicationCommands.SHOW_NOTIFICATION, "Załadowano maksymalną liczbę planów dodatkowych");
                    sendNotification(ApplicationCommands.UI_REFRESHED, "<font face='DinBold' color='#990000'>Maksymalnie można dodać 2 plany archiwalne.</font>.");
                    return;
                }
                var qd:QueueData = new QueueData(this, setAdditionalData, value);

                actions.addItem(qd);
                //if(edp.eventsCollection.length==1){
                    notifyAction(1, "Parsing additional data...", qd);
                //}
            }

        }
		public function setData(value:String, execute:Boolean=false):void
		{
            if(execute){
                sendNotification(ApplicationCommands.SET_EVENTS_DATA,  XML(value));
            }else{
                var qd:QueueData = new QueueData(this, setData, value);
                additionalEventsDataProxy.clearData();
                actions.removeAll();
                actions.addItem(qd);
                actions.refresh();
                CallLaterProxy.GetInstance().removeAllActions();
                notifyAction(1, "Parsing global data...", qd);

            }
		}
        private function get additionalEventsDataProxy():AdditionalEventsDataProxy
        {
            return facade.retrieveProxy(AdditionalEventsDataProxy.NAME) as AdditionalEventsDataProxy;
        }

        private function notifyAction(delay:int, description:String, qd:QueueData):void
        {
            if(actions.length==1){
                current = qd;
                callFunctionLater(new ActionData(sendNotification, this, [ApplicationCommands.UI_REFRESHED],
                        delay, -1, "<font face='DinBold' color='#990000'>" + description + "</font>"));

                //sendNotification(ApplicationCommands.UI_REFRESHED, "<font face='DinBold' color='#990000'>" + description + "</font>")
            }
        }


        public function get current():QueueData
        {
            return _current;
        }

        public function set current(value:QueueData):void
        {
            _current = value;
        }
    }
}

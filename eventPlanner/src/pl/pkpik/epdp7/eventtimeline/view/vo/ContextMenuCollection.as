package pl.pkpik.epdp7.eventtimeline.view.vo {


    import org.apache.flex.collections.VectorCollection;
    import flash.geom.Point;

    import pl.pkpik.epdp7.eventtimeline.view.component.ISelectable;

    public final class ContextMenuCollection extends VectorCollection {

        private var _header:String;
        private var _mousePosition:Point;

        public function ContextMenuCollection(source:* = null)
        {
            _mousePosition = new Point();
            super(source);
        }

        private function getSelectionsArray(sel:VectorCollection):Array
        {
            if (sel == null) return null;
            var arr:Array = [];
            for each(var item:ISelectable in sel){
                arr.push(item.dataId);
            }
            return arr;

        }

        public function setData(h:String, mouseX:Number, mouseY:Number, data:Array, selections:VectorCollection=null):void
        {
            _header = h;
            mousePosition.x = mouseX;
            mousePosition.y = mouseY;
            this.removeAll();
            var arr:Array = getSelectionsArray(selections);
            for each(var itm:ContextMenuVO in data){
                if(arr){
                    itm.selections = arr;
                }

                this.addItem(itm);
            }
        }

        public function get header():String
        {
            return _header;
        }

        public function get mousePosition():Point {
            return _mousePosition;
        }
    }
}

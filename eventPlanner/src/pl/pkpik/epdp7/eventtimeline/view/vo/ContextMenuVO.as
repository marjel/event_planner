package pl.pkpik.epdp7.eventtimeline.view.vo {
    public final class ContextMenuVO {

        public var label:String;
        public var name:String;
        public var type:String;
        public var data:Object;
        public var selections:Array;


        function ContextMenuVO(n:String, l:String, t:String, d:Object=null)
        {
            this.name = n;
            this.label = l;
            this.type = t;
            this.data = d;
        }

        public function toString():String {
            return "ContextMenuVO{label=" + String(label) + ",name=" + String(name) + ",type=" + String(type) + ",data=" + String(data) + "}";
        }
    }
}

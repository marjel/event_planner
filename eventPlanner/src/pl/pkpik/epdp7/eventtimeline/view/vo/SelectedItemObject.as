package pl.pkpik.epdp7.eventtimeline.view.vo {
    import org.apache.flex.collections.VectorCollection;

    public final class SelectedItemObject {

    public var type:String;
    public var data:VectorCollection;

    function SelectedItemObject(t:String, d:VectorCollection)
    {
        type = t;
        data = d;
    }
}
}

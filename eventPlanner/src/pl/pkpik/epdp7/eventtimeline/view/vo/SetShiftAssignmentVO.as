package pl.pkpik.epdp7.eventtimeline.view.vo {

    import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;
    import pl.pkpik.epdp7.eventtimeline.view.component.ShiftRectangle;

    public final class SetShiftAssignmentVO {

        public var employeeVO:EmployeeVO;
        public var rectangle:ShiftRectangle;

        function SetShiftAssignmentVO(empl:EmployeeVO, rect:ShiftRectangle)
        {
            employeeVO = empl;
            rectangle = rect;
        }

    }
}

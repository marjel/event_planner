package pl.pkpik.epdp7.eventtimeline.view.component
{
	import pl.pkpik.epdp7.eventtimeline.util.CustomToolTipManagerImpl;
	
	import mx.core.mx_internal;
	import mx.preloaders.SparkDownloadProgressBar;
	
	import pl.pkpik.epdp7.eventtimeline.util.XYAxisDragManagerImpl;

	use namespace mx_internal;

	public final class ApplicationPreloader extends SparkDownloadProgressBar
	{
		
		public function ApplicationPreloader()
		{
			super();
			registerCustomClasses();
		}
		
		protected function registerCustomClasses():void
		{
			mx.core.Singleton.registerClass("mx.managers::IToolTipManager2", CustomToolTipManagerImpl);
			mx.core.Singleton.registerClass("mx.managers::IDragManager", XYAxisDragManagerImpl);
		}
	}
}
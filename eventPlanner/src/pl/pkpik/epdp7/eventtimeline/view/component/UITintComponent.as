package pl.pkpik.epdp7.eventtimeline.view.component {
import flash.display.DisplayObject;
    import flash.events.Event;

    import pl.pkpik.epdp7.eventtimeline.view.util.DisplayObjectUtils;

import spark.components.Group;

public class UITintComponent extends Group{

        private var _tint:uint;

        public function UITintComponent() {
        }

        [Bindable]
        public function get tint():int {
            return _tint;
        }
        override public function dispatchEvent(evt:Event):Boolean {
            if (hasEventListener(evt.type) || evt.bubbles) {
                return super.dispatchEvent(evt);
            }
            return true;
        }
        public function set tint(value:int):void {
            if(_tint != value){
                _tint = value;
                DisplayObjectUtils.setTint(this as DisplayObject, value);
            }

        }
    }
}

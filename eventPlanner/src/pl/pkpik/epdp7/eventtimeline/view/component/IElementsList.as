package pl.pkpik.epdp7.eventtimeline.view.component {
import spark.components.supportClasses.ItemRenderer;

public interface IElementsList {
        function getItemById(value:int):ISelectable;
        function getItemByIndex(value:int):ISelectable;
        function getItemRenderer(value:Object):ItemRenderer;
    }
}

package pl.pkpik.epdp7.eventtimeline.view.component {
    public interface IContextMenuComponent {
        function get contextMenuData():Array;
    }
}

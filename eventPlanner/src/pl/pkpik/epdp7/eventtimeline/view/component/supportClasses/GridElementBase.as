/**
 * Created by mariel on 25.03.15.
 */
package pl.pkpik.epdp7.eventtimeline.view.component.supportClasses {

    import flash.events.Event;

    import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;

    import spark.components.Group;
    import spark.primitives.Rect;

    public class GridElementBase extends Group {

        private var _employee:EmployeeVO;
        private var _minutesShift:Number;
        private var _dayIndex:Number;
        private var _startMinute:Number;
        private var _employeeHight:Number;
        protected var _minuteSize:Number;
        protected var _daySize:Number;
        protected var _disabledTime:Number;
        protected var eventRectangle:Rect;


        override public function set height(value:Number):void
        {
        }

        override public function get height():Number
        {

            return eventRectangle.height;
        }

        override public function set width(value:Number):void
        {
        }

        override public function get width():Number
        {
            return eventRectangle.width;
        }

        public function get index():int
        {
            if (_employee != null) {
                return _employee.index;
            }
            return -1;
        }

        public function set index(value:int):void
        {
            if (_employee != null) {
                _employee.index = value;
            }
        }

        public function get employeeHight():Number
        {
            return _employeeHight;
        }

        public function set employeeHight(value:Number):void
        {
            _employeeHight = value;
        }

        public function get startMinute():Number
        {
            return _startMinute;
        }

        public function set startMinute(value:Number):void
        {
            _startMinute = value;
        }

        public function set dayIndex(value:int):void
        {
            _dayIndex = value;
        }
        public function get dayIndex():int
        {
            return -1;
        }

        public function get isOutOfPlan():Boolean
        {
            return this.x < ((_daySize / 24) / 60) * _disabledTime;
        }

        public function setDisabledTime(value:Number):void
        {
            _disabledTime = value;
        }
        public function get minutesShift():Number {
            return _minutesShift;
        }

        public function set minutesShift(value:Number):void {
            _minutesShift = value;
        }
        public function get employee():EmployeeVO
        {
            return _employee;
        }

        public function set employee(value:EmployeeVO):void
        {
            if(_employee != value){
                _employee = value;
            }

        }

        public function get daySize():Number
        {
            return _daySize;
        }

        public function set daySize(value:Number):void
        {

            if (_daySize != value) {
                _daySize = value;
                _minuteSize = (_daySize / 24) / 60;
                redrawX();
            }
        }
        override public function dispatchEvent(evt:Event):Boolean {
            if (hasEventListener(evt.type) || evt.bubbles) {
                return super.dispatchEvent(evt);
            }
            return true;
        }
        protected function redrawX():void
        {
        }

        public function GridElementBase()
        {
            super();
        }
    }
}

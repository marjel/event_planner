package pl.pkpik.epdp7.eventtimeline.view.component {
import flash.display.Graphics;
import flash.events.Event;
import mx.core.UIComponent;

import org.apache.flex.collections.VectorCollection;

import pl.pkpik.epdp7.eventtimeline.view.util.DisplayObjectUtils;


import pl.pkpik.epdp7.eventtimeline.view.util.DrawingUtils;

public final class PlannerTimeGrid extends UIComponent {

    public var backgroundColor:uint = 0xffffff;
    public var expanded:Boolean = false;

    private var _elementWidth:Number;
    private var _elementHeight:Number;

    public var rows:uint = 30;
    public var columns:uint = 30;

    private var grid:UIComponent;
    private var selection:UIComponent;
    private var background:UIComponent;
    private var nightTimeContainer:UIComponent;
    private var offDaysContainer:UIComponent;
    private var freeDaysContainer:UIComponent;
    private var container:UIComponent;
    private var disableAreaContainer:UIComponent;

    private var additionalShiftsContainer0:UIComponent;
    private var additionalShiftsContainer1:UIComponent;

    private var groupsContainers:VectorCollection = new VectorCollection(new Vector.<IndexedVGroup>());

    private var verticalSelections:VectorCollection = new VectorCollection(new Vector.<UIComponent>());
    private var horizontalSelections:VectorCollection = new VectorCollection(new Vector.<UIComponent>());
    private var shiftsRectangles:VectorCollection = new VectorCollection(new Vector.<ShiftRectangle>());
    private var freeDaysRectangles:VectorCollection = new VectorCollection(new Vector.<FreeDayRectangle>());
    private var additionalShiftsRectangles:VectorCollection = new VectorCollection(new Vector.<AdditionalShiftRectangle>());

    public static const DAY_SHIFTS_OVERLOADED:String = "DayShiftsOverloaded";


    override protected function createChildren():void {

        addChild(background);

        addChild(additionalShiftsContainer0);
        addChild(additionalShiftsContainer1);
        addChild(nightTimeContainer);
        addChild(disableAreaContainer);
        addChild(freeDaysContainer);
        addChild(offDaysContainer);
        addChild(selection);
        addChild(container);


        addChild(grid);

        grid.mouseEnabled =
                grid.mouseFocusEnabled =
                        grid.mouseChildren = false;


    }



    private function drawAdditionalDataSeparator(g:Graphics):void {

        g.clear();
        var i:uint;
        var ypos:Number = 0;
        var w:Number = this.width;
        var h:Number = this.elementHeight;
        for (i = 0; i < rows; i++) {
            g.lineStyle(1, 0x993333, 1, true, "none", "square", "miter");
            DrawingUtils.DrawDash(g, 0, ypos + 1, w, ypos + 1);
            g.lineStyle();
            g.beginFill(0xfaf0f0);
            g.drawRect(1, ypos + 2, w - 1, 30);
            ypos += h;
        }
    }


    private function drawGrid():void {
        var gr:Graphics = this.grid.graphics;
        var w:Number = _elementWidth;
        var h:Number = _elementHeight;
        if ((w > 0) && (h > 0)) {

            var wd:Number = width;
            var hg:Number = height;

            var distance:Number;
            var i:uint;
            gr.clear();
            gr.lineStyle(1, 0xababab, 1, true, "none", "square", "miter");
            for (i = 0; i <= rows; i++) {
                distance = (h * i);
                gr.moveTo(0, distance);
                gr.lineTo(wd, distance);
            }
            for (i = 1; i <= columns; i++) {
                distance = (w * i);
                gr.moveTo(distance, 0);
                gr.lineTo(distance, hg);
            }
        } else {
            gr.clear();
        }
        DrawingUtils.drawRectangle(this.background.graphics, width, height, backgroundColor, 1);
        if(additionalShiftsRectangles){
            drawAdditionalDataSeparator(additionalShiftsContainer0.graphics);
            drawAdditionalDataSeparator(additionalShiftsContainer1.graphics);
        }

    }

    private function refreshElementsYPositions():void {
        var i:uint;
        var len:uint = nightTimeContainer.numChildren;
        var h:Number = _elementHeight;
        for (i = 0; i < len; i++) {
            (nightTimeContainer.getChildAt(i) as NightContractRectangle).employeeHight = h;
        }
        len = freeDaysContainer.numChildren;
        for (i = 0; i < len; i++) {
            (freeDaysContainer.getChildAt(i) as FreeDayRectangle).employeeHight = h;
        }
        len = offDaysContainer.numChildren;
        for (i = 0; i < len; i++) {
            (offDaysContainer.getChildAt(i) as OffDayRectangle).employeeHight = h;
        }
    }

    private function refreshGroupsPositions():void {
        var h:Number = _elementHeight;
        for each(var group:IndexedVGroup in groupsContainers) {
            group.y = group.vIndex * h;
        }
    }

    private function removeGroups():void {
        for each(var group:IndexedVGroup in groupsContainers) {
            container.removeChild(group);
        }
        groupsContainers.removeAll();
    }

    private function setGroupsContainers():void {
        var i:uint;
        var j:uint;
        var group:IndexedVGroup;
        var h:Number = _elementHeight;
        for (i = 0; i < rows; i++) {
            for (j = 0; j < columns; j++) {
                group = new IndexedVGroup();
                group.y = i * h;
                group.vIndex = i;
                group.hIndex = j;
                groupsContainers.addItem(group);
                container.addChild(group);

            }
        }

    }

    private function drawVerticalSelection(gr:Graphics):void {
        DrawingUtils.drawRectangle(gr, _elementWidth, height, 0x3275d2);
    }

    private function drawHorizontalSelection(gr:Graphics):void {
        DrawingUtils.drawRectangle(gr, width, _elementHeight, 0x3275d2);
    }

    public function setVerticalSelection(index:uint):void {
        var sel:UIComponent = new UIComponent();
        sel.mouseFocusEnabled = sel.mouseEnabled = false;
        verticalSelections.addItem(sel);
        sel.name = "v" + String(index);
        sel.x = index * _elementWidth;
        drawVerticalSelection(sel.graphics);
        selection.addChild(sel);

    }

    public function removeVerticalSelection(index:uint):void {
        var sel:UIComponent = (selection.getChildByName("v" + String(index)) as UIComponent);
        if (sel) {
            verticalSelections.removeItem(sel);
            selection.removeChild(sel);
        }
    }

    public function removeAllVerticalSelections():void {
        for each(var sel:UIComponent in verticalSelections) {
            selection.removeChild(sel);
        }
        verticalSelections.removeAll();

    }

    public function removeAllHorizontalSelections():void {
        for each (var sel:UIComponent in horizontalSelections) {
            selection.removeChild(sel);
        }
        horizontalSelections.removeAll();
    }

    public function setHorizontalSelection(index:uint):void {
        var sel:UIComponent = new UIComponent();
        sel.mouseFocusEnabled = sel.mouseEnabled = false;
        horizontalSelections.addItem(sel);
        sel.name = "h" + String(index);
        sel.y = index * _elementHeight;
        drawHorizontalSelection(sel.graphics);
        selection.addChild(sel);
    }

    public function removeHorizontalSelection(index:uint):void {
        var sel:UIComponent = (selection.getChildByName("h" + String(index)) as UIComponent);
        if (sel) {
            horizontalSelections.removeItem(sel);
            selection.removeChild(sel);
        }

    }


    public function setNightTime(value:NightContractRectangle):void {
        if (!this.nightTimeContainer.contains(value)) {
            this.nightTimeContainer.addChild(value);
        }

    }



    public function setAdditionalShiftRectangle(value:AdditionalShiftRectangle, index:uint):void {
        var container:UIComponent = index == 0 ? additionalShiftsContainer0 : additionalShiftsContainer1;
        if (!container.visible) {
            container.visible = true;
        }
        if (!container.contains(value)) {
            additionalShiftsRectangles.addItem(value);
            container.addChild(value);
        }
    }


    public function removeAdditionalShiftRectangles():void {
        var r:AdditionalShiftRectangle
        for each(r in additionalShiftsRectangles) {
            if (r.parent) {
                r.parent.removeChild(r);
            }
        }
        additionalShiftsRectangles.removeAll();
    }


    public function setOffDay(value:OffDayRectangle):void {
        if (!this.offDaysContainer.contains(value)) {
            this.offDaysContainer.addChild(value);
        }

    }


    public function removeOffDay(value:OffDayRectangle):void {
        if (this.offDaysContainer.contains(value)) {
            this.offDaysContainer.removeChild(value);
        }

    }


    public function setFreeDay(value:FreeDayRectangle):void {
        if (!this.freeDaysContainer.contains(value)) {
            value.addEventListener(Event.ADDED_TO_STAGE, function (event:Event):void {
                freeDaysRectangles.addItem(event.target as FreeDayRectangle);
            });
            value.addEventListener(Event.REMOVED_FROM_STAGE, function (event:Event):void {
                freeDaysRectangles.removeItem(event.target as FreeDayRectangle);
            });
            this.freeDaysContainer.addChild(value);
        }

    }


    public function removeFreeDay(value:FreeDayRectangle):void {
        if (this.freeDaysContainer.contains(value)) {
            this.freeDaysContainer.removeChild(value);
        }

    }


    public function getShiftsContentHeight(rowID:int):Number {
        var max:Number = 0;
        var group:IndexedVGroup;
        for each(group in groupsContainers) {
            if (group.vIndex == rowID) {
                if (group.height > max) {
                    max = group.height;
                }
            }
        }
        return max;
    }

    private function hitTest(rectangle1:ShiftRectangle, rectangle2:ShiftRectangle):Boolean {
        return (rectangle1.x <= rectangle2.x) ?
                (rectangle1.rightPosition > rectangle2.x) :
                (rectangle2.rightPosition > rectangle1.x);
    }

    private function validateOldGroupPositions(group:IndexedVGroup, rectangle:ShiftRectangle, index:int = 0):void
    {
        if (group.length < index) {
            return;
        }
        var v:Vector.<ShiftRectangle> = new Vector.<ShiftRectangle>().concat(group.getChildrenByGroupIndex(index + 1));
        if (!v || v.length == 0) {
            return;
        }
        var r:ShiftRectangle;
        var isSet:Boolean = false;
        for each(r in v) {
            if (hitTest(rectangle, r)) {
                isSet = setShiftPositions(r, group, index, r.isAssignedToActualEmployee, false);
                if (isSet) {
                    r.groupIndex = index;
                    break;
                }

            }
        }
        validateOldGroupPositions(group, isSet ? r : rectangle, index + 1);
    }

        private function getShiftsArrayToValidatePosition(group:IndexedVGroup, index:int):Vector.<ShiftRectangle> {
        var v:Vector.<ShiftRectangle> = new Vector.<ShiftRectangle>().concat(group.getChildrenByGroupIndex(index));
        var newGrouop:IndexedVGroup;
        var rect:ShiftRectangle;
        if (group.hIndex > 0) {
            newGrouop = getGroupByIndicies(group.vIndex, group.hIndex - 1);
            if(newGrouop){
                rect = newGrouop.getLastElementByGroupIndex(index);
                if(rect!=null){
                    v.push(rect);
                }
            }
        }
        if (group.hIndex < columns - 1) {
            newGrouop = getGroupByIndicies(group.vIndex, group.hIndex + 1);
            if(newGrouop){
                rect = newGrouop.getFirstElementByGroupIndex(index);
                if(rect!=null){
                    v.push(rect);
                }
            }
        }
        return v;
    }

    private function setShiftPositions(rectangle:ShiftRectangle, group:IndexedVGroup, index:uint, isOnPlan:Boolean, dispatch:Boolean = true):Boolean {
        if (isOnPlan) {
            if (index > 2) {
                if(dispatch){
                    dispatchEvent(new Event(DAY_SHIFTS_OVERLOADED, true));
                }
                return false;
            }
        }
        var v:Vector.<ShiftRectangle> = getShiftsArrayToValidatePosition(group, index);
        var rect:ShiftRectangle;
        var isSet:Boolean = true;
        for each(rect in v){
            if(hitTest(rectangle, rect)){
                isSet = false;
                break;
            }
        }
        if(isSet){
            rectangle.groupIndex = index;
            return true;
        }
        return setShiftPositions(rectangle, group, index+1, isOnPlan, dispatch);
    }
    private function validateNewShiftPositions(rectangle:ShiftRectangle, group:IndexedVGroup, isOnPlan:Boolean = false):Boolean {
        return setShiftPositions(rectangle, group, 0, isOnPlan);
    }

        private function getGroupByIndicies(vInd:int, hInd:int):IndexedVGroup {
            vInd = vInd < 0 ? 0 : vInd;
            for each(var itm:IndexedVGroup in groupsContainers) {
                if ((itm.hIndex == hInd) && (itm.vIndex == vInd)) {
                    return itm;
                }
            }
            return null;
        }

        public function getFreeDaysRectanglesByDayIndex(index:int, property:String = null, value:Boolean = false):VectorCollection {
            if (freeDaysRectangles.length == 0) {
                return null;
            }
            var collection:VectorCollection = new VectorCollection(new Vector.<FreeDayRectangle>());
            var freeDay:FreeDayRectangle;
            for each(freeDay in freeDaysRectangles) {
                if (freeDay.dayIndex == index) {
                    if (property != null) {
                        if (freeDay.hasOwnProperty(property)) {
                            freeDay[property] = Boolean(value);
                        }
                    }
                    collection.addItem(freeDay);
                }
            }
            return collection;
        }

        public function getFreeDaysRectanglesByEmployeeIndex(index:int, property:String = null, value:Boolean = false):VectorCollection {
            if (freeDaysRectangles.length == 0) {
                return null;
            }
            var collection:VectorCollection = new VectorCollection(new Vector.<FreeDayRectangle>());
            var freeDay:FreeDayRectangle;
            for each(freeDay in freeDaysRectangles) {
                if (freeDay.index == index) {
                    if (property != null) {
                        if (freeDay.hasOwnProperty(property)) {
                            freeDay[property] = value;
                        }
                    }
                    collection.addItem(freeDay);
                }
            }
            return collection;
        }

        public function getShiftsRectanglesByDayIndex(index:int, property:String = null, value:Boolean = false):VectorCollection {
            if (shiftsRectangles.length == 0) {
                return null;
            }
            var collection:VectorCollection = new VectorCollection(new Vector.<ShiftRectangle>());
            var shiftRectangle:ShiftRectangle;
            for each(shiftRectangle in shiftsRectangles) {
                if (shiftRectangle.dayIndex == index) {
                    if (property != null) {
                        if (shiftRectangle.hasOwnProperty(property)) {
                            shiftRectangle[property] = value;
                        }
                    }
                    collection.addItem(shiftRectangle);
                }
            }
            return collection;
        }

        public function getShiftsRectanglesByEmployeeIndex(index:int, property:String = null, value:Boolean = false):VectorCollection {
            if (shiftsRectangles.length == 0) {
                return null;
            }
            var collection:VectorCollection = new VectorCollection(new Vector.<ShiftRectangle>());
            var shiftRectangle:ShiftRectangle;
            for each(shiftRectangle in shiftsRectangles) {
                if (shiftRectangle.index == index) {
                    if (property != null) {
                        if (shiftRectangle.hasOwnProperty(property)) {
                            shiftRectangle[property] = value;
                        }
                    }
                    collection.addItem(shiftRectangle);
                }
            }
            return collection;
        }

        public function setElement(value:ShiftRectangle):Boolean {

            var added:Boolean = true;
            var parentGroup:IndexedVGroup = value.parent as IndexedVGroup;
            var oldIndex:int= value.groupIndex;
            var group:IndexedVGroup = getGroupByIndicies(value.index, value.dayIndex);
            if (group) {
                if (!shiftsRectangles.contains(value)) {
                    shiftsRectangles.addItem(value);
                }
                added = validateNewShiftPositions(value, group, value.isAssignedToActualEmployee);
                if (added) {
                    if(parentGroup){
                        parentGroup.removeChild(value);
                    }
                    group.addChild(value);
                    if (parentGroup) {
                        validateOldGroupPositions(parentGroup, value, oldIndex);
                    }
                }
            }
            return added;
        }

        private var _plannerStartMinute:int = 0;

        public function setDisableArea(startMinute:int):void {
            _plannerStartMinute = startMinute;
            disableAreaContainer.x = 1;
            var g:Graphics = disableAreaContainer.graphics;
            g.clear();
            if (startMinute > 0) {

                var minuteSize:Number = (_elementWidth / 24) / 60;
                g.beginFill(0x666666, .25);
                g.drawRect(0, 0, (startMinute * minuteSize) - 1, this.height);
                g.endFill();
            }
        }


        public function removeElement(value:ShiftRectangle):void {
            if (shiftsRectangles.contains(value)) {
                shiftsRectangles.removeItem(value);
            }
            var group:IndexedVGroup = getGroupByIndicies(value.index, value.dayIndex);
            if (group) {
                if (group.contains(value)) {
                    group.removeChild(value);
                }
            }
        }

        public function setGrid(r:uint, c:uint, w:Number, h:Number):void {
            rows = r;
            columns = c;
            _elementWidth = w;
            _elementHeight = h;
            drawGrid();
            setGroupsContainers();
        }
        public function disposeGrid():void
        {
            removeAdditionalShiftRectangles();
            removeAllVerticalSelections();
            removeAllHorizontalSelections();

            removeGroups();

            removeAllChildren();
        }
        private function removeAllChildren():void {



            DisplayObjectUtils.removeAllChildren(nightTimeContainer);

            removeChild(background);

            removeChild(additionalShiftsContainer0);
            removeChild(additionalShiftsContainer1);
            removeChild(nightTimeContainer);
            removeChild(disableAreaContainer);
            removeChild(freeDaysContainer);
            removeChild(offDaysContainer);
            removeChild(selection);
            removeChild(container);


            removeChild(grid);



        }
        override public function get height():Number {
            return _elementHeight * rows;
        }

        override public function set height(value:Number):void {
        }

        override public function get width():Number {
            return _elementWidth * columns;
        }

        override public function set width(value:Number):void {
        }

        public function PlannerTimeGrid() {
            super();
            grid = new UIComponent();
            background = new UIComponent();
            selection = new UIComponent();
            nightTimeContainer = new UIComponent();
            offDaysContainer = new UIComponent();
            freeDaysContainer = new UIComponent();
            container = new UIComponent();
            disableAreaContainer = new UIComponent();
            additionalShiftsContainer0 = new UIComponent();
            additionalShiftsContainer1 = new UIComponent();
            additionalShiftsContainer0.mouseFocusEnabled =
                    additionalShiftsContainer0.mouseChildren = additionalShiftsContainer0.mouseEnabled =
                            additionalShiftsContainer1.mouseFocusEnabled = additionalShiftsContainer1.mouseChildren =
                                    additionalShiftsContainer1.mouseEnabled =
                                            grid.mouseFocusEnabled = grid.mouseChildren = grid.mouseEnabled =
                                                    background.mouseFocusEnabled = background.mouseChildren = background.mouseEnabled =
                                                            selection.mouseFocusEnabled = selection.mouseChildren = selection.mouseEnabled = false;

            additionalShiftsContainer0.cacheAsBitmap =
                    additionalShiftsContainer1.cacheAsBitmap = true;

            additionalShiftsContainer1.visible =
                    additionalShiftsContainer0.visible = false;

            additionalShiftsContainer0.y = 60;
            additionalShiftsContainer1.y = 90;

        }

        public function get elementWidth():Number {
            return _elementWidth;
        }

        public function set elementWidth(value:Number):void {
            if (_elementWidth != value) {
                _elementWidth = value;
                drawGrid();

               (this.owner as UIComponent).invalidateDisplayList();
                setDaySizeForElements(value);
                resizeShiftsRectangles();
                resizeAdditionalShiftsRectangles();
            }

        }

        public function get elementHeight():Number {
            return _elementHeight;
        }

        public function setElementHeight(value:Number):void {
            if (_elementHeight != value) {
                elementHeight = value;
                (this.owner as UIComponent).invalidateDisplayList();
            }
        }

        public function set elementHeight(value:Number):void {
            if (_elementHeight != value) {
                _elementHeight = value;
                drawGrid();
                refreshGroupsPositions();
                resizeHeightAdditionalShiftsRectangles();
                refreshElementsYPositions();
                redrawSelections();
                setDisableArea(_plannerStartMinute);

            }
        }

        private function redrawSelections():void {
            var index:uint;
            var item:UIComponent;
            if (verticalSelections.length) {
                for each(item in verticalSelections) {
                    index = int(item.name.substr(1));
                    item.x = index * _elementWidth;
                    drawVerticalSelection(item.graphics);
                }
            }
            if (horizontalSelections.length) {
                for each(item in horizontalSelections) {
                    index = int(item.name.substr(1));
                    item.y = index * _elementHeight;
                    drawHorizontalSelection(item.graphics);
                }
            }
        }

        private function resizeHeightAdditionalShiftsRectangles():void {
            if (additionalShiftsRectangles.length) {
                var shift:AdditionalShiftRectangle;
                for each(shift in additionalShiftsRectangles) {
                    shift.employeeHight = _elementHeight;
                }
            }
        }

        private function resizeAdditionalShiftsRectangles():void {
            if (additionalShiftsRectangles.length) {
                var shift:AdditionalShiftRectangle;
                for each(shift in additionalShiftsRectangles) {
                    shift.daySize = _elementWidth;
                }
            }
        }

        private function resizeShiftsRectangles():void {
            var shift:ShiftRectangle;
            for each(shift in shiftsRectangles) {
                shift.daySize = _elementWidth;
            }
        }

        private function setDaySizeForElements(size:Number):void {
            var len:int = freeDaysContainer.numChildren;
            var i:uint;
            for (i = 0; i < len; i++) {
                (freeDaysContainer.getChildAt(i) as FreeDayRectangle).daySize = size;
            }
            len = offDaysContainer.numChildren;
            for (i = 0; i < len; i++) {
                (offDaysContainer.getChildAt(i) as OffDayRectangle).daySize = size;
            }
            len = nightTimeContainer.numChildren;
            for (i = 0; i < len; i++) {
                (nightTimeContainer.getChildAt(i) as NightContractRectangle).daySize = size;
            }
            redrawSelections();
            setDisableArea(_plannerStartMinute);

        }
}

}
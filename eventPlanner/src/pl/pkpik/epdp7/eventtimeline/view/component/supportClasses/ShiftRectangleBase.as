/**
 * Created by mariel on 25.03.15.
 */
package pl.pkpik.epdp7.eventtimeline.view.component.supportClasses {

    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftAssignmentVO;
    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftVO;
    import pl.pkpik.epdp7.eventtimeline.util.DateUtils;
    import pl.pkpik.epdp7.eventtimeline.util.MathUtils;
    import pl.pkpik.epdp7.eventtimeline.view.util.DrawingUtils;
    import spark.components.Label;
    import spark.primitives.Rect;

    public class ShiftRectangleBase extends GridElementBase {

        private var _data:ShiftVO;
        private var _isBlocked:Boolean;

        private var startTime:Label;
        private var endTime:Label;

        private var extendedBeforeRect:Rect;
        private var extendedAfterRect:Rect;
        private var isNightRect:Rect;


        private var _extMinuteAfter:int = 0;
        private var _extMinuteBefore:int = 0;

        private const minExtendedRectSize:Number = 2.0;
        private var _shiftAssignment:ShiftAssignmentVO;



        public function get data():ShiftVO
        {
            return _data;
        }
        public function set data(value:ShiftVO):void
        {
            if(_data!=value){
                _data = value;
                startTime.text = DateUtils.getTimeString(value.startDate);
                endTime.text = DateUtils.getTimeString(value.endDate);
                redrawX();
            }
        }

        public function get shiftAssignment():ShiftAssignmentVO
        {
            return _shiftAssignment;
        }

        public function set shiftAssignment(value:ShiftAssignmentVO):void
        {
            if(value && (shiftAssignment != value)){
                _shiftAssignment = value;
            }
            refreshShiftAssignments();
        }
        public function get isShiftNight():Boolean
        {
            return isNightRect.visible;
        }

        public function set isShiftNight(value:Boolean):void
        {
            if (isNightRect.visible != value) {
                isNightRect.includeInLayout =
                        isNightRect.visible = value;
            }
        }

        public function get isBlocked():Boolean
        {
            return _isBlocked;
        }

        public function set isBlocked(value:Boolean):void
        {
            _isBlocked = value;
            if (value) {
                this.alpha = .5;
            } else {
                this.alpha = 1.0;
            }
        }

        public function set extMinuteAfter(value:int):void
        {
            _extMinuteAfter = value;
            if (value) {
                if (extendedAfterRect == null) {
                    extendedAfterRect = DrawingUtils.getRectangle(height);
                    addElement(extendedAfterRect);
                }
                setExtendedAfterRectPos();
            } else {
                if (extendedAfterRect != null) {
                    removeElement(extendedAfterRect);
                    extendedAfterRect = null;
                }            }
        }

        public function set extMinuteBefore(value:int):void
        {
            _extMinuteBefore = value;
            if (value) {
                if (extendedBeforeRect == null) {
                    extendedBeforeRect = DrawingUtils.getRectangle(height);
                    addElement(extendedBeforeRect);
                }
                setExtendedBeforeRectPos();
            } else {
                if (extendedBeforeRect != null) {
                    removeElement(extendedBeforeRect);
                    extendedBeforeRect = null;
                }
            }
        }
        public function refreshShiftAssignments():void
        {
            this.isShiftNight = _shiftAssignment.isShiftNight;
            this.isBlocked = _shiftAssignment.isShiftBlocked;
            this.extMinuteAfter = (_shiftAssignment.extMinuteAfter) ? _shiftAssignment.extMinuteAfter : 0;
            this.extMinuteBefore = (_shiftAssignment.extMinuteBefore) ? _shiftAssignment.extMinuteBefore : 0;
        }

        override protected function redrawX():void
        {
            var r:Function = MathUtils.round;
            if (_data) {
                this.x = r(_minuteSize * startMinute) + r(_minuteSize * _data.startMinute) + r(_minuteSize * minutesShift);
                eventRectangle.width = r(_minuteSize * (_data.endMinute - _data.startMinute));
            }
            endTime.visible = startTime.visible = (eventRectangle.width > 65);
            endTime.width = eventRectangle.width - 4;
            setExtendedAfterRectPos();
            setExtendedBeforeRectPos();
        }


        private function setExtendedAfterRectPos():void
        {
            if (extendedAfterRect) {
                extendedAfterRect.width = MathUtils.round(_minuteSize * _extMinuteAfter);
                extendedAfterRect.x = this.width;
            }
        }

        private function setExtendedBeforeRectPos():void
        {
            if (extendedBeforeRect) {
                var w:Number = MathUtils.round(_minuteSize * _extMinuteBefore);
                extendedBeforeRect.width = w;
                extendedBeforeRect.x = -w;
            }
        }
        public function ShiftRectangleBase()
        {
            super();
        }
    }
}

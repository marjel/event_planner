package pl.pkpik.epdp7.eventtimeline.view.component {
    public interface IBreakable {
        function get broken():Boolean;
        function set broken(value:Boolean):void;
    }
}

package pl.pkpik.epdp7.eventtimeline.view.component {
import flash.events.MouseEvent;

import spark.components.HSlider;

public final class NoWheelSlider extends HSlider {

    override protected function system_mouseWheelHandler(event:MouseEvent):void
    {
    }
}
}

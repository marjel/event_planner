package pl.pkpik.epdp7.eventtimeline.view.component {
import flash.display.DisplayObject;
    import flash.events.Event;

    import mx.core.UIComponent;

import pl.pkpik.epdp7.eventtimeline.util.SortUtils;
    import pl.pkpik.epdp7.eventtimeline.view.util.DisplayObjectUtils;

    public final class IndexedVGroup extends UIComponent {

        public var vIndex:int;
        public var hIndex:int;



    private var _children:Vector.<ShiftRectangle>;

        override public function set height(value:Number):void {
        }
        override public function get height():Number {
            var rect:ShiftRectangle;
            var max:Number = 0;
            var vec:Vector.<ShiftRectangle> = SortUtils.VectorSortOn(_children, 'x', Array.NUMERIC, true);
            for each(rect in vec){
                if(rect.bottomPosition>max){
                    max = rect.bottomPosition;
                }
            }
            return max;
        }

        override public function addChild(child:DisplayObject):DisplayObject
        {
            var c:DisplayObject = super.addChild(child);
            _children.push(c as ShiftRectangle);
            return c;
        }
        override public function removeChild(child:DisplayObject):DisplayObject
        {
            if(contains(child as ShiftRectangle)){
                _children.splice(_children.indexOf(child as ShiftRectangle), 1);
                return super.removeChild(child);
            }

            return null;
        }
        function IndexedVGroup()
        {
            _children = new Vector.<ShiftRectangle>();
            super();
            this.mouseFocusEnabled = this.mouseEnabled = false;
        }

    public function get length():uint {
        return _children.length;
    }

        override public function dispatchEvent(evt:Event):Boolean {
            if (hasEventListener(evt.type) || evt.bubbles) {
                return super.dispatchEvent(evt);
            }
            return true;
        }

        public function getChildrenByGroupIndex(value:int):Vector.<ShiftRectangle> {
            var vec:Vector.<ShiftRectangle> = new Vector.<ShiftRectangle>();
            var rect:ShiftRectangle;
            for each(rect in _children){
                if(rect.groupIndex==value){
                    vec.push(rect);
                }
            }
            return vec;
        }

        public function getFirstElementByGroupIndex(value:int):ShiftRectangle {
            var vec:Vector.<ShiftRectangle> = getChildrenByGroupIndex(value);
            if(vec.length){
                SortUtils.VectorSortOn(vec, 'x', Array.NUMERIC);
                return vec[0] as ShiftRectangle;
            }
            return null;
        }

        public function getLastElementByGroupIndex(value:int):ShiftRectangle {
            var vec:Vector.<ShiftRectangle> = getChildrenByGroupIndex(value);
            var l:int = vec.length;
            if(l){
                SortUtils.VectorSortOn(vec, 'x', Array.NUMERIC);
                return vec[l-1];
            }
            return null;
        }

        public override function toString():String {
            return super.toString() + "{vIndex=" + String(vIndex) + ",hIndex=" + String(hIndex) + ",_children=" + String(_children) + "}";
        }
}
}

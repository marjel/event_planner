package pl.pkpik.epdp7.eventtimeline.view.component
{
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import mx.core.FlexGlobals;
    import mx.managers.PopUpManager;

    import spark.components.SkinnablePopUpContainer;

    public class Toast extends SkinnablePopUpContainer
    {
        public static const LONG_DELAY:int = 8000; // 8 seconds

        public static const SHORT_DELAY:int = 4000; // 4 seconds

        private var showing:Boolean;

        private var popupTimer:Timer;

        public function Toast(label:String, duration:int = SHORT_DELAY, notificationParent:DisplayObject = null)
        {
            super();
            _label = label;
            _duration = duration;
            _notificationParent = notificationParent ? notificationParent : FlexGlobals.topLevelApplication.root;

            init();
        }


        private var _label:String;

        [Bindable("change")]

        public function get label():String
        {
            return _label;
        }

        public function set label(value:String):void
        {
            if (_label == value)
                return;

            _label = value;

            if (hasEventListener(Event.CHANGE))
                dispatchEvent(new Event(Event.CHANGE));
        }

        private var _duration:int;

        public function get duration():int
        {
            return _duration;
        }

        public function set duration(value:int):void
        {
            if (_duration == value)
                return;

            _duration = value;
        }


        private var _notificationParent:DisplayObject;

        public function get notificationParent():DisplayObject
        {
            return _notificationParent;
        }

        private function init():void
        {
            // create a timer to control visibility
            popupTimer = new Timer(_duration, 1);
            popupTimer.addEventListener(TimerEvent.TIMER, popupTimer_handler, false, 0, true);
        }

        public static function makeInstance(label:String, duration:int = SHORT_DELAY, parent:DisplayObject = null):Toast
        {
            return new Toast(label, duration, parent);
        }

        public function show():void
        {
            if (popupTimer.running)
                popupTimer.stop();


            popupTimer.reset();
            popupTimer.start();

            if (!showing)
            {
                PopUpManager.addPopUp(this, _notificationParent);
                showing = true;
            }
        }

        public function hide():void
        {
            if (popupTimer.running)
                popupTimer.stop();

            // remove our notification
            if (showing)
            {
                PopUpManager.removePopUp(this);
                showing = false;
            }
        }

        public function dispose():void
        {
            if (showing)
            {
                PopUpManager.removePopUp(this);
                showing = false;
            }

            if (popupTimer)
            {
                if (popupTimer.hasEventListener(TimerEvent.TIMER))
                    popupTimer.removeEventListener(TimerEvent.TIMER, popupTimer_handler, false);

                popupTimer.stop();
                popupTimer = null;
            }
        }

        protected function popupTimer_handler(event:TimerEvent):void
        {
            hide();
        }

    }
}

package pl.pkpik.epdp7.eventtimeline.view.component
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.InteractiveObject;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.core.DragSource;
	import mx.core.IFlexModule;
	import mx.core.IUIComponent;
	import mx.core.UIComponent;
	import mx.core.mx_internal;
	import mx.effects.Move;
	import mx.effects.Zoom;
	import mx.events.DragEvent;
	import mx.events.EffectEvent;
	import mx.events.InterDragManagerEvent;
	import mx.events.InterManagerRequest;
	import mx.events.SandboxMouseEvent;
	import mx.managers.CursorManager;
	import mx.managers.DragManager;
	import mx.managers.ISystemManager;
	import mx.managers.SystemManager;
	import mx.modules.ModuleManager;
	import mx.styles.CSSStyleDeclaration;
	import mx.styles.IStyleManager2;
	import mx.styles.StyleManager;
	
	use namespace mx_internal;
	
	public class XYAxisDragProxy extends UIComponent
	{
		
		private var _xdirection:Boolean;
		private var _ydirection:Boolean;
		
		public function XYAxisDragProxy(dragInitiator:IUIComponent,
								  dragSource:DragSource, xdirection:Boolean = true, ydirection:Boolean = true)
		{
			super();

            this.mouseFocusEnabled = this.mouseEnabled = this.mouseChildren = false;

			_xdirection = xdirection;
			_ydirection = ydirection;
			
			this.dragInitiator = dragInitiator;
			this.dragSource = dragSource;
			
			var sm:ISystemManager = dragInitiator.systemManager.
				topLevelSystemManager as ISystemManager;
			
			var ed:IEventDispatcher = sandboxRoot = sm.getSandboxRoot();
			
			ed.addEventListener(MouseEvent.MOUSE_MOVE,
				mouseMoveHandler, true);
			
			ed.addEventListener(MouseEvent.MOUSE_UP,
				mouseUpHandler, true);
		}
		

		override public function initialize():void
		{
			super.initialize();

			dragInitiator.systemManager.getSandboxRoot().addEventListener(SandboxMouseEvent.MOUSE_UP_SOMEWHERE, 
				mouseLeaveHandler);

			if (!getFocus())
				setFocus();
		}

		override public function get styleManager():IStyleManager2
		{

			if (this.dragInitiator is IFlexModule)
				return StyleManager.getStyleManager(IFlexModule(dragInitiator).moduleFactory);
			
			return super.styleManager;
		}

		private var cursorClass:Class = null;

		private var cursorID:int = CursorManager.NO_CURSOR;

		private var lastMouseEvent:MouseEvent;

		private var sandboxRoot:IEventDispatcher;
		

		public var dragInitiator:IUIComponent;

		public var dragSource:DragSource;

		public var xOffset:Number;

		public var yOffset:Number;

		public var startX:Number;

		public var startY:Number;

		public var target:DisplayObject = null;

		public var action:String;

		public var allowMove:Boolean = true;
		

		public function showFeedback():void
		{
			var newCursorClass:Class = cursorClass;
			var styleSheet:CSSStyleDeclaration =
				styleManager.getMergedStyleDeclaration("mx.managers.DragManager");
			
			if (action == DragManager.COPY)
				newCursorClass = styleSheet.getStyle("copyCursor");
			else if (action == DragManager.LINK)
				newCursorClass = styleSheet.getStyle("linkCursor");
			else if (action == DragManager.NONE)
				newCursorClass = styleSheet.getStyle("rejectCursor");
			else
				newCursorClass = styleSheet.getStyle("moveCursor");
			
			if (newCursorClass != cursorClass)
			{
				cursorClass = newCursorClass;
				if (cursorID != CursorManager.NO_CURSOR)
					cursorManager.removeCursor(cursorID);
				cursorID = cursorManager.setCursor(cursorClass, 2, 0, 0);
			}
		}


		private function dispatchDragEvent(type:String, mouseEvent:MouseEvent, eventTarget:Object):void
		{
			var dragEvent:DragEvent = new DragEvent(type);
			var pt:Point = new Point();
			
			dragEvent.dragInitiator = dragInitiator;
			dragEvent.dragSource = dragSource;
			dragEvent.action = action;
			dragEvent.ctrlKey = mouseEvent.ctrlKey;
			dragEvent.altKey = mouseEvent.altKey;
			dragEvent.shiftKey = mouseEvent.shiftKey;
			pt.x = lastMouseEvent.localX;
			pt.y = lastMouseEvent.localY;
			pt = DisplayObject(lastMouseEvent.target).localToGlobal(pt);
			pt = DisplayObject(eventTarget).globalToLocal(pt);
			dragEvent.localX = pt.x;
			dragEvent.localY = pt.y;
			_dispatchDragEvent(DisplayObject(eventTarget), dragEvent);
		}

		private function _dispatchDragEvent(target:DisplayObject, event:DragEvent):void
		{

			if (isSameOrChildApplicationDomain(target))
				target.dispatchEvent(event);
			else
			{

				var me:InterManagerRequest = new InterManagerRequest(InterManagerRequest.INIT_MANAGER_REQUEST);
				me.name = "mx.managers::IDragManager";
				sandboxRoot.dispatchEvent(me);

				var mde:InterDragManagerEvent = new InterDragManagerEvent(InterDragManagerEvent.DISPATCH_DRAG_EVENT, false, false,
					event.localX,
					event.localY,
					event.relatedObject,
					event.ctrlKey,
					event.altKey,
					event.shiftKey,
					event.buttonDown,
					event.delta,
					target,
					event.type,
					event.dragInitiator,
					event.dragSource,
					event.action,
					event.draggedItem
				);
				sandboxRoot.dispatchEvent(mde);
			}
		}
		
		private function isSameOrChildApplicationDomain(target:Object):Boolean
		{
			var swfRoot:DisplayObject = SystemManager.getSWFRoot(target);
			if (swfRoot)
				return true;
			
			if (ModuleManager.getAssociatedFactory(target))
				return true;
			
			var me:InterManagerRequest = new InterManagerRequest(InterManagerRequest.SYSTEM_MANAGER_REQUEST);
			me.name = "hasSWFBridges";
			sandboxRoot.dispatchEvent(me);
			if (!me.value) return true;
			
			return false;
		}

		public function mouseMoveHandler(event:MouseEvent):void
		{
			var dropTarget:DisplayObject;
			lastMouseEvent = event;
			var point:Point = new Point(event.localX, event.localY);
			var stagePoint:Point = DisplayObject(event.target).localToGlobal(point);
			point = DisplayObject(sandboxRoot).globalToLocal(stagePoint);
			
			if(_ydirection){
				var mouseY:Number = point.y;
				y = mouseY - yOffset;
			}

			if(_xdirection){
				var mouseX:Number = point.x;
				x = mouseX - xOffset;
			}
			if(!event)return;
			event.updateAfterEvent();
	
			var targetList:Array; /* of DisplayObject */
			targetList = [];
			XYAxisDragProxy.getObjectsUnderPoint(DisplayObject(sandboxRoot), stagePoint, targetList);
			
			var newTarget:DisplayObject = null;

			var targetIndex:int = targetList.length - 1;
			while (targetIndex >= 0)
			{
				newTarget = targetList[targetIndex];
				if (newTarget != this && !contains(newTarget))
					break;
				targetIndex--;
			}
			if (target)
			{
				var foundIt:Boolean = false;
				var oldTarget:DisplayObject = target;
				
				dropTarget = newTarget;
				
				while (dropTarget)
				{
					if (dropTarget == target)
					{

						dispatchDragEvent(DragEvent.DRAG_OVER, event, dropTarget);
						foundIt = true;
						break;
					} 
					else 
					{

						dispatchDragEvent(DragEvent.DRAG_ENTER, event, dropTarget);
						if (target == dropTarget)
						{
							foundIt = false;
							break;
						}
					}
					dropTarget = dropTarget.parent;
				}
				
				if (!foundIt)
				{
					dispatchDragEvent(DragEvent.DRAG_EXIT, event, oldTarget);
					
					if (target == oldTarget)
						target = null;
				}
			}

			if (!target)
			{
				action = DragManager.MOVE;
				dropTarget = newTarget;
				while (dropTarget)
				{
					if (dropTarget != this)
					{
						dispatchDragEvent(DragEvent.DRAG_ENTER, event, dropTarget);
						if (target)
							break;
					}
					dropTarget = dropTarget.parent;
				}
				
				if (!target)
					action = DragManager.NONE;
			}
			showFeedback();
		}

		public function mouseLeaveHandler(event:Event):void
		{
			mouseUpHandler(lastMouseEvent);
		}

		public function mouseUpHandler(event:MouseEvent):void
		{
			var dragEvent:DragEvent;
			
			var ed:IEventDispatcher = sandboxRoot;
			
			ed.removeEventListener(MouseEvent.MOUSE_MOVE,
				mouseMoveHandler, true);
			ed.removeEventListener(MouseEvent.MOUSE_UP,
				mouseUpHandler, true);
			ed.removeEventListener(SandboxMouseEvent.MOUSE_UP_SOMEWHERE, 
				mouseLeaveHandler);
			var delegate:Object = automationDelegate;
			if (target && action != DragManager.NONE)
			{
				dragEvent = new DragEvent(DragEvent.DRAG_DROP);
				dragEvent.dragInitiator = dragInitiator;
				dragEvent.dragSource = dragSource;
				dragEvent.action = action;
				dragEvent.ctrlKey = event.ctrlKey;
				dragEvent.altKey = event.altKey;
				dragEvent.shiftKey = event.shiftKey;
				var pt:Point = new Point();
				pt.x = lastMouseEvent.localX;
				pt.y = lastMouseEvent.localY;
				pt = DisplayObject(lastMouseEvent.target).localToGlobal(pt);
				pt = DisplayObject(target).globalToLocal(pt);
				dragEvent.localX = pt.x;
				dragEvent.localY = pt.y;
				if (delegate)
					delegate.recordAutomatableDragDrop(target, dragEvent);
				_dispatchDragEvent(target, dragEvent);
			}
			else
			{
				action = DragManager.NONE;
			}
			if (action == DragManager.NONE)
			{
				var m1:Move = new Move(this);
				m1.addEventListener(EffectEvent.EFFECT_END, effectEndHandler);
				m1.xFrom = x;
				m1.yFrom = y;
				m1.xTo = startX;
				m1.yTo = startY;
				m1.duration = 200;
				m1.play();
			}
			else
			{
				var e:Zoom = new Zoom(this);
				e.zoomWidthFrom = e.zoomHeightFrom = 1.0;
				e.zoomWidthTo = e.zoomHeightTo = 0;
				e.duration = 200;
				e.play();
				
				var m:Move = new Move(this);
				m.addEventListener(EffectEvent.EFFECT_END, effectEndHandler);
				m.xFrom = x;
				m.yFrom = y;
				m.xTo = parent.mouseX;
				m.yTo = parent.mouseY;
				m.duration = 200;
				m.play();
			}
			dragEvent = new DragEvent(DragEvent.DRAG_COMPLETE);
			dragEvent.dragInitiator = dragInitiator;
			dragEvent.dragSource = dragSource;
			dragEvent.relatedObject = InteractiveObject(target);
			dragEvent.action = action;
			dragEvent.ctrlKey = event.ctrlKey;
			dragEvent.altKey = event.altKey;
			dragEvent.shiftKey = event.shiftKey;
			dragInitiator.dispatchEvent(dragEvent);
			
			if (delegate && action == DragManager.NONE)
				delegate.recordAutomatableDragCancel(dragInitiator, dragEvent);
			cursorManager.removeCursor(cursorID);
			cursorID = CursorManager.NO_CURSOR;
			
			this.lastMouseEvent = null;
		}

		private function effectEndHandler(event:EffectEvent):void
		{
			DragManager.endDrag();
		}

		private static function getObjectsUnderPoint(obj:DisplayObject, pt:Point, arr:Array):void
		{
			if (!obj.visible)
				return;
			
			if ((obj is UIComponent) && !UIComponent(obj).$visible)
				return;
			
			if (obj.hitTestPoint(pt.x, pt.y, true))
			{
				if (obj is InteractiveObject && InteractiveObject(obj).mouseEnabled)
					arr.push(obj);
				if (obj is DisplayObjectContainer)
				{
					var doc:DisplayObjectContainer = obj as DisplayObjectContainer;
					if (doc.mouseChildren)
					{
						if ("rawChildren" in doc)
						{
							var rc:Object = doc["rawChildren"];
							n = rc.numChildren;
							for (i = 0; i < n; i++)
							{
								try
								{
									getObjectsUnderPoint(rc.getChildAt(i), pt, arr);
								}
								catch (e:Error)
								{
								}
							}
						}
						else
						{
							if (doc.numChildren)
							{
								var n:int = doc.numChildren;
								for (var i:int = 0; i < n; i++)
								{
									try
									{
										var child:DisplayObject = doc.getChildAt(i);
										getObjectsUnderPoint(child, pt, arr);
									}
									catch (e:Error)
									{
									}
								}
							}
						}
					}
				}
			}
		}
		
	}
	
}

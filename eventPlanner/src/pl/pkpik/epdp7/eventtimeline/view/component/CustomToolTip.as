package pl.pkpik.epdp7.eventtimeline.view.component
{
	import mx.core.FlexGlobals;
	import mx.core.IDataRenderer;
	import mx.core.IFactory;
	import mx.core.IToolTip;
	import mx.core.UIComponent;
	import mx.styles.CSSStyleDeclaration;
	
	import pl.pkpik.epdp7.eventtimeline.view.skin.CustomToolTipSkin;
	
	import spark.components.Group;
	import spark.components.RichText;
	import spark.components.supportClasses.SkinnableComponent;
	import spark.utils.TextFlowUtil;
	

	public final class CustomToolTip extends SkinnableComponent implements IToolTip
	{
		private static var defaultStyleValuesInited:Boolean = classConstruct();
		
		private static function classConstruct():Boolean {
			if (!FlexGlobals.topLevelApplication.styleManager.getStyleDeclaration("pl.pkpik.epdp7.eventtimeline.view.component.CustomToolTip"))
			{
				var niceToolTipStyles:CSSStyleDeclaration = new CSSStyleDeclaration();
				niceToolTipStyles.defaultFactory = function():void
				{	
					this.color = 0x303030;
					this.chromeColor = 0x90d000;
					this.fontSize = 11;
				}
				FlexGlobals.topLevelApplication.styleManager.setStyleDeclaration("pl.pkpik.epdp7.eventtimeline.view.component.CustomToolTip", niceToolTipStyles, true);
			}
			return true;
		}

		
		public function CustomToolTip()
		{
			super();
			mouseEnabled = false;
			this.mouseChildren = false;
			maxWidth = 300;
		}
		

		[SkinPart(required="false", type="spark.components.Label")]
		public var displayTitle:IFactory;

		private var displayTitleInstance:IDataRenderer;
		

		[SkinPart(required="true")]
		public var displayText:RichText;
		

		[SkinPart(required="true")]
		public var tooltipGroup:Group;
		
		

		private var _text:String;
		
		public function set text (value:String):void {
			
			if (_text != value && displayText && value) {
				_text = value;
				displayText.content = TextFlowUtil.importFromString(_text);
			}
		}
		
		public function get text ():String {
			return _text;
		}
		

		private var _title:String;
		
		public function set title (value:String):void {
			
			if (_title != value) {
				_title = value;

				if (!displayTitleInstance) 
				{
					displayTitleInstance = IDataRenderer(this.createDynamicPartInstance("displayTitle"));
					UIComponent(displayTitleInstance).setStyle("color", this.getStyle("color"));
					tooltipGroup.addElementAt(UIComponent(displayTitleInstance), 0);
				}
				displayTitleInstance.data = TextFlowUtil.importFromString(_title);
			}
		}
		
		public function get title ():String {
			return _title;
		}
		
		override public function stylesInitialized():void {
			var skinClass:Object = this.getStyle("skinClass");
			if (!skinClass) {
				this.setStyle("skinClass", CustomToolTipSkin);
			}
		}
	}
}
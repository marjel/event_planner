package pl.pkpik.epdp7.eventtimeline.view.component {
    public interface ISelectable {

        function setWeakSelection(value:Boolean):void;
        function get selected():Boolean;
        function set selected(value:Boolean):void;
        function get isBlocked():Boolean;
        function set isBlocked(value:Boolean):void;
        function get index():int;
        function get dataId():int;
        function set index(value:int):void;

    }
}

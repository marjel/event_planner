package pl.pkpik.epdp7.eventtimeline.view.mediator
{

    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.utils.clearTimeout;
    import flash.utils.setTimeout;

    import mx.core.IFlexDisplayObject;
    import mx.events.CloseEvent;
    import mx.events.FlexEvent;
    import mx.events.ResizeEvent;

    import mx.managers.CursorManager;
    import mx.managers.PopUpManager;
    import mx.managers.ToolTipManager;

    import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.ContextMenuDataProxy;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.ResourcesMonitorProxy;
    import pl.pkpik.epdp7.eventtimeline.view.component.DataProcessingInfo;
    import pl.pkpik.epdp7.eventtimeline.view.vo.ContextMenuVO;
    import pl.pkpik.epdp7.eventtimeline.util.CustomToolTipManagerImpl;
    import pl.pkpik.epdp7.eventtimeline.view.component.ContextMenuContainer;
    import pl.pkpik.epdp7.eventtimeline.view.component.Toast;
    import pl.pkpik.epdp7.eventtimeline.view.vo.ContextMenuCollection;


    public final class ApplicationMediator extends Mediator// implements IContextMenuComponent
	{

        public function get contextMenuData():Array
        {
            return [
                new ContextMenuVO("settings","Ustawienia...", "global"),
                new ContextMenuVO("version","Wersja: " + Version, "version")
            ];
        }
		
		public static const NAME:String = "ApplicationMediator";
        private var popupContent:IFlexDisplayObject;

		private var dataProcessingInfo:DataProcessingInfo;

		private var contextMenu:ContextMenuContainer;
		
		public function ApplicationMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
		}
		
		override public function onRegister():void
		{

            application.addEventListener(Event.ADDED_TO_STAGE, function(event:Event):void
            {
                event.target.stage.addEventListener(MouseEvent.RIGHT_CLICK, function (event:MouseEvent):void {
                    if(dataProcessingInfo==null){
                        sendNotification(ApplicationCommands.SET_CONTEXT_MENU,
                                contextMenuDataProxy.setContextMenuData("Event Planner",
                                        event.stageX, event.stageY, contextMenuData));
                    }else{
                        event.preventDefault();
                        event.stopImmediatePropagation();
                    }

                }, true);
                event.target.stage.addEventListener(MouseEvent.MOUSE_DOWN, function (event:MouseEvent):void {
                    if(contextMenu!=null){
                        removeContextMenu();
                    }
                }, true);

            });
            var resourcesMonitorProxy:ResourcesMonitorProxy = facade.retrieveProxy( ResourcesMonitorProxy.NAME ) as ResourcesMonitorProxy;
            resourcesMonitorProxy.loadResources();
            facade.registerMediator(new EventTimelineMediator( application.eventTimeline ));
		}
		
		override public function listNotificationInterests():Array 
		{
			return [
				ApplicationCommands.OPEN_POPUP,
				ApplicationCommands.ENABLE_APPLICATION,
                ApplicationCommands.SHOW_NOTIFICATION,
                ApplicationCommands.SET_CONTEXT_MENU,
                ApplicationCommands.SHOW_DATA_PROCESSING_INFO,
                ApplicationCommands.HIDE_DATA_PROCESSING_INFO,
                ApplicationCommands.QUEUE_EXECUTED
			];
		}
		private var tmooutId:uint;
		override public function handleNotification( note:INotification ):void 
		{
			switch (note.getName()){
				case ApplicationCommands.ENABLE_APPLICATION:
					enableAppliation(Boolean(note.getBody()));
					break;
				case ApplicationCommands.OPEN_POPUP:
					showAsPopUp(note.getBody().classValue as Class, note.getBody().popupParameters);
					break;
                case ApplicationCommands.SET_CONTEXT_MENU:
                    setContextMenu(note.getBody() as ContextMenuCollection)
                    break;
                case ApplicationCommands.SHOW_NOTIFICATION:
                    showNotification(note.getBody() as String);
                    break;
                case ApplicationCommands.SHOW_DATA_PROCESSING_INFO:
                    setDataProcessingInfo();
                    break;
                case ApplicationCommands.QUEUE_EXECUTED:
                        if(dataProcessingInfo){
                            sendNotification(ApplicationCommands.ACTION_EXECUTED, "The process is finished.");
                            dataProcessingInfo.enabled  = true;
                            if(tmooutId<1){
                                tmooutId = setTimeout(removeDataProcessingInfo, 12000);
                            }
                        }

                    break;
			}
		}


        private function updateTimeoutAftClick(event:MouseEvent):void{
            if(tmooutId>0){
                clearTimeout(tmooutId);
                tmooutId = 0
            }
            tmooutId = setTimeout(removeDataProcessingInfo, 6000);
        }


        private function setDataProcessingInfo():void
        {
            if(tmooutId>0){
                clearTimeout(tmooutId);
                tmooutId = 0
            }
            if(!facade.hasMediator(DataProcessingInfoMediator.NAME)){
                dataProcessingInfo = new DataProcessingInfo();
                dataProcessingInfo.enabled =false;
                dataProcessingInfo.addEventListener(Event.ADDED_TO_STAGE, function(event:Event):void{
                    var dpi:DataProcessingInfo = event.target as DataProcessingInfo;
                    dpi.width = dpi.stage.stageWidth * .4;
                    PopUpManager.centerPopUp(dpi);
                    dpi.stage.addEventListener(Event.RESIZE , centerDataProcessingInfo);
                }, false, 0 , true);
                dataProcessingInfo.addEventListener(CloseEvent.CLOSE, removeDataProcessingInfo);
                dataProcessingInfo.addEventListener(MouseEvent.MOUSE_DOWN, updateTimeoutAftClick);

                dataProcessingInfo.addEventListener(ResizeEvent.RESIZE, centerDataProcessingInfo);
                PopUpManager.addPopUp(dataProcessingInfo, application, true);
                facade.registerMediator(new DataProcessingInfoMediator(dataProcessingInfo));
            }


        }

        private function centerDataProcessingInfo(event:Event = null):void
        {
            PopUpManager.centerPopUp(dataProcessingInfo);
        }
        private function removeDataProcessingInfo(event:CloseEvent = null):void
        {
            if(tmooutId>0){
                clearTimeout(tmooutId);
                tmooutId = 0
            }

            dataProcessingInfo.removeEventListener(CloseEvent.CLOSE, removeDataProcessingInfo);
            dataProcessingInfo.removeEventListener(MouseEvent.MOUSE_DOWN, updateTimeoutAftClick);
            dataProcessingInfo.stage.removeEventListener(Event.RESIZE , centerDataProcessingInfo);
            dataProcessingInfo.removeEventListener(ResizeEvent.RESIZE, centerDataProcessingInfo);

            if(dataProcessingInfo!=null){
                sendNotification(ApplicationCommands.HIDE_DATA_PROCESSING_INFO);
                PopUpManager.removePopUp(dataProcessingInfo);
                facade.removeMediator(DataProcessingInfoMediator.NAME);
                dataProcessingInfo = null;
                application.setFocus();
            }
        }

        private function showAsPopUp(classValue:Class, parameters:Object = null):IFlexDisplayObject {

            if (parameters == null) {
                parameters = {}
            }

            removePopup();
            popupContent = PopUpManager.createPopUp(application, classValue, parameters.modal != null ? parameters.modal : true);
            if (parameters.center != false) {
                PopUpManager.centerPopUp(popupContent);
            }
            return popupContent;
        }

        private function removePopup():void {
            if (popupContent != null) {

                PopUpManager.removePopUp(popupContent);
                popupContent = null;
            }


        }




        private function setContextMenu(data:ContextMenuCollection):void
        {
            removeContextMenu();

            (CustomToolTipManagerImpl.getInstance() as CustomToolTipManagerImpl).removeToolTips();
            ToolTipManager.enabled = false;
            contextMenu = new ContextMenuContainer();
            contextMenu.addEventListener(FlexEvent.CONTENT_CREATION_COMPLETE, contextMenuCreationComplete, false, 0, true);
            application.addElement(contextMenu);

            contextMenu.data = data;



        }

        private function contextMenuCreationComplete(event:Event):void
        {
            contextMenu.removeEventListener(FlexEvent.CONTENT_CREATION_COMPLETE, contextMenuCreationComplete);
            facade.registerMediator(new ContextMenuMediator(contextMenu));
        }
        private function removeContextMenu():void
        {
            if(contextMenu!=null){
                contextMenu.removeEventListener(FlexEvent.CONTENT_CREATION_COMPLETE, contextMenuCreationComplete);
                ToolTipManager.enabled = true;
                facade.removeMediator(ContextMenuMediator.NAME);
                application.removeElement(contextMenu);
                contextMenu = null;
                application.setFocus();
            }

        }

        private function showNotification(text:String):void
        {
            Toast.makeInstance(text, Toast.LONG_DELAY).show();
        }
		
		private function enableAppliation(value:Boolean):void
		{
			if(!value){
				CursorManager.setBusyCursor();
			}else{
				CursorManager.removeBusyCursor();
			}
			
			application.enabled = value;
		}
		
		public function get application():EventPlanner
		{
			return this.viewComponent as EventPlanner;
		}

        private function get contextMenuDataProxy():ContextMenuDataProxy
        {
            return facade.retrieveProxy(ContextMenuDataProxy.NAME) as ContextMenuDataProxy;
        }
		
	}
}
package pl.pkpik.epdp7.eventtimeline.view.mediator {
import flash.events.Event;

import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
import pl.pkpik.epdp7.eventtimeline.model.proxy.ContextMenuDataProxy;
import pl.pkpik.epdp7.eventtimeline.view.vo.ContextMenuVO;

import spark.events.IndexChangeEvent;
import org.puremvc.as3.patterns.mediator.Mediator;

import pl.pkpik.epdp7.eventtimeline.view.component.ContextMenuContainer;

public class ContextMenuMediator extends Mediator {

    public static const NAME:String = "ContextMenuMediator";

    public function ContextMenuMediator(viewComponent:Object = null) {
        super(NAME, viewComponent);
    }

    override public function onRegister():void
    {
        typedComponent.menuList.addEventListener(IndexChangeEvent.CHANGE, function(event:IndexChangeEvent):void
        {
            event.preventDefault();
            var dataItem:ContextMenuVO = contextMenuDataProxy.typedData.getItemAt(event.newIndex) as ContextMenuVO;
            sendNotification(ApplicationCommands.EXECUTE_CONTEXT_MENU, dataItem);
        });



    }
    private function get typedComponent():ContextMenuContainer
    {
        return this.viewComponent as ContextMenuContainer;
    }

    private function get contextMenuDataProxy():ContextMenuDataProxy
    {
        return facade.retrieveProxy(ContextMenuDataProxy.NAME) as ContextMenuDataProxy;
    }
}
}

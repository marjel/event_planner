package pl.pkpik.epdp7.eventtimeline.view.mediator
{

    import flash.events.Event;
import flash.events.MouseEvent;
import flash.utils.Dictionary;
import mx.core.IUIComponent;
    import mx.core.IVisualElement;
    import mx.events.DragEvent;
import mx.events.PropertyChangeEvent;
import mx.managers.DragManager;
import org.apache.flex.collections.VectorCollection;

import org.puremvc.as3.interfaces.INotification;
import org.puremvc.as3.patterns.mediator.Mediator;

import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
import pl.pkpik.epdp7.eventtimeline.controller.event.SelectionEvent;
import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
import pl.pkpik.epdp7.eventtimeline.model.proxy.AdditionalEventsDataProxy;
import pl.pkpik.epdp7.eventtimeline.model.proxy.BrokenRulesDataProxy;
import pl.pkpik.epdp7.eventtimeline.model.proxy.ConfigProxy;
import pl.pkpik.epdp7.eventtimeline.model.proxy.ContextMenuDataProxy;
import pl.pkpik.epdp7.eventtimeline.model.proxy.EventsDataProxy;
import pl.pkpik.epdp7.eventtimeline.model.proxy.ResourcesMonitorProxy;
import pl.pkpik.epdp7.eventtimeline.model.vo.AdditionalPlannerSolutionVO;
import pl.pkpik.epdp7.eventtimeline.model.vo.BrokenRuleVO;
import pl.pkpik.epdp7.eventtimeline.service.DataService;

    import pl.pkpik.epdp7.eventtimeline.util.MathUtils;
    import pl.pkpik.epdp7.eventtimeline.view.component.IBreakable;
import pl.pkpik.epdp7.eventtimeline.view.component.ISelectable;
import pl.pkpik.epdp7.eventtimeline.model.vo.ContractVO;
import pl.pkpik.epdp7.eventtimeline.model.vo.DayOffVO;
import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;
import pl.pkpik.epdp7.eventtimeline.model.vo.FreeDayVO;
import pl.pkpik.epdp7.eventtimeline.model.vo.MeasurementVO;
import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftAssignmentVO;
import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftDateVO;
import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftVO;
import pl.pkpik.epdp7.eventtimeline.util.KeyboardProxy;
import pl.pkpik.epdp7.eventtimeline.view.component.AdditionalShiftRectangle;
import pl.pkpik.epdp7.eventtimeline.view.component.DateList;
import pl.pkpik.epdp7.eventtimeline.view.component.DatesMeasurementList;
import pl.pkpik.epdp7.eventtimeline.view.component.EmployeesList;
import pl.pkpik.epdp7.eventtimeline.view.component.EmployeesMeasurementList;
import pl.pkpik.epdp7.eventtimeline.view.component.EventTimeline;
import pl.pkpik.epdp7.eventtimeline.view.component.FreeDayRectangle;
import pl.pkpik.epdp7.eventtimeline.view.component.NightContractRectangle;
import pl.pkpik.epdp7.eventtimeline.view.component.OffDayRectangle;
import pl.pkpik.epdp7.eventtimeline.view.component.PlannerPanel;
import pl.pkpik.epdp7.eventtimeline.view.component.PlannerTimeGrid;
import pl.pkpik.epdp7.eventtimeline.view.component.ShiftRectangle;
import pl.pkpik.epdp7.eventtimeline.view.component.UnassignedShiftsPanel;
import pl.pkpik.epdp7.eventtimeline.view.component.VirtualEmployeePanel;
import pl.pkpik.epdp7.eventtimeline.view.itemrenderer.DateListItemRenderer;
import pl.pkpik.epdp7.eventtimeline.view.itemrenderer.DateMeasurementHeadRenderer;
import pl.pkpik.epdp7.eventtimeline.view.itemrenderer.EmployeeMeasurementHeadRenderer;
import pl.pkpik.epdp7.eventtimeline.view.itemrenderer.EmployeesListItemRenderer;
import pl.pkpik.epdp7.eventtimeline.view.vo.SelectedItemObject;
import pl.pkpik.epdp7.eventtimeline.view.vo.SetShiftAssignmentVO;

import spark.components.Group;
    import spark.components.Scroller;

    public class EventTimelineMediator extends Mediator
{

    public static const NAME:String = "EventTimelineMediator";
    private static const NORMAL_ZOOM_VALUE:Number = 200;

    private var unassignedShiftsGrid:PlannerTimeGrid;
    private var virtualEmployeeGrid:PlannerTimeGrid;
    private var plannerGrid:PlannerTimeGrid;

    private var events:Dictionary;
    private var offDaysRectangles:Dictionary;

    private var mousePositionY:Number=0;
    private var mousePositionX:Number=0;

    private var _zoomValue:Number = NORMAL_ZOOM_VALUE;

    private var freeDays:Array;

    private var selectedEmployees:VectorCollection =new VectorCollection(new Vector.<EmployeesListItemRenderer>());
    private var selectedDates:VectorCollection =new VectorCollection(new Vector.<DateListItemRenderer>());

    private var keyboardProxy:KeyboardProxy = KeyboardProxy.GetInstance();

    private var selectedShifts:VectorCollection = new VectorCollection(new Vector.<ShiftRectangle>());

    private var selectedFreeDays:VectorCollection = new VectorCollection(new Vector.<FreeDayRectangle>());

    private var brokenElements:VectorCollection = new VectorCollection(new Vector.<IBreakable>());

    private var additionalRectanglesSet:VectorCollection = new VectorCollection(new Vector.<AdditionalShiftRectangle>());



    public function EventTimelineMediator(viewComponent:Object=null)
    {
        super(NAME, viewComponent);
    }
    override public function onRegister():void
    {
        facade.registerMediator( new ZoomPanelMediator( eventTimeline.zoomPanel ));

    }

    private function setInitialization():void
    {
        initializePanelsEvents();
        initializeView();

        sendNotification(ApplicationCommands.EXTERNAL_INTERFACE_INIT);
    }

    override public function listNotificationInterests():Array
    {
        return [
            ResourcesMonitorProxy.LOADING_COMPLETE,
            ApplicationCommands.SET_EVENTS_DATA,
            ApplicationCommands.QUEUE_EXECUTED,
            ApplicationCommands.EVENTS_DATA_LOADED,
            ApplicationCommands.ADDITIONAL_EVENTS_DATA_LOADED,
            ApplicationCommands.SET_ADDITIONAL_EVENTS_DATA,
            ApplicationCommands.DATA_REFRESHED,
            ApplicationCommands.MEASUREMENT_ALREADY_SET,
            ApplicationCommands.FREE_DAYS_REFRESHED,
            ApplicationCommands.OFF_DAYS_REFRESHED,
            ApplicationCommands.BROKEN_RULES_ALREADY_SET,
            ApplicationCommands.ZOOM_VIEW,
            ApplicationCommands.SHOW_DATE_MEASUREMENT,
            ApplicationCommands.HIDE_DATE_MEASUREMENT,
            ApplicationCommands.SHOW_EMPLOYEE_MEASUREMENT,
            ApplicationCommands.HIDE_EMPLOYEE_MEASUREMENT,
            ApplicationCommands.EXPAND_UNASSIGNED_SHIFTS_PANEL,
            ApplicationCommands.COLLAPSE_UNASSIGNED_SHIFTS_PANEL,
            ApplicationCommands.EXPAND_VIRTUAL_EMPLOYEE_PANEL,
            ApplicationCommands.COLLAPSE_VIRTUAL_EMPLOYEE_PANEL,
            ApplicationCommands.SCROLL_TO_SHIFT,
            ApplicationCommands.SCROLL_TO_DATE,
            ApplicationCommands.SCROLL_TO_EMPLOYEE,
            ApplicationCommands.NAVIGATE_TO_BROKEN_RULE,
            ApplicationCommands.SHOW_BROKEN_RULES,
            ApplicationCommands.HIDE_BROKEN_RULES,
            ApplicationCommands.SET_SHIFT_ASSIGNMENT,
            ApplicationCommands.ACTIONS_STOP_EXECUTE,
            ApplicationCommands.HIDE_DATA_PROCESSING_INFO

        ];
    }

    private function clearSelectionLists():void
    {
        selectedShifts.removeAll();
        selectedEmployees.removeAll();
        selectedDates.removeAll();
    }

    override public function handleNotification( note:INotification ):void
    {
        switch (note.getName()){
            case ResourcesMonitorProxy.LOADING_COMPLETE:
                sendNotification(ApplicationCommands.SHOW_DATA_PROCESSING_INFO);
                setInitialization();
                break;
            case ApplicationCommands.QUEUE_EXECUTED:
                eventTimeline.enabled =
                        eventTimeline.zoomPanel.enabled = true;
                break;
            case ApplicationCommands.SET_EVENTS_DATA:
                disposeAll();

                break;
            case ApplicationCommands.DATA_REFRESHED:
                refreshShiftsAsssignments(note.getBody() as Vector.<ShiftAssignmentVO>);
                break;
            case ApplicationCommands.MEASUREMENT_ALREADY_SET:

                setMeasurement();
                break;
            case ApplicationCommands.OFF_DAYS_REFRESHED:
                refreshOffDaysRectangles(note.getBody() as Vector.<EmployeeVO>);
                break;
            case ApplicationCommands.FREE_DAYS_REFRESHED:
                refreshFreeDaysRectangles();
                break;
            case ApplicationCommands.SET_ADDITIONAL_EVENTS_DATA:

                sendNotification(ApplicationCommands.SHOW_DATA_PROCESSING_INFO);
                break;

            case ApplicationCommands.ADDITIONAL_EVENTS_DATA_LOADED:
                parseAdditionalData();
                break;
            case ApplicationCommands.EVENTS_DATA_LOADED:
                addScrollEventsListeners();
                refreshViewAfterLoadNewData();
                break;
            case ApplicationCommands.SHOW_BROKEN_RULES:
                switchBrokenRules(true);
                break;
            case ApplicationCommands.BROKEN_RULES_ALREADY_SET:
                dispatchBrokenRules();
                sendNotification(ApplicationCommands.SHOW_DATA_PROCESSING_INFO);
                break;
            case ApplicationCommands.ZOOM_VIEW:

                var zoomVal:Number = Number(note.getBody());
                _zoomValue = NORMAL_ZOOM_VALUE + ((zoomVal>0) ? zoomVal*40 : zoomVal*16);
                resizeGrid(minimumZoomValue);
                break;
            case ApplicationCommands.SHOW_DATE_MEASUREMENT:
                if(datesMeasurementList.dataProvider){
                    eventTimeline.dateMeasurementHeaderDG.visible =
                            eventTimeline.dateMeasurementHeaderDG.includeInLayout =
                                    eventTimeline.datesMeasurementList.visible =
                                            eventTimeline.datesMeasurementList.includeInLayout = true;
                }
                break;
            case ApplicationCommands.HIDE_DATE_MEASUREMENT:
                eventTimeline.dateMeasurementHeaderDG.visible =
                        eventTimeline.dateMeasurementHeaderDG.includeInLayout =
                                eventTimeline.datesMeasurementList.visible =
                                        eventTimeline.datesMeasurementList.includeInLayout = false;
                break;
            case ApplicationCommands.SHOW_EMPLOYEE_MEASUREMENT:
                if(employeesMeasurementList.dataProvider){
                    eventTimeline.employeeMeasurementContainer.visible =
                            eventTimeline.employeeMeasurementContainer.includeInLayout = true;
                    setElementsSizeTO();
                }
                break;
            case ApplicationCommands.HIDE_EMPLOYEE_MEASUREMENT:
                    if(eventTimeline.employeeMeasurementContainer.visible){
                        eventTimeline.employeeMeasurementContainer.visible =
                                eventTimeline.employeeMeasurementContainer.includeInLayout = false;
                        setElementsSizeTO();
                    }
                break;
            case ApplicationCommands.EXPAND_UNASSIGNED_SHIFTS_PANEL:
                expandUnassignedShiftsGrid();
                break;
            case ApplicationCommands.COLLAPSE_UNASSIGNED_SHIFTS_PANEL:
                collapseUnassignedShiftsGrid();
                break;
            case ApplicationCommands.EXPAND_VIRTUAL_EMPLOYEE_PANEL:
                expandVirtualEmployeeGrid();
                break;
            case ApplicationCommands.COLLAPSE_VIRTUAL_EMPLOYEE_PANEL:
                collapseVirtualEmployeeGrid();
                break;
            case ApplicationCommands.HIDE_BROKEN_RULES:
                switchBrokenRules(false);
                break;
            case ApplicationCommands.NAVIGATE_TO_BROKEN_RULE:
                navigateToBrokenRule(int(note.getBody()));
                break;
            case ApplicationCommands.SCROLL_TO_SHIFT:
                scrollToShift(int(note.getBody()));
                break;
            case ApplicationCommands.SCROLL_TO_EMPLOYEE:
                scrollToEmployee(int(note.getBody()));
                break;
            case ApplicationCommands.SCROLL_TO_DATE:
                scrollToDate(int(note.getBody()));
                break;
            case ApplicationCommands.SET_SHIFT_ASSIGNMENT:
                setShiftAssignment(note.getBody() as SetShiftAssignmentVO);
                break;
            case ApplicationCommands.HIDE_DATA_PROCESSING_INFO:
                eventTimeline.enabled =
                        eventTimeline.zoomPanel.enabled = true;
                break;
        }
    }

    private function get minimumZoomValue():Number
    {

        var value:Number = MathUtils.round(this._zoomValue);
        var viewWidth:Number = eventTimeline.stage.stageWidth - (employeesList.contentWidth+14) -
                ((eventTimeline.employeeMeasurementContainer.visible) ? employeesMeasurementList.contentWidth : 0);
        return (viewWidth < (value*dateList.numElements)) ? value : MathUtils.round(viewWidth/dateList.numElements);

    }
    private function disposeAll():void
    {
        eventTimeline.visible = false;
        collapseVirtualEmployeeGrid();
        collapseUnassignedShiftsGrid();
        sendNotification(ApplicationCommands.SHOW_DATA_PROCESSING_INFO);
        eventTimeline.enabled =
                eventTimeline.zoomPanel.enabled = false;
        clearSelectionLists();
        clearLists();
        unassignedShiftsPanel.horizontalScrollPosition=
            virtualEmployeePanel.horizontalScrollPosition=
                dateList.horizontalScrollPosition=
                        plannerPanel.horizontalScrollPosition =
                                employeesList.verticalScrollPosition =
                                        employeesMeasurementList.verticalScrollPosition = 0;

        removeEventRectangles();
        removeScrollEventsListeners();
        removePlannerGrid();
        clearMeasurement();
        removeVirtualEmployeeGrid();
        removeUnassignedShiftsGrid();
    }
    private function initialBlockShifts():void
    {
        var vec:Vector.<int> = eventsDataProxy.getBlockedEmployeesIndices();
        var i:int;
        for each(i in vec)
        {
            changeEmployeeElementsBlocked(i, true);
        }
        vec = eventsDataProxy.getBlockedDatesIndices();
        for each(i in vec)
        {
            changeDateElementsBlocked(i, true);
        }
    }

    public function changeDateBlocked(id:int, value:Boolean):void
    {
        var listElement:ISelectable = dateList.getItemById(id);
        listElement.isBlocked = value;
        changeDateElementsBlocked(listElement.index, value);
    }
    public function changeEmployeeBlocked(id:int, value:Boolean):void{

        var listElement:ISelectable = employeesList.getItemById(id);
        listElement.isBlocked = value;
        changeEmployeeElementsBlocked(listElement.index, value);
    }

    private function changeDateElementsBlocked(index:int, value:Boolean):void
    {
        plannerGrid.getShiftsRectanglesByDayIndex(index, "isBlocked", value);
    }
    private function changeEmployeeElementsBlocked(index:int, value:Boolean):void{
        plannerGrid.getShiftsRectanglesByEmployeeIndex(index, "isBlocked", value);
    }
    private function setShiftAssignment(value:SetShiftAssignmentVO):void
    {

        value.rectangle.employee = value.employeeVO;
        plannerGrid.setElement(value.rectangle);
        sendNotification(ApplicationCommands.CHANGE_SHIFT_ASSIGNMENT, value.rectangle);

        value.rectangle.selected = true;

        scrollToEmployee(value.employeeVO.index);

    }

    private function scrollToEmployee(index:int):void
    {
        employeesList.verticalScrollPosition=index * Number(configProxy.configVO.itemHeight);
    }

    private function scrollToDate(index:int):void
    {

    }

    private function switchBrokenRules(value:Boolean):void
    {
        for each(var broken:IBreakable in brokenElements){
            broken.broken = value;
        }
    }
    private function navigateToBrokenRule(id:int):void
    {

        if(brokenRulesDataProxy.typedData && brokenRulesDataProxy.typedData.length){
            var brVO:BrokenRuleVO = brokenRulesDataProxy.getBrokenRuleById(id);
            if(brVO){
                var elementId:uint;
                if(brVO.shiftAssignmentList.length){

                    elementId = uint(brVO.shiftAssignmentList.getItemAt(0));
                    scrollToShift(elementId);

                }else if(brVO.employeeList.length){
                    elementId = uint(brVO.employeeList.getItemAt(0));
                    var emploeeVO:EmployeeVO = eventsDataProxy.getEmployeeVOById(elementId);
                    var employeeListItemRenderer:ISelectable = employeesList.getItemByIndex(emploeeVO.index);
                    employeeListItemRenderer.selected = true;
                    scrollToEmployee(emploeeVO.index);
                }

            }else{
                sendNotification(ApplicationCommands.SHOW_NOTIFICATION, "Brak Złamanej reguły")
            }
        }else{
            sendNotification(ApplicationCommands.SHOW_NOTIFICATION, "Nie załadowano złamanych reguł.")
        }
    }

    private function removeBrokenRules():void
    {
        if(brokenElements.length){
            for each(var broken:IBreakable in brokenElements){
                broken.broken = false;
            }
            brokenElements.removeAll();
        }
    }

    private function dispatchBrokenRules():void
    {
        removeBrokenRules();
        var brokenRolesCollection:VectorCollection = brokenRulesDataProxy.typedData;
        if(brokenRolesCollection.length){
            var employees:VectorCollection;
            var assigments:VectorCollection;
            var brokenRuleItem:BrokenRuleVO;
            var item:uint;
            var emploeeVO:EmployeeVO;
            var employeeListItemRenderer:IBreakable;
            var rect:IBreakable;
            var shiftAss:ShiftAssignmentVO;
            var shiftVO:ShiftVO;
            for each(brokenRuleItem in brokenRolesCollection){
                employees = brokenRuleItem.employeeList;
                if(employees.length){
                    for each(item in employees){
                        emploeeVO = eventsDataProxy.getEmployeeVOById(item);
                        if(emploeeVO){
                            employeeListItemRenderer  =
                                    employeesList.getElementAt(emploeeVO.index) as IBreakable;
                            if(employeeListItemRenderer){
                                brokenElements.addItem(employeeListItemRenderer);
                            }
                        }
                    }
                }
                assigments = brokenRuleItem.shiftAssignmentList;
                if(assigments.length){
                    for each(item in assigments){
                        shiftAss = eventsDataProxy.getShiftAssignmentByID(item);

                        if(shiftAss){
                            shiftVO = eventsDataProxy.getShiftVOById(shiftAss.shiftReference);

                            if(shiftVO==null){
                                shiftVO = eventsDataProxy.getShiftVOById(shiftAss.id);
                            }
                            if(shiftVO){
                                rect = events[shiftVO] as IBreakable;
                                if(rect){
                                    brokenElements.addItem(rect);
                                }
                            }
                        }
                    }
                }
            }

            sendNotification(ApplicationCommands.ENABLE_BROKEN_RULES_BUTTON, true);
            executeQueueActionAfterTick("Set Broken Rules");
        }

    }
    private function executeQueueActionAfterTick(desc:String):void
    {
        var a:VectorCollection = DataService.getInstance().actions;
            callFunctionLater(new ActionData(sendNotification, this, [ApplicationCommands.UI_REFRESHED], 1, -1,
                    a.length>0 ? "<font color='#990000'>"+  desc + "</font>" : "<font color='#009900'>You can close the window or wait till it disappears.</font>"));

    }

    private function resizeGridHeight(value:Number = NaN):void
    {

        if(plannerGrid){
            var itmHgh:Number = Number(configProxy.configVO.itemHeight);
            employeesList.setEmployeeHeight(value);
            plannerGrid.setElementHeight(value);
            if(employeesMeasurementList.dataProvider){
                employeesMeasurementList.setHeight(value);
            }
            var val:Object;
            for each(val in events)
            {
                (val as ShiftRectangle).employeeHight =
                        isNaN(value) ? itmHgh : value ;
            }
        }

    }

    private function resizeGrid(value:Number, forceResizeDay:Boolean=false):void
    {
        dateList.setDayWidth(value, forceResizeDay);
        virtualEmployeeGrid.elementWidth =
            unassignedShiftsGrid.elementWidth =
                 plannerGrid.elementWidth = value;
        if(datesMeasurementList.dataProvider){
            datesMeasurementList.setDayWidth(value);
        }
    }


    private function initializeView():void
    {
        employeesList.addEventListener(SelectionEvent.CHANGE_SELECTION, function(event:SelectionEvent):void
        {
            var eIr:EmployeesListItemRenderer = event.target as EmployeesListItemRenderer;
            var index:int = eIr.index;
            if(event.data == true){
                deselectAllDates();
                deselectAllShifts();
                deselectAllFreeDays();
                if(!keyboardProxy.isCtrl){
                    deselectAllEmployees();
                }
                plannerGrid.setHorizontalSelection(index);
                selectedEmployees.addItem(eIr);
            }else{
                if((selectedEmployees.length>1) && (!keyboardProxy.isCtrl)){
                    deselectAllEmployees();
                    eIr.selected = true;
                }else{
                    plannerGrid.removeHorizontalSelection(index);
                    selectedEmployees.removeItem(eIr);
                }
            }
            sendNotification(ApplicationCommands.SELECT_OBJECT, new SelectedItemObject("employee", selectedEmployees));
        });
        eventTimeline.employeeMeasurementHeaderDG.addEventListener(MouseEvent.RIGHT_CLICK, function (event:MouseEvent):void {
            var item:EmployeeMeasurementHeadRenderer = event.target as EmployeeMeasurementHeadRenderer;
            deselectAllItems();
            sendNotification(ApplicationCommands.SET_CONTEXT_MENU,
                    contextMenuDataProxy.setContextMenuData((item.data as MeasurementVO).shortcut,
                            event.stageX, event.stageY, item.contextMenuData));
        });
        eventTimeline.dateMeasurementHeaderDG.addEventListener(MouseEvent.RIGHT_CLICK, function (event:MouseEvent):void {
            var item:DateMeasurementHeadRenderer = event.target as DateMeasurementHeadRenderer;
            deselectAllItems();
            sendNotification(ApplicationCommands.SET_CONTEXT_MENU,
                    contextMenuDataProxy.setContextMenuData((item.data as MeasurementVO).shortcut,
                            event.stageX, event.stageY, item.contextMenuData));
        });
        employeesList.addEventListener(MouseEvent.RIGHT_CLICK, function (event:MouseEvent):void {
            var item:EmployeesListItemRenderer = event.target as EmployeesListItemRenderer;
            item.selected = true;
            sendNotification(ApplicationCommands.SET_CONTEXT_MENU,
                    contextMenuDataProxy.setContextMenuData((item.data as EmployeeVO).name,
                            event.stageX, event.stageY, item.contextMenuData, selectedEmployees));
        });
        dateList.addEventListener(SelectionEvent.CHANGE_SELECTION, function(event:SelectionEvent):void
        {
            var dateIr:DateListItemRenderer = event.target as DateListItemRenderer;
            var index:int = dateIr.index;
            if(event.data == true){
                deselectAllEmployees();
                deselectAllShifts();
                deselectAllFreeDays();
                if(!keyboardProxy.isCtrl){
                    deselectAllDates();
                }
                plannerGrid.setVerticalSelection(index);
                unassignedShiftsGrid.setVerticalSelection(index);
                virtualEmployeeGrid.setVerticalSelection(index);
                selectedDates.addItem(dateIr);

            }else{
                if((selectedDates.length>1) && (!keyboardProxy.isCtrl)){
                    deselectAllDates();
                    dateIr.selected = true;
                }else{
                    plannerGrid.removeVerticalSelection((index));
                    unassignedShiftsGrid.removeVerticalSelection(index);
                    virtualEmployeeGrid.removeVerticalSelection(index);
                    selectedDates.removeItem(dateIr);
                }

            }
            sendNotification(ApplicationCommands.SELECT_OBJECT, new SelectedItemObject("date", selectedDates));

        });
        dateList.addEventListener(MouseEvent.RIGHT_CLICK, function (event:MouseEvent):void {
            var item:DateListItemRenderer = event.target as DateListItemRenderer;
            item.selected = true;
            sendNotification(ApplicationCommands.SET_CONTEXT_MENU,
                    contextMenuDataProxy.setContextMenuData((item.data as ShiftDateVO).date.toDateString(),
                            event.stageX, event.stageY, item.contextMenuData, selectedDates));
        });
        eventTimeline.unassignedPanelLabel.resizeButton.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void
        {
            if (event.target.selected){
                sendNotification(ApplicationCommands.EXPAND_UNASSIGNED_SHIFTS_PANEL);
            } else{
                sendNotification(ApplicationCommands.COLLAPSE_UNASSIGNED_SHIFTS_PANEL);
            }
        });
        eventTimeline.virtualEmployeePanelLabel.resizeButton.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void
        {
            if (event.target.selected){
                sendNotification(ApplicationCommands.EXPAND_VIRTUAL_EMPLOYEE_PANEL);
            } else{
                sendNotification(ApplicationCommands.COLLAPSE_VIRTUAL_EMPLOYEE_PANEL);
            }
        });
    }

    private function deselectAllEmployees():void {
        if(selectedEmployees.length){
            var eIr:EmployeesListItemRenderer;
            var dta:EmployeeVO;
            for each(eIr in selectedEmployees)
            {
                dta = eIr.data as EmployeeVO;
                eIr.setWeakSelection(false);
            }
            selectedEmployees.removeAll();
            plannerGrid.removeAllHorizontalSelections();
            sendNotification(ApplicationCommands.SELECT_OBJECT, new SelectedItemObject("employee", selectedEmployees));
        }
    }



    private function deselectAllDates():void {
        if(selectedDates.length){
            var dateIr:DateListItemRenderer;
            var dta:ShiftDateVO;
            for each(dateIr in selectedDates)
            {
                dta = dateIr.data as ShiftDateVO;
                dateIr.setWeakSelection(false);
            }
            plannerGrid.removeAllVerticalSelections();
            virtualEmployeeGrid.removeAllVerticalSelections();
            unassignedShiftsGrid.removeAllVerticalSelections();
            selectedDates.removeAll();
            sendNotification(ApplicationCommands.SELECT_OBJECT, new SelectedItemObject("date", selectedDates));
        }
    }

    private function updateNewLoadedData():void
    {
        var employeesAC:VectorCollection = new VectorCollection(eventsDataProxy.eventsData.employees.source);
        dateList.dataProvider = eventsDataProxy.eventsData.shiftDates;

        employeesAC.filterFunction = employeeFilter;
        employeesAC.refresh();
        employeesList.dataProvider = employeesAC;
        setEmployeesIndices(employeesAC);
        setShiftDatesIndices(dateList.dataProvider as VectorCollection);
    }

    private function refreshViewAfterNewLoadedData():void
    {
        collapseUnassignedShiftsGrid();
        refreshDataView();
        employeesList.removeAdditionalData();
        employeesList.setEmployeeHeight(Number(configProxy.configVO.itemHeight));

    }

    private function refreshViewAfterLoadNewData():void {
        callFunctionLater([
            new ActionData(updateNewLoadedData, this, null, 1, -1, "Dispatch new loaded data..."),
            new ActionData(createPlannerGrid, this, null, 1, -1, "Create Planner Grid..."),
            new ActionData(createVirtualEmployeeGrid, this, null, 1, -1, "Create Virtual Employee Grid..."),
            new ActionData(createUnassignedShiftsGrid, this, null, 1, -1, "Create Unassigned Shifts Grid..."),
            new ActionData(createEventRectangles, this, null, 1, -1, "Create Shifts..."),
            new ActionData(createNightTimeRectangles, this, null, 1, -1, "Create Night Time..."),
            new ActionData(createOffDaysRectangles, this, null, 1, -1, "Create Off Days Rectangles..."),
            new ActionData(createFreeDaysRectangles, this, null, 1, -1, "Create Free Days Rectangles..."),
            new ActionData(refreshViewAfterNewLoadedData, this, null, 1, -1, "Refresh view after new plan dispatched...."),
            new ActionData(initialBlockShifts, this, null, 1, -1, "Block shift if necessary..."),
            new ActionData(setElementsSizeTO, this, [true, "Refresh Size"], 1, -1, "Refresh elements size...")
        ]);
    }





    private function refreshSizeAfterTimeout(dispatch:Boolean=false, notification:String=null, body:String=null):void
    {
       callFunctionLater(new ActionData(setElementsSizeTO, this, [dispatch, notification, body], 1, -1, "Refresh elements size..."));
    }

    private function setElementsSizeTO(dispatch:Boolean=false, notification:String = "", body:String=""):void
    {

        if(plannerGrid == null){
            refreshSizeAfterTimeout(dispatch, notification, body);
            return;
        }
        dateList.setDayWidth(minimumZoomValue, true);

        if(eventTimeline.datesMeasurementList.visible && datesMeasurementList.dataProvider && datesMeasurementList.dataProvider.length){
            datesMeasurementList.setDayWidth(plannerGrid.elementWidth, true);
            datesMeasurementList.horizontalScrollPosition=plannerPanel.horizontalScrollPosition;
            datesMeasurementList.validateDisplayList();
            datesMeasurementList.visible = true;
        }
        if(employeesMeasurementList.dataProvider){
            employeesMeasurementList.setHeight(plannerGrid.elementHeight);
        }
        resizeGrid(minimumZoomValue);
        this.eventTimeline.refreshActualSize();

        enableUnassignedShiftsExpandButton();
        enableVirtualEmployeeExpandButton();

        if(notification){
            sendNotification(notification, true);
        }

        if(dispatch){
            executeQueueActionAfterTick(notification || body);
        }

    }

    private function clearLists():void
    {

        if(employeesList.dataProvider is VectorCollection){
            employeesList.dataProvider.removeAll();
            employeesList.dataProvider = null;
        }
        if(dateList.dataProvider is VectorCollection){
            dateList.dataProvider.removeAll();
            dateList.dataProvider = null;
        }

    }

    private static function setShiftDatesIndices(ac:VectorCollection):void
    {

        for each (var itm:ShiftDateVO in ac){
            itm.index = ac.getItemIndex(itm);
        }

    }

    private static function setEmployeesIndices(ac:VectorCollection):void
    {

        for each (var itm:EmployeeVO in ac){
            itm.index = ac.getItemIndex(itm);
        }

    }

    private static function employeeFilter(item:Object):Boolean
    {
        return item.id>-1;
    }

    private function clearMeasurement():void
    {
        if((datesMeasurementList.dataProvider && datesMeasurementList.dataProvider.length) || eventTimeline.dateMeasurementHeaderDG.visible){
            eventTimeline.dateMeasurementHeaderDG.visible =
                    eventTimeline.dateMeasurementHeaderDG.includeInLayout =
                            eventTimeline.datesMeasurementList.visible =
                                    eventTimeline.datesMeasurementList.includeInLayout = false;
            datesMeasurementList.dataProvider = null;
        }
        if((employeesMeasurementList.dataProvider && employeesMeasurementList.dataProvider.length) || eventTimeline.employeeMeasurementContainer.visible){
            eventTimeline.employeeMeasurementContainer.visible =
                    eventTimeline.employeeMeasurementContainer.includeInLayout = false;
            employeesMeasurementList.dataProvider = null;
            eventTimeline.employeeMeasurementHeaderDG.dataProvider = null;
            eventTimeline.dateMeasurementHeaderDG.dataProvider = null;
        }

    }

    private function setMeasurement():void
    {
        clearMeasurement();
        var employeesAC:VectorCollection = employeesList.dataProvider as VectorCollection;
        if((employeesAC==null) || employeesAC.length==0){
            return;
        }
        datesMeasurementList.visible = false;
        var measurement:VectorCollection = (employeesAC.getItemAt(0) as EmployeeVO).measurements;

        datesMeasurementList.dataProvider = dateList.dataProvider;
        employeesMeasurementList.dataProvider = employeesList.dataProvider;

        eventTimeline.employeeMeasurementHeaderDG.dataProvider =
                (measurement.length) ? measurement : null;

        measurement = (dateList.dataProvider.getItemAt(0) as ShiftDateVO).measurements;
        eventTimeline.dateMeasurementHeaderDG.dataProvider =
                (measurement.length) ? measurement : null;
        refreshSizeAfterTimeout(true, ApplicationCommands.UI_REFRESHED, "Set Measurement");

    }

    private function removePlannerGrid():void
    {
        if(plannerGrid){
            plannerGrid.visible = false;
            removeFreeDaysRectangles();
            removeOffDaysRectangles();

            plannerGrid.disposeGrid();
            plannerPanel.removeElement(plannerGrid);

            plannerGrid = null;
        }

    }

    private function createPlannerGrid():void
    {
        removePlannerGrid();

        plannerGrid = new PlannerTimeGrid();
        plannerGrid.setGrid(employeesList.dataProvider.length, eventsDataProxy.eventsData.shiftDates.length, minimumZoomValue, Number(configProxy.configVO.itemHeight));
        plannerGrid.setDisableArea(eventsDataProxy.solutionInfo.planningDateStartMinute);
        plannerPanel.addElement(plannerGrid);
    }

    private function initializePanelsEvents():void
    {
        unassignedShiftsPanel.addEventListener(DragEvent.DRAG_ENTER, function(event:DragEvent):void
        {
            if(event.dragSource.hasFormat("bmd")){
                DragManager.acceptDragDrop(event.currentTarget as IUIComponent);
                DragManager.showFeedback( DragManager.COPY );
                event.updateAfterEvent();
            }
        });
        unassignedShiftsPanel.addEventListener(DragEvent.DRAG_DROP, function(event:DragEvent):void
        {
            var rect:ShiftRectangle = event.dragInitiator as ShiftRectangle;
            if(rect && (rect.employee != null)){
                rect.employee = null;
                   ((event.currentTarget as Group).getElementAt(0) as PlannerTimeGrid).setElement(rect);
                    expandUnassignedShiftsGrid();
                    enableUnassignedShiftsExpandButton();
                    sendNotification(ApplicationCommands.CHANGE_SHIFT_ASSIGNMENT, rect);
            }
        });

        plannerPanel.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void
        {
            mousePositionX = event.stageX;
            mousePositionY = event.stageY;

        });

        plannerPanel.addEventListener(MouseEvent.MOUSE_MOVE, function(event:MouseEvent):void
        {
            if (event.buttonDown){
                if(event.altKey){
                    plannerPanel.layout.verticalScrollPosition += mousePositionY-event.stageY;

                    if(!DragManager.isDragging){
                        plannerPanel.layout.horizontalScrollPosition += mousePositionX-event.stageX;
                    }
                }
                mousePositionY=event.stageY;
                mousePositionX = event.stageX;
                event.updateAfterEvent();
            }

        });
        plannerPanel.addEventListener(PlannerTimeGrid.DAY_SHIFTS_OVERLOADED, function(event:Event):void
        {
            sendNotification(ApplicationCommands.SHOW_NOTIFICATION, "Nie można dodać pracownikowi danej zmiany.\nWybierz inną zmianę lub usuń elementy, żeby dodać inne.")
        });
        plannerPanel.addEventListener(DragEvent.DRAG_ENTER, function(event:DragEvent):void
        {
            if(event.dragSource.hasFormat("bmd")){
                if(!event.altKey){
                    DragManager.acceptDragDrop(event.currentTarget as IUIComponent);
                    DragManager.showFeedback( DragManager.COPY );
                    event.updateAfterEvent();
                }else{
                    event.stopImmediatePropagation();
                }
            }
        });
        plannerPanel.addEventListener(MouseEvent.MOUSE_WHEEL,  function(event:MouseEvent):void
         {
             var p:PlannerPanel = event.currentTarget as PlannerPanel;
             event.stopImmediatePropagation();
             var scroller:Scroller = (p.owner as IVisualElement).owner as Scroller;
             if(scroller.verticalScrollBar.visible){
                 p.verticalScrollPosition+=(event.delta*-20);
             }else if(scroller.horizontalScrollBar.visible){
                 p.horizontalScrollPosition+=(event.delta*-20);
             }

             event.preventDefault();
         }, true);
        plannerPanel.addEventListener(DragEvent.DRAG_DROP, function(event:DragEvent):void
        {
            if(!event.altKey){
                var rect:ShiftRectangle = event.dragInitiator as ShiftRectangle;
                if(rect){
                    var emplId:int = MathUtils.floor(event.localY/rect.employeeHight);
                    var isSet:Boolean;
                    var oldEmployee:EmployeeVO;
                    var grid:PlannerTimeGrid;
                    var empls:VectorCollection = employeesList.dataProvider as VectorCollection;
                    if(rect.employee != null){
                        if((emplId != rect.employee.index) || (rect.isVirtual)){
                            oldEmployee = rect.employee;
                            if(emplId>=empls.length){
                                isSet = false;
                            }else{
                                rect.employee = empls.getItemAt(emplId) as EmployeeVO;
                                grid = ((event.currentTarget as Group).getElementAt(0) as PlannerTimeGrid);
                                isSet = grid.setElement(rect);
                            }
                            if(isSet){
                                sendNotification(ApplicationCommands.CHANGE_SHIFT_ASSIGNMENT, rect);
                            }else{
                                rect.employee = oldEmployee;
                            }
                        }
                    }else{
                        oldEmployee = rect.employee;
                        if(emplId>=empls.length){
                            isSet = false;
                        }else{
                            rect.employee = empls.getItemAt(emplId) as EmployeeVO;
                            grid = ((event.currentTarget as Group).getElementAt(0) as PlannerTimeGrid);
                            isSet = grid.setElement(rect);
                        }
                        if(isSet){
                            if(unassignedShiftsGrid.expanded){
                                expandUnassignedShiftsGrid();
                            }
                            sendNotification(ApplicationCommands.CHANGE_SHIFT_ASSIGNMENT, rect);
                        }else{
                            rect.employee = oldEmployee;
                        }
                    }
                    enableUnassignedShiftsExpandButton();
                    enableVirtualEmployeeExpandButton();
                }
            }
        });
    }
    private function createNightTimeRectangles():void
    {
        var dates:VectorCollection = eventsDataProxy.eventsData.shiftDates;
        var date:ShiftDateVO;
        var employees:VectorCollection = eventsDataProxy.eventsData.employees;
        var employee:EmployeeVO;
        var contract:ContractVO;
        var nightTimeRectangle:NightContractRectangle;
        var itemHeight:Number = Number(configProxy.configVO.itemHeight);
        var startMinute:Number = eventsDataProxy.startMinute;
        var setNT:Function = plannerGrid.setNightTime;
        var zv:Number = minimumZoomValue;
        for each(employee in employees){

            for each(contract in employee.contracts){
                for each(date in dates){
                    nightTimeRectangle = new NightContractRectangle();
                    nightTimeRectangle.date = date;
                    nightTimeRectangle.startMinute = startMinute;
                    nightTimeRectangle.daySize = zv;
                    nightTimeRectangle.employeeHight = itemHeight;
                    nightTimeRectangle.employee = employee;
                    nightTimeRectangle.data = contract;
                    setNT(nightTimeRectangle);
                }
            }

        }
        eventTimeline.refreshActualSize();
        eventTimeline.visible = true;
    }

    private function createOffDaysRectangles():void
    {
        var employees:VectorCollection = eventsDataProxy.eventsData.employees;
        var employee:EmployeeVO;
        if(offDaysRectangles==null){
            offDaysRectangles = new Dictionary();
        }

        for each(employee in employees){
            createOffDaysRectangleForEmployee(employee);

        }

    }

   private function removeOffDaysRectangles():void
   {
       for (var key:Object in offDaysRectangles)
       {
           plannerGrid.removeOffDay(key as OffDayRectangle);
           delete offDaysRectangles[key];
       }
       offDaysRectangles = new Dictionary();
   }

    private function removeOffDaysRectangleForEmployee(employee:EmployeeVO):void
    {
        for (var key:Object in offDaysRectangles)
        {
            if((offDaysRectangles[key] as EmployeeVO) == employee){
                plannerGrid.removeOffDay(key as OffDayRectangle);
                delete offDaysRectangles[key];
            }
        }
    }
    private function createOffDaysRectangleForEmployee(employee:EmployeeVO):void
    {

        var daysOffCollection:VectorCollection = employee.daysOff;
        if(daysOffCollection.length){

            var offDay:DayOffVO;
            var offDayRectangle:OffDayRectangle;
            var dayOffColor:int = configProxy.getInt("dayOffColor");
            var itemHeight:Number = Number(configProxy.configVO.itemHeight);
            var startMinute:Number = eventsDataProxy.startMinute;
            var daysOffCollection:VectorCollection = employee.daysOff;

            var shiftDateVO:ShiftDateVO;

            var getShiftDateByDate:Function = eventsDataProxy.getShiftDateVOByDate;
            var setOffDay:Function = plannerGrid.setOffDay;
            var zv:Number = minimumZoomValue;

            for each(offDay in daysOffCollection){
                offDayRectangle = new OffDayRectangle();
                offDayRectangle.startMinute = startMinute;
                offDayRectangle.daySize = zv;
                offDayRectangle.employeeHight = itemHeight;
                offDayRectangle.color = dayOffColor;
                offDayRectangle.data = offDay;
                offDayRectangle.employee = employee;
                shiftDateVO = getShiftDateByDate(offDay.startDate);
                if(shiftDateVO){
                    offDayRectangle.minutesShift = shiftDateVO.minutesShift;
                }
                offDaysRectangles[offDayRectangle] = employee;
                setOffDay(offDayRectangle);
            }
        }


    }
    private function refreshOffDaysRectangles(employees:Vector.<EmployeeVO>):void
    {
        for each(var employee:EmployeeVO in employees){
            if(employee.daysOff.length){
                removeOffDaysRectangleForEmployee(employee);
            }
            createOffDaysRectangleForEmployee(employee);
        }
        executeQueueActionAfterTick("Refresh Off Days...");
    }

    private function refreshFreeDaysRectangles():void
    {
        for each(var freeDayRect:FreeDayRectangle in freeDays){
            freeDayRect.refreshData();
        }
        executeQueueActionAfterTick("Refresh Free Days");
    }

    private function removeFreeDaysRectangles():void
    {
        if(freeDays && freeDays.length){
            for each(var itm:FreeDayRectangle in freeDays){
                itm.removeEventListener(MouseEvent.RIGHT_CLICK, rightClichFreeDay);
                itm.removeEventListener(SelectionEvent.CHANGE_SELECTION, onChangeFreeDaySelection);
                plannerGrid.removeFreeDay(itm);
            }
        }

        freeDays = [];
    }
    private function createFreeDaysRectangles():void
    {
        removeFreeDaysRectangles();
        var plannigStartMinute:Number = eventsDataProxy.solutionInfo.planningDateStartMinute;
        var employees:VectorCollection = eventsDataProxy.eventsData.employees;
        var employee:EmployeeVO;
        var freeDay:FreeDayVO;
        var shiftDateVO:ShiftDateVO;
        var freeDayRectangle:FreeDayRectangle;
        var itemHeight:Number = Number(configProxy.configVO.itemHeight);
        var startMinute:Number = eventsDataProxy.startMinute;
        var getShiftDateByDate:Function = eventsDataProxy.getShiftDateVOByDate;
        var setFreeDay:Function = plannerGrid.setFreeDay;
        var zv:Number = minimumZoomValue;

        for each(employee in employees){

            for each(freeDay in employee.freeDays){

                shiftDateVO = getShiftDateByDate(freeDay.date);

                if(shiftDateVO){
                    freeDayRectangle = new FreeDayRectangle();
                    freeDays.push(freeDayRectangle);
                    freeDayRectangle.startMinute = startMinute;
                    freeDayRectangle.daySize = zv;
                    freeDayRectangle.employeeHight = itemHeight;
                    freeDayRectangle.setDisabledTime(plannigStartMinute);

                    freeDayRectangle.minutesShift = shiftDateVO.minutesShift;
                    freeDayRectangle.dayIndex = shiftDateVO ? shiftDateVO.index : -1;
                    freeDayRectangle.employee = employee;
                    //freeDayRectangle.isBlocked = freeDay.isBlocked;
                    freeDayRectangle.data = freeDay;
                    freeDayRectangle.addEventListener(MouseEvent.RIGHT_CLICK, rightClichFreeDay);
                    freeDayRectangle.addEventListener(SelectionEvent.CHANGE_SELECTION, onChangeFreeDaySelection);
                    setFreeDay(freeDayRectangle);
                }


            }

        }

    }

    private function onChangeFreeDaySelection(event:SelectionEvent):void
    {
        var rect:FreeDayRectangle = event.currentTarget as FreeDayRectangle;
        if(rect.selected){
            deselectAllEmployees();
            deselectAllDates();
            deselectAllShifts();
            if(!keyboardProxy.isCtrl){
                deselectAllFreeDays();
            }
            if(!selectedFreeDays.contains(rect)){
                selectedFreeDays.addItem(rect);
                sendNotification(ApplicationCommands.SELECT_OBJECT,
                        new SelectedItemObject("freeDay", selectedFreeDays));
            }

        }else{
            if(selectedFreeDays.contains(rect)){
                if((selectedFreeDays.length>1) && (!keyboardProxy.isCtrl)){
                    deselectAllFreeDays(false);
                    rect.selected = true;
                }else{
                    selectedFreeDays.removeItem(rect);
                }
                sendNotification(ApplicationCommands.SELECT_OBJECT,
                        new SelectedItemObject("freeDay", selectedFreeDays));

            }

        }
    }
    private function rightClichFreeDay(event:MouseEvent):void
    {
        var fdRect:FreeDayRectangle = event.target as FreeDayRectangle;
        if(fdRect){
            if(!fdRect.isOutOfPlan){
                sendNotification(ApplicationCommands.SET_CONTEXT_MENU,
                        contextMenuDataProxy.setContextMenuData("Dzień wolny",
                                event.stageX, event.stageY, fdRect.contextMenuData));
            }
        }
    }

    private function deselectAllFreeDays(dispatch:Boolean=true):void
    {
        if(selectedFreeDays.length){
            var rect:FreeDayRectangle;
            for each(rect in selectedFreeDays)
            {
                rect.setWeakSelection(false);
            }
            selectedFreeDays.removeAll();
            if(dispatch){
                sendNotification(ApplicationCommands.SELECT_OBJECT,
                        new SelectedItemObject("freeDay", selectedFreeDays));
            }

        }

    }

    private function removeEventRectangles():void
    {

        if((events != null) && events.length){
            var rect:ShiftRectangle;
            var val:Object;
            for each(val in events)
            {
                rect = val as ShiftRectangle;
                rect.visible = false;
                rect.removeEventListener(SelectionEvent.CHANGE_SELECTION, onChangeShiftSelection);
                rect.removeEventListener(MouseEvent.RIGHT_CLICK, rightClichShift);
                if(rect.isVirtual){
                    virtualEmployeeGrid.removeElement(rect);
                }else if(rect.isUnassigned){
                    unassignedShiftsGrid.removeElement(rect);
                }else{
                    plannerGrid.removeElement(rect);
                }
                rect.dispose();
                delete events[val];
                rect = null;
            }
            events = new Dictionary();
        }

        events = new Dictionary();

    }
    private function createEventRectangles():void
    {

        removeEventRectangles();
        var plannigStartMinute:Number = eventsDataProxy.solutionInfo.planningDateStartMinute;
        var allShifts:VectorCollection = eventsDataProxy.eventsData.shifts;
        var eventRectangle:ShiftRectangle;
        var shift:ShiftVO;
        var shftDte:ShiftDateVO;
        var rectColor:int = configProxy.getInt("defaultShiftColor");
        var itemHeight:Number = Number(configProxy.configVO.itemHeight);
        var startMinute:Number = eventsDataProxy.startMinute;
        var zv:Number = minimumZoomValue;
        for each(shift in allShifts){
            eventRectangle = new ShiftRectangle();

            eventRectangle.color = rectColor;
            eventRectangle.startMinute = startMinute;
            eventRectangle.daySize = zv;
            eventRectangle.setDisabledTime(plannigStartMinute);
            eventRectangle.employeeHight = itemHeight;
            events[shift] = eventRectangle;
            shftDte = eventsDataProxy.getShiftDateVOByDate(shift.startDate);
            if(shftDte){
                eventRectangle.dayIndex = shftDte.index;
                eventRectangle.minutesShift = shftDte.minutesShift;
            }
            eventRectangle.data = shift;
            eventRectangle.addEventListener(MouseEvent.RIGHT_CLICK, rightClichShift);
            eventRectangle.addEventListener(SelectionEvent.CHANGE_SELECTION, onChangeShiftSelection);
        }
    }

    private function rightClichShift(event:MouseEvent):void
    {
        var shiftRect:ShiftRectangle = event.target as ShiftRectangle;

        if(!shiftRect.isOutOfPlan){
            if(shiftRect.hitTestMouse(event)){
                shiftRect.selected = true;
                sendNotification(ApplicationCommands.SET_CONTEXT_MENU,
                        contextMenuDataProxy.setContextMenuData(shiftRect.data.name,
                                event.stageX, event.stageY, shiftRect.contextMenuData, selectedShifts));
            }

        }
    }
    private function deselectAllItems():void
    {
        deselectAllEmployees();
        deselectAllDates();
        deselectAllShifts();
        deselectAllFreeDays();
    }



    private function onChangeShiftSelection(event:SelectionEvent):void
    {


        var rect:ShiftRectangle = event.currentTarget as ShiftRectangle;
        if(rect.selected){
            deselectAllEmployees();
            deselectAllDates();
            deselectAllFreeDays();
            if(!keyboardProxy.isCtrl){
                deselectAllShifts();
            }
            if(!selectedShifts.contains(rect)){
                selectedShifts.addItem(rect);
                sendNotification(ApplicationCommands.SELECT_OBJECT,
                        new SelectedItemObject("shift", selectedShifts));
            }

        }else{
            if(selectedShifts.contains(rect)){
                if((selectedShifts.length>1) && (!keyboardProxy.isCtrl)){
                    deselectAllShifts(false);
                    rect.selected = true;
                }else{
                    selectedShifts.removeItem(rect);
                }
                sendNotification(ApplicationCommands.SELECT_OBJECT,
                        new SelectedItemObject("shift", selectedShifts));

            }

        }
    }

    private function deselectAllShifts(dispatch:Boolean=true):void
    {
        if(selectedShifts.length){
            var rect:ShiftRectangle;
            for each(rect in selectedShifts)
            {
                rect.setWeakSelection(false);
            }
            selectedShifts.removeAll();
            if(dispatch){
                sendNotification(ApplicationCommands.SELECT_OBJECT,
                        new SelectedItemObject("shift", selectedShifts));
            }

        }

    }

    private function showAdditionalData():void
    {
            var additionalSet:VectorCollection = this.additionalRectanglesSet;
            var item:AdditionalShiftRectangle;
            var employee:EmployeeVO;
            var planIndex:int = additionalEventsDataProxy.eventsCollection.length - 1;
            var f1:Function = eventsDataProxy.getEmployeeVOById;
            var f2:Function = plannerGrid.setAdditionalShiftRectangle;
            var h:Number = plannerGrid.elementHeight;
            for each(item in additionalSet) {
                employee = f1(item.employeeId);
                if(employee != null){
                    item.employee = employee;
                    item.employeeHight = h;
                    f2(item, planIndex);
                }
            }
            if(item != null){
                item.employeeHight = plannerGrid.elementHeight;
            }

            executeQueueActionAfterTick("Show archival plan");


    }
    private function parseAdditionalData():void
    {



        additionalRectanglesSet.removeAll();

        var additionalPlannerVO:AdditionalPlannerSolutionVO = additionalEventsDataProxy.getEventsData(additionalEventsDataProxy.pointer+1) as AdditionalPlannerSolutionVO;


        var planIndex:int = additionalEventsDataProxy.eventsCollection.length - 1;
        var plannigStartMinute:Number = eventsDataProxy.solutionInfo.planningDateStartMinute;
        var shifts:VectorCollection = additionalPlannerVO.shifts;
        var shiftAssignments:VectorCollection = additionalPlannerVO.shiftAssignments;
        var shiftAssignment:ShiftAssignmentVO;
        var shiftRect:AdditionalShiftRectangle;
        var shift:ShiftVO;
        var employee:EmployeeVO;
        var rectColor:int = configProxy.getInt("additionalShiftColor");
        var startMinute:Number = eventsDataProxy.startMinute;
        var itemHeight:Number = Number(configProxy.configVO.itemHeight);
        var geteDateByDate:Function = eventsDataProxy.getShiftDateVOByDate;
        var geteEmployeeByRefId:Function = additionalEventsDataProxy.getEmployeeVOByReferenceId;
        var addRectToSet:Function = additionalRectanglesSet.addItem;

        var shftDte:ShiftDateVO;
        var zv:Number = minimumZoomValue;

        for each(shift in shifts) {
            shiftRect = new AdditionalShiftRectangle();
            shiftRect.color = rectColor;
            shiftRect.employeeHight = itemHeight;
            shiftRect.startMinute = startMinute;
            shiftRect.daySize = zv;
            shiftRect.setDisabledTime(plannigStartMinute);
            shiftRect.data = shift;
            shftDte = geteDateByDate(shift.startDate);
            if (shftDte) {
                shiftRect.dayIndex = shftDte.index;
                shiftRect.minutesShift = shftDte.minutesShift;
            }
            for each(shiftAssignment in shiftAssignments){
                if(shiftAssignment.shiftReference == shift.referenceId){
                    employee = geteEmployeeByRefId(planIndex, shiftAssignment.employeeReference);
                }
                if(employee!=null){
                    shiftRect.employeeId = employee.id;
                    addRectToSet(shiftRect);
                    employee = null;
                    break;
                }
            }
        }
        employeesList.setAdditionalData(planIndex);
        if(planIndex==0){
            resizeGridHeight(90);
        }else{
            resizeGridHeight(120);
        }
        callFunctionLater(new ActionData(showAdditionalData, this, null, 1, -1, "Additional Data Parsed"));
    }

    private function refreshShiftsAsssignments(data:Vector.<ShiftAssignmentVO>):void
    {
        if(plannerGrid != null){
            var assignment:ShiftAssignmentVO;
            var rect:ShiftRectangle;
            var f:Function = eventsDataProxy.getShiftVOById;
            var f1:Function = eventsDataProxy.getEmployeeVOById;

            var vrtEmplSetEl:Function = virtualEmployeeGrid.setElement;
            var unsEmplSetEl:Function = unassignedShiftsGrid.setElement;
            var emplSetEl:Function = plannerGrid.setElement;

            var oldEmployeeVO:EmployeeVO;
            var isSet:Boolean;

            for each(assignment in data){
                rect = events[f(assignment.shiftReference)] as ShiftRectangle;
                if(rect){
                    oldEmployeeVO = rect.employee;
                    rect.shiftAssignment = assignment;
                    rect.employee = f1(assignment.employeeReference);
                    if(rect.isVirtual){
                        vrtEmplSetEl(rect);
                    }else if(rect.isUnassigned){
                        unsEmplSetEl(rect);
                    }else{
                        isSet = emplSetEl(rect);
                        if(!isSet){
                            rect.employee = oldEmployeeVO;
                        }
                    }
                }
            }
            enableUnassignedShiftsExpandButton();
            enableVirtualEmployeeExpandButton();
            executeQueueActionAfterTick("Shift Assignments refreshed...");
        }

    }
    private function refreshDataView():void
    {
        if(plannerGrid!=null){
            var rect:ShiftRectangle;
            var assignment:ShiftAssignmentVO;
            var val:Object;
            var oldEmployeeVO:EmployeeVO;
            var isSet:Boolean;
            var f:Function = eventsDataProxy.getShiftAssignmentByShiftID;
            var f1:Function = eventsDataProxy.getEmployeeVOByReferenceId;
            var vrtEmplSetEl:Function = virtualEmployeeGrid.setElement;
            var unsEmplSetEl:Function = unassignedShiftsGrid.setElement;
            var emplSetEl:Function = plannerGrid.setElement;
            for each(val in events)
            {
                rect = val as ShiftRectangle;
                assignment = f(rect.data.referenceId);
                if(assignment){
                    oldEmployeeVO = rect.employee;
                    rect.shiftAssignment = assignment;
                    rect.employee = f1(assignment.employeeReference);

                    if(rect.isVirtual){
                        vrtEmplSetEl(rect);
                    }else if(rect.isUnassigned){
                        rect.refreshShiftAssignments();
                        unsEmplSetEl(rect);
                    }else{
                        isSet = emplSetEl(rect);
                        if(!isSet){
                            rect.employee = oldEmployeeVO;
                        }else{
                            rect.refreshShiftAssignments();
                        }
                    }
                }else{
                    unsEmplSetEl(rect);
                }
            }
            if(unassignedShiftsGrid.expanded){
                expandUnassignedShiftsGrid();
            }
            enableUnassignedShiftsExpandButton();
            enableVirtualEmployeeExpandButton();
        }
    }

    private function removeUnassignedShiftsGrid():void
    {
        if(unassignedShiftsGrid != null){

            unassignedShiftsGrid.visible = false;
            unassignedShiftsGrid.disposeGrid();
            collapseUnassignedShiftsGrid();
            unassignedShiftsPanel.removeElement(unassignedShiftsGrid);
            unassignedShiftsGrid = null;
        }
    }

    private function enableVirtualEmployeeExpandButton():void
    {
        var val:Boolean = virtualEmployeeGrid.getShiftsContentHeight(0)>Number(configProxy.configVO.collapseHeight);
        eventTimeline.virtualEmployeePanelLabel.resizeButton.visible = val;
        if(val){
            eventTimeline.virtualEmployeePanelLabel.resizeButton.selected = virtualEmployeeGrid.expanded;
        }
    }

    private function enableUnassignedShiftsExpandButton():void
    {
        var val:Boolean = unassignedShiftsGrid.getShiftsContentHeight(0)>Number(configProxy.configVO.collapseHeight);
        eventTimeline.unassignedPanelLabel.resizeButton.visible = val;
        if(val){
            eventTimeline.unassignedPanelLabel.resizeButton.selected = unassignedShiftsGrid.expanded;
        }
    }

    private function expandUnassignedShiftsGrid():void
    {
        var collapseHgh:Number = Number(configProxy.configVO.collapseHeight);
        var expandHgh:Number = Number(configProxy.configVO.expandHeight);
        var hgh:Number = unassignedShiftsGrid.getShiftsContentHeight(0);
        unassignedShiftsGrid.elementHeight = hgh<collapseHgh ? collapseHgh : hgh;
        var h:Number = hgh>collapseHgh ? hgh<expandHgh ? hgh : expandHgh : collapseHgh;
        unassignedShiftsPanel.height = h
        unassignedShiftsGrid.expanded = h > collapseHgh;
    }
    private function collapseUnassignedShiftsGrid():void
    {
        var h:Number = Number(configProxy.configVO.collapseHeight);
        if(unassignedShiftsGrid){
            unassignedShiftsGrid.expanded = false;
            unassignedShiftsGrid.elementHeight = h;
        }
        unassignedShiftsPanel.height = h;
    }

    private function createUnassignedShiftsGrid():void
    {

        removeUnassignedShiftsGrid();
        unassignedShiftsGrid = new PlannerTimeGrid();
        unassignedShiftsGrid.backgroundColor = 0xf6f6f6;
        unassignedShiftsGrid.setGrid(1, eventsDataProxy.eventsData.shiftDates.length, minimumZoomValue, Number(configProxy.configVO.collapseHeight));
        unassignedShiftsGrid.setDisableArea(eventsDataProxy.solutionInfo.planningDateStartMinute);
        unassignedShiftsPanel.addElement(unassignedShiftsGrid);
    }

    private function expandVirtualEmployeeGrid():void
    {
        var collapseHgh:Number = Number(configProxy.configVO.collapseHeight);
        var expandHgh:Number = Number(configProxy.configVO.expandHeight);
        var hgh:Number = virtualEmployeeGrid.getShiftsContentHeight(0);
        virtualEmployeeGrid.elementHeight = hgh<collapseHgh ? collapseHgh : hgh;
        var h:Number = hgh>collapseHgh ? hgh<expandHgh ? hgh : expandHgh : collapseHgh;;
        virtualEmployeePanel.height = h;
        virtualEmployeeGrid.expanded = h > collapseHgh;
    }
    private function collapseVirtualEmployeeGrid():void
    {
        var h:Number = Number(configProxy.configVO.collapseHeight);
        if(virtualEmployeeGrid){
            virtualEmployeeGrid.expanded = false;
            virtualEmployeeGrid.elementHeight = h;
        }
        virtualEmployeePanel.height = Number(configProxy.configVO.collapseHeight)
    }


    private function removeVirtualEmployeeGrid():void
    {
        if(virtualEmployeeGrid != null){
            virtualEmployeeGrid.disposeGrid();
            virtualEmployeePanel.removeElement(virtualEmployeeGrid);
            virtualEmployeeGrid = null;
        }
    }
    private function createVirtualEmployeeGrid():void
    {
        removeVirtualEmployeeGrid();
        virtualEmployeeGrid = new PlannerTimeGrid();
        virtualEmployeeGrid.backgroundColor = 0xfcfcfc;
        virtualEmployeeGrid.setGrid(1, eventsDataProxy.eventsData.shiftDates.length, minimumZoomValue, Number(configProxy.configVO.collapseHeight));
        virtualEmployeeGrid.setDisableArea(eventsDataProxy.solutionInfo.planningDateStartMinute);
        virtualEmployeePanel.addElement(virtualEmployeeGrid);
    }


    private function scrollToShift(shiftId:int):void
    {
        var rect:ShiftRectangle = events[eventsDataProxy.getShiftVOById(shiftId)] as ShiftRectangle;

        sendNotification(ApplicationCommands.CHANGE_ZOOM, 10);
        callFunctionLater(new ActionData(scrollAfterZoom, this, [rect], 1, -1, "Scroll to shift."));

    }

    private function scrollAfterZoom(rect:ShiftRectangle):void
    {
        var rectContainer:IUIComponent = rect.parent as IUIComponent;
        rect.selected = true;
        plannerPanel.verticalScrollPosition=rectContainer.y;
        plannerPanel.horizontalScrollPosition=rect.x;
    }
    private function removeScrollEventsListeners():void
    {
            plannerPanel.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, plannerSroll);
    }

    private function addScrollEventsListeners():void
    {
        removeScrollEventsListeners()
            plannerPanel.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, plannerSroll);

    }

    private function plannerSroll(event:PropertyChangeEvent):void
    {
        if(event.property == "horizontalScrollPosition"){
            datesMeasurementList.horizontalScrollPosition=
                    unassignedShiftsPanel.horizontalScrollPosition=
                            virtualEmployeePanel.horizontalScrollPosition=
                                    dateList.horizontalScrollPosition=Number(event.newValue);
        }else if(event.property == "verticalScrollPosition"){
            employeesMeasurementList.verticalScrollPosition=
                    employeesList.verticalScrollPosition=Number(event.newValue);
        }
    }

    private function get brokenRulesDataProxy():BrokenRulesDataProxy
    {
        return facade.retrieveProxy(BrokenRulesDataProxy.NAME) as BrokenRulesDataProxy;
    }

    private function get configProxy():ConfigProxy
    {
        return facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
    }

    private function get additionalEventsDataProxy():AdditionalEventsDataProxy
    {
        return facade.retrieveProxy(AdditionalEventsDataProxy.NAME) as AdditionalEventsDataProxy;
    }

    private function get eventsDataProxy():EventsDataProxy
    {
        return facade.retrieveProxy(EventsDataProxy.NAME) as EventsDataProxy;
    }

    private function get contextMenuDataProxy():ContextMenuDataProxy
    {
        return facade.retrieveProxy(ContextMenuDataProxy.NAME) as ContextMenuDataProxy;
    }

    private function get unassignedShiftsPanel():UnassignedShiftsPanel
    {
        return eventTimeline.unassignedShiftsPanel;
    }

    private function get virtualEmployeePanel():VirtualEmployeePanel
    {
        return eventTimeline.virtualEmployeePanel;
    }

    private function get plannerPanel():PlannerPanel
    {
        return eventTimeline.plannerPanel;
    }

    private function get employeesMeasurementList():EmployeesMeasurementList
    {
        return eventTimeline.employeesMeasurementList;
    }

    private function get employeesList():EmployeesList
    {
        return eventTimeline.employeesList;
    }

    private function get dateList():DateList
    {
        return eventTimeline.dateList;
    }

    private function get datesMeasurementList():DatesMeasurementList
    {
        return eventTimeline.datesMeasurementList;
    }
    private function get eventTimeline():EventTimeline
    {
        return this.viewComponent as EventTimeline;
    }
}
}



package pl.pkpik.epdp7.eventtimeline.view.mediator
{
    import flash.events.MouseEvent;
    import mx.events.FlexEvent;

    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;
    import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.ConfigProxy;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.ResourcesMonitorProxy;
    import pl.pkpik.epdp7.eventtimeline.util.MathUtils;
    import pl.pkpik.epdp7.eventtimeline.view.component.ZoomPanel;
    import spark.components.HSlider;

    public class ZoomPanelMediator extends Mediator
	{
		
		public static const NAME:String = "ZoomPanelMediator";
		public function ZoomPanelMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
		}

        override public function listNotificationInterests():Array
        {
            return [
                ResourcesMonitorProxy.LOADING_COMPLETE,
                ApplicationCommands.MEASUREMENT_ALREADY_SET,
                ApplicationCommands.SET_EVENTS_DATA,
                ApplicationCommands.CHANGE_ZOOM,
                ApplicationCommands.ENABLE_BROKEN_RULES_BUTTON

            ];
        }

        override public function handleNotification( note:INotification ):void {
            switch (note.getName()) {
                case ApplicationCommands.MEASUREMENT_ALREADY_SET:
                    typedComponent.employeeMeasurementButton.enabled =
                            typedComponent.dateMeasurementButton.enabled = true;
                    if(typedComponent.employeeMeasurementButton.selected){
                        sendNotification(ApplicationCommands.SHOW_EMPLOYEE_MEASUREMENT);
                    }
                    if(typedComponent.dateMeasurementButton.selected){
                        sendNotification(ApplicationCommands.SHOW_DATE_MEASUREMENT);
                    }
                    break;
                case ApplicationCommands.SET_EVENTS_DATA:
                    typedComponent.brokenRulesButton.enabled =
                            typedComponent.employeeMeasurementButton.enabled =
                                typedComponent.dateMeasurementButton.enabled = false;
                    break;
                case ApplicationCommands.ENABLE_BROKEN_RULES_BUTTON:
                    typedComponent.brokenRulesButton.enabled = Boolean(note.getBody());
                        if(typedComponent.brokenRulesButton.enabled){
                            typedComponent.brokenRulesButton.selected = true;
                            sendNotification(ApplicationCommands.SHOW_BROKEN_RULES);
                        }
                    break;
                case ResourcesMonitorProxy.LOADING_COMPLETE:
                    typedComponent.applicationButton.label = String(configProxy.configVO.applicationButtonLabel);
                    break;
                case ApplicationCommands.CHANGE_ZOOM:
                    var val:Number = Number(note.getBody());
                    typedComponent.hSlider.value = val;
                    setNewZoomValue(val);
                    break;
            }
        }
		
		override public function onRegister():void
		{

            typedComponent.hSlider.addEventListener(FlexEvent.CHANGE_END, function(event:FlexEvent):void
            {
                var currentSlider:HSlider=HSlider(event.currentTarget);
                setNewZoomValue(currentSlider.value);
            });
            typedComponent.employeeMeasurementButton.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void
            {
                if (event.target.selected){
                    sendNotification(ApplicationCommands.SHOW_EMPLOYEE_MEASUREMENT);
                } else{
                    sendNotification(ApplicationCommands.HIDE_EMPLOYEE_MEASUREMENT);
                }
            });
            typedComponent.dateMeasurementButton.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void
            {
                if (event.target.selected){
                    sendNotification(ApplicationCommands.SHOW_DATE_MEASUREMENT);
                } else{
                    sendNotification(ApplicationCommands.HIDE_DATE_MEASUREMENT);
                }
            });
            typedComponent.brokenRulesButton.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void
            {
                if (event.target.selected){
                    sendNotification(ApplicationCommands.SHOW_BROKEN_RULES);
                } else{
                    sendNotification(ApplicationCommands.HIDE_BROKEN_RULES);
                }
            });

            typedComponent.applicationButton.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void
            {
                sendNotification(ApplicationCommands.RUN_APPLICATION_BUTTON_COMMAND);
            });

		}
        private function setNewZoomValue(value:Number):void
        {
            sendNotification(ApplicationCommands.ZOOM_VIEW, value);
            typedComponent.zoomValLabel.text = getFormattedPercent(value);
        }
        public static function getFormattedPercent(value:Number):String
        {
            if(value==0) {
                return "100%";
            }
            if(value>0){
                return MathUtils.round(100+value*20)+"%";
            }
            return MathUtils.round(100+value*8)+"%";
        }

        private function get configProxy():ConfigProxy
        {
            return facade.retrieveProxy(ConfigProxy.NAME) as ConfigProxy;
        }
		
		private function get typedComponent():ZoomPanel
		{
			return this.viewComponent as ZoomPanel;
		}
	}
}
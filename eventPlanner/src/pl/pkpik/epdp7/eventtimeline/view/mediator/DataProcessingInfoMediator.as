package pl.pkpik.epdp7.eventtimeline.view.mediator {

import org.puremvc.as3.interfaces.INotification;
import org.puremvc.as3.patterns.mediator.Mediator;

import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
import pl.pkpik.epdp7.eventtimeline.view.component.DataProcessingInfo;

public class DataProcessingInfoMediator extends Mediator {
    public static const NAME:String = "DataProcessingInfoMediator";
    public function DataProcessingInfoMediator(viewComponent:Object = null) {
        super(NAME, viewComponent);
    }

    override public function onRegister():void
    {


    }

    override public function listNotificationInterests():Array {
        return [
            ApplicationCommands.ACTIONS_START_EXECUTE,
            ApplicationCommands.ACTIONS_STOP_EXECUTE,
            ApplicationCommands.ACTION_EXECUTED
        ];
    }

    override public function handleNotification( note:INotification ):void {

        var body:Object = note.getBody();
        var ad:ActionData = body as ActionData;
        var description:String = "";
        if(ad){
            description = ad.description;
        }else if(body is String){
            description = body as String;
        }
        switch (note.getName()) {
            ///case ApplicationCommands.ACTIONS_START_EXECUTE:
               // dataProcessingInfo.logInfo = "START: " + description;
            //    break;
            case ApplicationCommands.ACTION_EXECUTED:
                dataProcessingInfo.logInfo = "<font face='DinBold' color='#009900'>ACTION: </font>" + description;
                break;
           // case ApplicationCommands.ACTIONS_STOP_EXECUTE:
                //dataProcessingInfo.logInfo = "END.";
            //    break;
        }
    }

    private function get dataProcessingInfo():DataProcessingInfo
    {
        return this.viewComponent as DataProcessingInfo;
    }
}
}

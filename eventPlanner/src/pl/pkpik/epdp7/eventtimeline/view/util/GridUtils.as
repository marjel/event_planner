package pl.pkpik.epdp7.eventtimeline.view.util {
    import org.apache.flex.collections.VectorCollection;

    import pl.pkpik.epdp7.eventtimeline.view.component.IndexedVGroup;

    public final class GridUtils {


        public function getShiftsContentHeight(rowID:uint, groupsContainers:VectorCollection):Number {
            var max:Number = 0;
            var group:IndexedVGroup;
            for each(group in groupsContainers) {
                if (group.vIndex == rowID) {
                    if (group.height > max) {
                        max = group.height;
                    }
                }
            }
            return max;
        }
    }
}

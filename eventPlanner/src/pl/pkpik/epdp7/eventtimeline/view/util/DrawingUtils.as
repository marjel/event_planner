
package pl.pkpik.epdp7.eventtimeline.view.util {
    import flash.display.CapsStyle;
    import flash.display.Graphics;
    import flash.display.JointStyle;
    import flash.display.LineScaleMode;

    import mx.graphics.SolidColor;
    import mx.graphics.SolidColorStroke;

    import spark.primitives.Rect;

    public final class DrawingUtils {

        public static function getRectangle(h:Number, c:int=0xffc602, a:Number=0):Rect {
            var rect:Rect = new Rect();
            rect.fill = new SolidColor(c);
            rect.stroke = new SolidColorStroke(c, 2, a, true, LineScaleMode.NONE, CapsStyle.SQUARE, JointStyle.MITER);
            rect.height = h;
            return rect;
        }
        public static function drawVerticalLine(gr:Graphics, w:Number, h:Number, c:int=0xfcfcfc):void {

        }
        public static function drawHorizontallLine(gr:Graphics, w:Number, c:int=0xbb0000):void {
            gr.clear();
            gr.lineStyle(1, c, 1, true, LineScaleMode.NONE, CapsStyle.SQUARE, JointStyle.MITER, 4);
            gr.moveTo(0, 0);
            gr.lineTo(w, 0);
        }
        public static function drawRectangle(gr:Graphics, w:Number, h:Number, c:int=0xfcfcfc, a:Number=.44):void {
            gr.clear();
            gr.moveTo(0, 0);
            gr.beginFill(c, a);
            gr.drawRect(0, 0, w, h);
            gr.endFill();
        }
        public static function DrawDash(target:Graphics, x1:Number, y1:Number, x2:Number, y2:Number, dashLength:Number=5, spaceLength:Number=5):void
        {

            var x:Number = x2 - x1;
            var y:Number = y2 - y1;
            var hyp:Number = Math.sqrt((x) * (x) + (y) * (y));
            var units:Number = hyp / (dashLength + spaceLength);
            var dashSpaceRatio:Number = dashLength / (dashLength + spaceLength);
            var dashX:Number = (x / units) * dashSpaceRatio;
            var spaceX:Number = (x / units) - dashX;
            var dashY:Number = (y / units) * dashSpaceRatio;
            var spaceY:Number = (y / units) - dashY;

            target.moveTo(x1, y1);
            while (hyp > 0)
            {
                x1 += dashX;
                y1 += dashY;
                hyp -= dashLength;
                if (hyp < 0)
                {
                    x1 = x2;
                    y1 = y2;
                }
                target.lineTo(x1, y1);
                x1 += spaceX;
                y1 += spaceY;
                target.moveTo(x1, y1);
                hyp -= spaceLength;
            }
            target.moveTo(x2, y2);
        }
    }
}

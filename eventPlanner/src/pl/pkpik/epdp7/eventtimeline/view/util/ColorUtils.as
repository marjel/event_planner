/**
 * Created by mariel on 07.03.15.
 */
package pl.pkpik.epdp7.eventtimeline.view.util {

    import pl.pkpik.epdp7.eventtimeline.util.MathUtils;

    public final class ColorUtils {

        /**
         * Return the same color, but lighter.
         *
         * @param ratio   The amount of white added.  0 returns the same color, 1 returns white.
         */
        public static function lighter(color:Number, ratio:Number = 0.5):Number
        {
            var hsv:Object = ColorUtils.RGBToHSV(color);
            hsv.s *= (1-ratio);
            return ColorUtils.HSVToRGB( hsv );
        }

        /**
         * Return the same color, but darker.
         *
         * @param ratio   The amount of black added.  0 returns the same color, 1 returns black.
         */
        public static function darker(color:Number, ratio:Number = 0.5):Number
        {
            var hsv:Object = ColorUtils.RGBToHSV(color);
            hsv.v *= (1-ratio);
            return ColorUtils.HSVToRGB( hsv );
        }


        /**
         * Achromatic RGB color
         * Return the corresponding achromatic color (greyscale)
         */
        public static function getAchromatic(color:Number):Number
        {
            var hsvColor:Object=ColorUtils.RGBToHSV(color);

            hsvColor.s=0;

            return ColorUtils.HSVToRGB(hsvColor);
        }

        /**
         * Return the inverted RGB color
         */
        public static function getInverted(color:Number):Number
        {

            return ColorUtils.HSVToRGB(ColorUtils.getInvertedHSV(ColorUtils.RGBToHSV(color)));
        }


        /**
         * Return the normalised luminosity from 0 to 1
         * Luminosity is calculated from rgb
         * @see http://www.w3.org/Graphics/Color/sRGB
         * @see http://www.w3.org/TR/2006/WD-WCAG20-20060427/appendixA.html#luminosity-contrastdef
         *
         */
        public static function getLuminosity(color:Number):Number{
            var rgb:Object= ColorUtils.RGBToRGBComponents(color);


            //with 2.2 gamma correction
            return 0.2126*Math.pow(rgb.red,2.2) + 0.7152*Math.pow(rgb.green,2.2) + 0.0722*Math.pow(rgb.blue,2.2);

        }

        /**
         * Return the luminosity contrast ratio from color1 and color2
         * from 1 to 21
         * Good contrast is >5
         * @see http://www.w3.org/Graphics/Color/sRGB
         * @see http://www.w3.org/TR/2006/WD-WCAG20-20060427/appendixA.html#luminosity-contrastdef
         *
         */
        public static function getLuminosityRatio(color1:Number,color2:Number):Number{
            var l1:Number=ColorUtils.getLuminosity(color1);
            var l2:Number=ColorUtils.getLuminosity(color2);
            if(l1>l2){
                return (l1+0.05)/(l2+0.05);
            }
            else{
                return (l2+0.05)/(l1+0.05);
            }

        }

        private static function RGBToRGBComponents(color:Number):Object{
            var rgb:Object = {};

            rgb.red =  (color >> 16) / 255;
            rgb.green = (0xFF & (color >> 8)) / 255;
            rgb.blue = (0xFF & color)/ 255;

            return rgb;
        }

        private static function getInvertedHSV(hsv:Object):Object
        {
            var invertedHSV:Object= {};
            invertedHSV.h=360-hsv.h;//(hsv.h % 360);
            invertedHSV.s=hsv.s;
            invertedHSV.v=1-hsv.v;

            return invertedHSV;
        }
        private static function HSVToRGB(hsv:Object):Number
        {
            var dst:Number;

            var h:Number = hsv.h;
            var s:Number = hsv.s;
            var v:Number = hsv.v;

            var floor:Function = MathUtils.floor;

            var r:Number,g:Number,b:Number;
            if( s == 0 || h==-1) {
                v = floor(v*255);
                dst = (v << 16) + (v << 8) + v;
                return dst;
            }
            h /= 60;
            var i:Number = floor( h );
            var f:Number = h - i;
            var p:Number = v * ( 1 - s );
            var q:Number = v * ( 1 - s * f );
            var t:Number = v * ( 1 - s * ( 1 - f ) );
            switch( i ) {
                case 0:
                    r = v;
                    g = t;
                    b = p;
                    break;
                case 1:
                    r = q;
                    g = v;
                    b = p;
                    break;
                case 2:
                    r = p;
                    g = v;
                    b = t;
                    break;
                case 3:
                    r = p;
                    g = q;
                    b = v;
                    break;
                case 4:
                    r = t;
                    g = p;
                    b = v;
                    break;
                default:
                    r = v;
                    g = p;
                    b = q;
                    break;
            }
            dst = (floor(r*255) << 16) + (floor(g*255) << 8) + (floor(b*255));
            return dst;
        }

        private static function RGBToHSV(src:Number):Object
        {
            var dst:Object = {};

            var red:Number =  (src >> 16) / 255;
            var green:Number = (0xFF & (src >> 8)) / 255;
            var blue:Number = (0xFF & src)/ 255;
            var minF:Function = MathUtils.min;
            var maxF:Function = MathUtils.max;

            var min:Number = minF( minF(red, green) , blue );
            var max:Number= maxF( maxF(red, green) , blue );
            dst.v = max;
            var delta:Number = (max - min);
            if( max != 0 )
                dst.s = delta / (max);
            else {
                dst.s = 0;
                dst.h = -1;
                return dst;
            }

            if(delta==0){
                dst.h = -1;
                return dst;
            }

            if( red == max ) {
                dst.h = ( green - blue ) / delta;
            }
            else if( green == max ) {
                dst.h = 2 + ( blue - red ) / delta;
            }
            else {
                dst.h = 4 + ( red - green ) / delta;
            }
            dst.h *= 60;
            if( dst.h < 0 )
                dst.h += 360;


            return dst;

        }
    }
}

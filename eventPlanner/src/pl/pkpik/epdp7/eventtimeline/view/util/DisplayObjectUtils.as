package pl.pkpik.epdp7.eventtimeline.view.util {
    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.geom.ColorTransform;

    import mx.core.UIComponent;

    import pl.pkpik.epdp7.eventtimeline.util.MathUtils;

    public final class DisplayObjectUtils {
        public static function setTint(displayObject:DisplayObject, tintColor:int, tintMultiplier:Number = 1):void
        {
            var colTransform:ColorTransform = new ColorTransform();
            if(tintColor<0){
                colTransform.redMultiplier =
                        colTransform.greenMultiplier =
                                colTransform.blueMultiplier =
                                    colTransform.redOffset =
                                         colTransform.greenOffset =
                                            colTransform.blueOffset = 0
            }else{
                var r:Function = MathUtils.round;
                colTransform.redMultiplier = colTransform.greenMultiplier = colTransform.blueMultiplier = 1-tintMultiplier;
                colTransform.redOffset = r(((tintColor & 0xFF0000) >> 16) * tintMultiplier);
                colTransform.greenOffset = r(((tintColor & 0x00FF00) >> 8) * tintMultiplier);
                colTransform.blueOffset = r(((tintColor & 0x0000FF)) * tintMultiplier);
            }

            displayObject.transform.colorTransform = colTransform;
        }

        public static function removeAllChildren(parent:UIComponent):void
        {
            while(parent.numChildren > 0 ){
                parent.removeChildAt( 0 );
            }
        }


        public static function bringInFront(ui:DisplayObject):void {
            if (ui && ui.parent) {
                var par:DisplayObjectContainer = ui.parent;
                if ((par.numChildren > 1) && (par.getChildIndex(ui) != (par.numChildren - 1))) {
                    par.setChildIndex(ui, par.numChildren - 1);
                }
            }
        }
    }
}

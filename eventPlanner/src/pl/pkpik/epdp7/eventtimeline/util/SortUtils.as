
package pl.pkpik.epdp7.eventtimeline.util {
    import pl.pkpik.epdp7.eventtimeline.view.component.ShiftRectangle;

    public final class SortUtils {
        static public function VectorSortOn( v:Vector.<ShiftRectangle>, prop:String, options:uint=0, copy:Boolean = false ):Vector.<ShiftRectangle>
        {
            var vec:Vector.<ShiftRectangle> = copy ? v.concat() : v;
            function sorty( ox:*, oy:* ):int
            {
                var px:String = String(ox[prop]);
                var py:String = String(oy[prop]);

                if( options & 1 )
                {
                    px = px.toLowerCase();
                    py = py.toLowerCase();
                }
                var rtn:Number = ( options & 16 ) ?  Number(px) - Number(py) : (px < py) ? -1 : (px > py) ? 1 : 0;
                return ( options & 2 ) ? -rtn : rtn;
            }

            return vec.sort(sorty);
        }
    }
}

package pl.pkpik.epdp7.eventtimeline.util {

    import flash.display.DisplayObject;
    import flash.display.Stage;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;

    import mx.core.FlexGlobals;
    import mx.managers.SystemManager;

    public final class KeyboardProxy {


        private var _isCtrl:Boolean;
        private var _isShift:Boolean;
        private var _isEnter:Boolean;

        private static const INSTANCE:KeyboardProxy = new KeyboardProxy();


        public static function GetInstance():KeyboardProxy
        {
            return INSTANCE;
        }


        private function initializeProxy():void
        {
            removeKeyboardEvents();
            addKeyboardEvents();
        }

        private function addKeyboardEvents():void
        {
            component.addEventListener( KeyboardEvent.KEY_DOWN, keyDownHandler );
            component.addEventListener( KeyboardEvent.KEY_UP, keyUpHandler );
        }

        protected function keyDownHandler( event:KeyboardEvent ):void
        {
            switch (event.keyCode){
                case Keyboard.CONTROL:
                        this._isCtrl = true;
                    break;
                case Keyboard.SHIFT:
                        this._isShift = true;
                    break;
                case Keyboard.ENTER:
                        this._isEnter = true;
                    break;
            }
        }

        protected function keyUpHandler( event:KeyboardEvent ):void
        {
            switch (event.keyCode){
                case Keyboard.CONTROL:
                    this._isCtrl = false;
                    break;
                case Keyboard.SHIFT:
                    this._isShift = false;
                    break;
                case Keyboard.ENTER:
                    this._isEnter = false;
                    break;
            }
        }

        private function removeKeyboardEvents() :void
        {
            if(component && component.hasEventListener(KeyboardEvent.KEY_DOWN)){
                component.removeEventListener( KeyboardEvent.KEY_DOWN, keyDownHandler);
                component.removeEventListener( KeyboardEvent.KEY_UP, keyUpHandler);
            }

        }

        public function KeyboardProxy() {
            initializeProxy();
        }

        private function get component():Stage {
            return SystemManager.getSWFRoot(FlexGlobals.topLevelApplication as DisplayObject).stage;
        }

        public function get isCtrl():Boolean {
            return _isCtrl;
        }

        public function get isShift():Boolean {
            return _isShift;
        }

        public function get isEnter():Boolean {
            return _isEnter;
        }
    }
}

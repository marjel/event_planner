package pl.pkpik.epdp7.eventtimeline.util
{
	import flash.events.TimerEvent;
	import mx.core.IChildList;
	import pl.pkpik.epdp7.eventtimeline.view.component.CustomToolTip;
	
	import flash.display.DisplayObject;
	import flash.geom.Point;
	
	import mx.controls.ToolTip;
	import mx.core.FlexGlobals;
	import mx.core.IFlexModule;
	import mx.core.IToolTip;
	import mx.core.IUIComponent;
	import mx.core.UIComponent;
	import mx.core.mx_internal;
	import mx.events.DynamicEvent;
	import mx.events.EffectEvent;
	import mx.managers.ISystemManager;
	import mx.managers.IToolTipManager2;
	import mx.managers.ToolTipManagerImpl;
	import mx.resources.ResourceManager;
	import mx.styles.IStyleClient;
	
	use namespace mx_internal;
	

	public final class CustomToolTipManagerImpl extends ToolTipManagerImpl implements IToolTipManager2
	{
		public static var DEFAULT_TOOL_TIP_BUNDLE_NAME:String = "ToolTipBundle";
		
		private var _useDefaultImplementation:Boolean;
		
		private static var _instance:IToolTipManager2;
		
		public function CustomToolTipManagerImpl()
		{
			super();
			if (_instance)
				throw new Error("Instance already exists.");
		}
		
		
		public function removeToolTips():void
        {
            var sm:ISystemManager = FlexGlobals.topLevelApplication.systemManager as ISystemManager;
            var childList:IChildList = sm.topLevelSystemManager.toolTipChildren;
            var len:int = childList.numChildren;
            currentToolTip = null;
            for(var i:uint=0;i<len;i++){
                childList.removeChildAt(i);
            }

        }
		override public function createToolTip(text:String, x:Number, y:Number,
									  errorTipBorderStyle:String = null,
									  context:IUIComponent = null):IToolTip
		{
			var toolTip:CustomToolTip = new CustomToolTip();
			var sm:ISystemManager = context ?
				context.systemManager as ISystemManager:
				FlexGlobals.topLevelApplication.systemManager as ISystemManager;
			
			if (context is IFlexModule)
				toolTip.moduleFactory = IFlexModule(context).moduleFactory;
			else
				toolTip.moduleFactory = sm;
			
			var e:DynamicEvent;
			if (hasEventListener("addChild"))
			{
				e = new DynamicEvent("addChild", false, true);
				e.sm = sm;
				e.toolTip = toolTip;
			}
			if (!e || dispatchEvent(e))
			{
				sm.topLevelSystemManager.toolTipChildren.addChild(toolTip as DisplayObject);
			}
			
			if (errorTipBorderStyle)
			{
				toolTip.setStyle("styleName", "errorTip");
				toolTip.setStyle("borderStyle", errorTipBorderStyle);
			}
			
			toolTip.text = text;
			
			sizeTip(toolTip);
			
			toolTip.move(x, y);
			return toolTip as IToolTip;
		}
		
		public function get useDefaultImplementation():Boolean
		{
			return _useDefaultImplementation;
		}

		public function set useDefaultImplementation(value:Boolean):void
		{
			if (value) {
				toolTipClass = ToolTip;
			} else {
				toolTipClass = CustomToolTip;
			}
			_useDefaultImplementation = value;
			
		}

		public static function getInstance():IToolTipManager2
		{
			if (!_instance)
				_instance = new CustomToolTipManagerImpl();
			_instance.toolTipClass = CustomToolTip;
			return _instance;
		}

        override mx_internal function showTimer_timerHandler(event:TimerEvent):void
        {
            if (enabled)
            {
                super.showTimer_timerHandler(event);
            }
        }
		override mx_internal function initializeTip():void {
			
			if (useDefaultImplementation) {
				super.initializeTip();
				return;
			}
			
			if (isError && currentToolTip is IStyleClient)
				IStyleClient(currentToolTip).setStyle("styleName", "errorTip");
			
			if (currentToolTip is IToolTip)
				assignResourcesToToolTip();
			
			sizeTip(currentToolTip);
			
			if (currentToolTip is IStyleClient)
			{
				if (showEffect)
					IStyleClient(currentToolTip).setStyle("showEffect", showEffect);
				if (hideEffect)
					IStyleClient(currentToolTip).setStyle("hideEffect", hideEffect);
			}
			
			if (showEffect || hideEffect)
			{
				currentToolTip.addEventListener(EffectEvent.EFFECT_END,
					effectEndHandler);
			}
		}
		
		override mx_internal function positionTip():void {
			
			if (useDefaultImplementation) {
				super.positionTip();
				return;
			}
			
			var x:Number;
			var y:Number;
			
			var screenWidth:Number = currentToolTip.screen.width;
			var screenHeight:Number = currentToolTip.screen.height;
			
			var targetPos:Point = currentTarget.localToGlobal(new Point(0, 0));
			var targetWidth:Number = currentTarget.width;
			var targetHeight:Number = currentTarget.height;
			var toolTipWidth:Number = currentToolTip.width;
			var toolTipHeight:Number = currentToolTip.height;
			var toLeft:Boolean;
			var toTop:Boolean;
			
			var sm:ISystemManager = getSystemManager(currentTarget);
			
			var ttPos:String = IStyleClient(currentToolTip).getStyle("placement");
			
			if (ttPos == "topLeft" || 
				ttPos == "bottomLeft" || 
				targetPos.x + targetWidth * .75 + toolTipWidth > screenWidth) {
				toLeft = true;
			}
			
			if (ttPos == "topLeft" ||
				ttPos == "topRight" ||
				targetPos.y + targetHeight/2 + toolTipWidth > screenHeight) {
				toTop = true;
			}
			if (toLeft && toTop) {
				IStyleClient(currentToolTip).setStyle("placement", "topLeft");
				x = targetPos.x + MathUtils.max (20, targetWidth * .1 - 25);
				y = targetPos.y + targetHeight * .25;
			} 
			else if (toLeft && !toTop)
			{
				IStyleClient(currentToolTip).setStyle("placement", "bottomLeft");
				x = targetPos.x + MathUtils.max (20, targetWidth * .1 - 25);
				y = targetPos.y + targetHeight * .75;
			} 
			else if (!toLeft && toTop) {
				IStyleClient(currentToolTip).setStyle("placement", "topRight");
				x = targetPos.x + targetWidth * .9 - 25;
				y = targetPos.y + targetHeight * .25;
			} 
			else 
			{
				IStyleClient(currentToolTip).setStyle("placement", "bottomRight");
				x = targetPos.x + targetWidth * .9 - 25;
				y = targetPos.y + targetHeight * .75;
			}
			
			var pos:Point = new Point(x, y);
			pos = DisplayObject(sm).localToGlobal(pos);
			pos = DisplayObject(sm.getSandboxRoot()).globalToLocal(pos);
			x = pos.x;
			y = pos.y;
			
			currentToolTip.move(x, y);
		}
		

		protected function assignResourcesToToolTip():void {
			
			if (!currentText || !currentToolTip)
				return;
			
			var toolTipKey:String = "tooltip";
			if (isError) {
				toolTipKey = "errortip";
			}
			var tokens:Array = currentText.split(".");
			
			var locale:String = ResourceManager.getInstance().localeChain[0];
			var bundleKey:String =  DEFAULT_TOOL_TIP_BUNDLE_NAME;
			
			if (ResourceManager.getInstance().getResourceBundle(locale, tokens[0]) != null) {
				bundleKey = tokens[0];
			}

			var targetKey:String = "default";
			
			if (tokens.length > 1)
			{
				targetKey = tokens[1];
			} 
			else if (this.currentTarget is UIComponent && UIComponent(currentTarget).id != null) {
				targetKey = UIComponent (currentTarget).id;
			}
			
			var lookupKey:String = toolTipKey + "." + targetKey;

			var color:uint = ResourceManager.getInstance().getUint(bundleKey, lookupKey + ".color");
			if (color) 
			{
				IStyleClient(currentToolTip).setStyle("color", color);
			}

			var chromeColor:uint = ResourceManager.getInstance().getUint(bundleKey, lookupKey + ".chromeColor");
			if (chromeColor)
			{
				IStyleClient(currentToolTip).setStyle("chromeColor", chromeColor);
			}

			var styleName:String = ResourceManager.getInstance().getString(bundleKey, lookupKey + ".styleName");
			if (styleName)
			{
				IStyleClient(currentToolTip).styleName = styleName;
			}

			var placement:String = ResourceManager.getInstance().getString(bundleKey, lookupKey + ".placement");
			if (placement)
			{
				IStyleClient(currentToolTip).setStyle("placement", placement);
			}

			var toolTipText:String = ResourceManager.getInstance().getString(bundleKey, lookupKey + ".text");
			if (toolTipText) 
			{
				currentToolTip.text = toolTipText;
			} else 
			{
				currentToolTip.text = currentText;
			}
			if (currentToolTip is CustomToolTip) {
				var toolTipTitle:String = ResourceManager.getInstance().getString(bundleKey, lookupKey + ".title");
				if (toolTipTitle != null && toolTipTitle.length > 0) {
					CustomToolTip(currentToolTip).title = toolTipTitle;
				}
			}
		}
	}
}
/**
 * Created by mariel on 04.03.15.
 */
package pl.pkpik.epdp7.eventtimeline.util {

    public final class MathUtils {

        public static function max(x:Number, y:Number):Number
        {
            return (x > y) ? x : y;
        }
        public static function min(x:Number, y:Number):Number
        {
           return (x < y) ? x : y;
        }
        public static function floor(n:Number):int
        {
            var ni:int = n;
            return (n < 0 && n != ni) ? ni - 1 : ni;
        }
        public static function round(n:Number):int
        {
            return n < 0 ? n + .5 == (n | 0) ? n : n - .5 : n + .5;
        }
    }
}

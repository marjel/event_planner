package pl.pkpik.epdp7.eventtimeline.util
{
	public final class DateUtils
	{

		public static function addZero(num:Number):String
		{
			var newString:String = String(num);
			if(newString.length <= 1){
				newString = "0" + num;
			}
			
			return newString;
		}

		public static function removeZero(dateNumber:String):Number
		{
			var newNumber:Number = (dateNumber.substr(0,1) == "0") ? Number(dateNumber.substr(1,1)) : Number(dateNumber);
			return newNumber;
		}

		public static function convertToLittleEndian(dateString:String):String
		{
			return dateString.split("-").reverse().join("-");
		}

		

		public static function getFullDateFromDateString(dateString:String, delimeter:String = " "):Date
		{
			if(!dateString){
				return null;
			}
			var splitedString:Array = dateString.split(delimeter);
			var dateStr:String = splitedString[0];
			var timeStr:String = splitedString[1]; 
			var date:Date = new Date();
			date.fullYear = Number(dateStr.substr(0,4));
			date.month = Number(removeZero(dateStr.substr(5,2))) - 1;
			date.date = Number(removeZero(dateStr.substr(8,2)));
			
			date.hours = Number(removeZero(timeStr.substr(0,2)));
			date.minutes = Number(removeZero(timeStr.substr(3,2)));
			date.seconds = 0;
			
			return date;
		}


		public static function getMinutesShiftFromDateString(dateString:String):Number
		{
			var timeStr:Array = (dateString.split("+")[1] as String).split(":");
			return ((removeZero(timeStr[0])-1)*60) + removeZero(timeStr[1]);
		}
		
		public static function getTimeOffset(dateString:String, delimeter:String = "."):int
		{
			var splitedString:Array = dateString.split(delimeter);
			var dateStr:String = splitedString[1];

			var minutes:int = int(removeZero(dateStr.substr(4,2)))*60;
			minutes+=int(removeZero(dateStr.substr(7,2)));
			
			return (dateStr.substr(3,1)=="-") ? -minutes : minutes;
		}
		public static function getTimeString(value:Date = null):String
		{
			var date:Date = value || new Date();
			
			var timeString:String = DateUtils.addZero(date.hours) + ":" + DateUtils.addZero(date.minutes);
			
			return timeString;
		}
		
		public static function getShortStyleDay(index:Number):String
		{
			var day:String = "";
			
			switch (String(index))
			{
				case '1':
					day = "Pn";
					break;
				case '2':
					day = "Wt";
					break;
				case '3':
					day = "Śr";
					break;
				case '4':
					day = "Cz";
					break;
				case '5':
					day = "Pt";
					break;
				case '6':
					day = "So";
					break;
				case '0':
					day = "Nd";
					break;
				default:
					day = "Day Error";
					break;
			}
			return day;
		}

        public static function getDay(index:Number):String
        {
            var day:String = "";

            switch (String(index))
            {
                case '1':
                    day = "Poniedziałek";
                    break;
                case '2':
                    day = "Wtorek";
                    break;
                case '3':
                    day = "Środa";
                    break;
                case '4':
                    day = "Czwartek";
                    break;
                case '5':
                    day = "Piątek";
                    break;
                case '6':
                    day = "Sobota";
                    break;
                case '0':
                    day = "Niedziela";
                    break;
                default:
                    day = "Day Error";
                    break;
            }
            return day;
        }

        public static function getLatinStyleMonth(index:Number):String
        {
            var month:String = "";

            switch (String(index))
            {
                case '0':
                    month = "I";
                    break;
                case '1':
                    month = "II";
                    break;
                case '2':
                    month = "III";
                    break;
                case '3':
                    month = "IV";
                    break;
                case '4':
                    month = "V";
                    break;
                case '5':
                    month = "VI";
                    break;
                case '6':
                    month = "VII";
                    break;
                case '7':
                    month = "VIII";
                    break;
                case '8':
                    month = "IX";
                    break;
                case '9':
                    month = "X";
                    break;
                case '10':
                    month = "XI";
                    break;
                case '11':
                    month = "XII";
                    break;
                default:
                    month = "Month Error";
                    break;
            }
            return month;
        }

		public static function getMonth(index:Number):String
		{
			var month:String = "";
			
			switch (String(index))
			{
				case '0':
					month = "Styczeń";
					break;
				case '1':
					month = "Luty";
					break;
				case '2':
					month = "Marzec";
					break;
				case '3':
					month = "Kwiecień";
					break;
				case '4':
					month = "Maj";
					break;
				case '5':
					month = "Czerwiec";
					break;
				case '6':
					month = "Lipiec";
					break;
				case '7':
					month = "Sierpień";
					break;
				case '8':
					month = "Wrzesień";
					break;
				case '9':
					month = "Październik";
					break;
				case '10':
					month = "Listopad";
					break;
				case '11':
					month = "Grudzień";
					break;
				default:
					month = "Month Error";
					break;
			}
			return month;
		}
	}
}
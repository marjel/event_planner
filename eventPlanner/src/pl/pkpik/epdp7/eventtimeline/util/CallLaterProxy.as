package pl.pkpik.epdp7.eventtimeline.util {
    import flash.display.Shape;
    import flash.events.Event;

    import org.apache.flex.collections.VectorCollection;

    import org.puremvc.as3.patterns.observer.Notifier;

    import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;

    import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
    import pl.pkpik.epdp7.eventtimeline.service.DataService;

    public final class CallLaterProxy extends Notifier {


        private static const EFD:Shape = new Shape();
        private const _actionsQueue:Vector.<ActionData> = new Vector.<ActionData>();
        private static const INSTANCE:CallLaterProxy = new CallLaterProxy();
        private var _currentAction:ActionData;
        private var _serviceActions:VectorCollection;

        private function get inProgress():Boolean
        {
            return EFD.hasEventListener(Event.ENTER_FRAME);
        }

        private function set inProgress(value:Boolean):void
        {
            if(value != inProgress){
                if(value){
                    EFD.addEventListener(Event.ENTER_FRAME, efh);
                    sendNotification(ApplicationCommands.ACTIONS_START_EXECUTE, "New Event Planner");
                }else{
                    //if(_serviceActions.length<1){
                        EFD.removeEventListener(Event.ENTER_FRAME, efh);
                        sendNotification(ApplicationCommands.ACTIONS_STOP_EXECUTE);
                    //}
                }
            }
        }

        public static function GetInstance():CallLaterProxy
        {
            INSTANCE._serviceActions = DataService.getInstance().actions;
            return INSTANCE;
        }

        public function _callFunctionLater(ac:*):void {
            var action:ActionData;
            if(ac is ActionData){
                action = ac as ActionData
            }else{
                var a:*;
                for each(a in ac){
                    _callFunctionLater(a);
                }
                return;
            }
            if(action.counter==0){
                executeAction(action);
                return;
            }

            action.index = -1;
            _actionsQueue.push(action);
            inProgress = true;
        }

        public function removeAllActions():void {
            _currentAction = null;
            inProgress = false;
            if(_actionsQueue.length){
                _actionsQueue.splice(0, _actionsQueue.length);
            }

        }

        private function executeAction(data:ActionData):void
        {

            _currentAction = null;
            data.isWaiting = false;
            data.action.apply(data.scope, data.arguments);
            sendNotification(ApplicationCommands.ACTION_EXECUTED, data.notyficationBody);
            data.index = -1;
        }

        private function efh(event:Event):void
        {
            if(_currentAction==null){
                _currentAction = _actionsQueue.shift();

                if(_currentAction){
                    _currentAction.isWaiting = true;
                    _currentAction.index = _actionsQueue.length;
                }

                return;
            }
            if(_currentAction.isWaiting){
                if((_currentAction.counter--)==0) {
                    executeAction(_currentAction);
                    if(_actionsQueue.length==0){
                        inProgress = false;
                    }
                }
            }
        }

        public function CallLaterProxy() {

            EFD.visible = false;
        }
    }
}

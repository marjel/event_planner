package pl.pkpik.epdp7.eventtimeline.util
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import mx.core.DragSource;
	import mx.core.IFlexDisplayObject;
	import mx.core.IFlexModule;
	import mx.core.IFlexModuleFactory;
	import mx.core.ILayoutDirectionElement;
	import mx.core.IUIComponent;
	import mx.core.LayoutDirection;
	import mx.core.UIComponentGlobals;
	import mx.core.mx_internal;
	import mx.events.DragEvent;
	import mx.events.Request;
	import mx.managers.DragManager;
	import mx.managers.IDragManager;
	import mx.managers.ILayoutManagerClient;
	import mx.managers.ISystemManager;
	import mx.managers.SystemManagerGlobals;
	import mx.styles.CSSStyleDeclaration;
	import mx.styles.IStyleManager2;
	import mx.styles.StyleManager;
	import mx.utils.MatrixUtil;
	
	import pl.pkpik.epdp7.eventtimeline.view.component.XYAxisDragProxy;
	
	use namespace mx_internal;
	
	public class XYAxisDragManagerImpl extends EventDispatcher implements IDragManager
	{

		private static var sm:ISystemManager;

		private static var instance:IDragManager;

		public static var mixins:Array;
		

		public static function getInstance():IDragManager
		{
			if (!instance)
			{
				sm = SystemManagerGlobals.topLevelSystemManagers[0];
				instance = new XYAxisDragManagerImpl();
				
			}
			
			return instance;
		}

		public function XYAxisDragManagerImpl()
		{
			super();
			
			if (instance)
				throw new Error("Instance already exists.");
			
			if (mixins)
			{
				var n:int = mixins.length;
				for (var i:int = 0; i < n; i++)
				{
					new mixins[i](this);
				}
			}
			
			sandboxRoot = sm.getSandboxRoot();
			
			if (sm.isTopLevelRoot())
			{
				sm.addEventListener(MouseEvent.MOUSE_DOWN, sm_mouseDownHandler, false, 0, true);
				sm.addEventListener(MouseEvent.MOUSE_UP, sm_mouseUpHandler, false, 0, true);
			}
			
			if (hasEventListener("initialize"))
				dispatchEvent(new Event("initialize"));
			
		}

		private var sandboxRoot:IEventDispatcher;
		

		private var dragInitiator:IUIComponent;
	
		public var dragProxy:XYAxisDragProxy;

		public var bDoingDrag:Boolean = false;

		private var mouseIsDown:Boolean = false;

		public function get isDragging():Boolean
		{
			return bDoingDrag;
		}

		public function doDrag(
			dragInitiator:IUIComponent, 
			dragSource:DragSource, 
			mouseEvent:MouseEvent,
			dragImage:IFlexDisplayObject = null, 
			xOffset:Number = 0,
			yOffset:Number = 0,
			imageAlpha:Number = 0.5,
			allowMove:Boolean = true):void
		{
			var proxyWidth:Number;
			var proxyHeight:Number;
			
			if (bDoingDrag)
				return;
			
			if (!(mouseEvent.type == MouseEvent.MOUSE_DOWN ||
				mouseEvent.type == MouseEvent.CLICK ||
				mouseIsDown ||
				mouseEvent.buttonDown))
			{
				return;
			}    
			
			bDoingDrag = true;
			
			if (hasEventListener("doDrag"))
				dispatchEvent(new Event("doDrag"));
			
			this.dragInitiator = dragInitiator;

			dragProxy = new XYAxisDragProxy(dragInitiator, dragSource, xOffset<Number.POSITIVE_INFINITY, yOffset<Number.POSITIVE_INFINITY);
			
			if(xOffset==Number.POSITIVE_INFINITY){
				xOffset=0;
			}
			
			if(yOffset==Number.POSITIVE_INFINITY){
				yOffset=0;
			}
			
			var e:Event; 
			if (hasEventListener("popUpChildren"))
				e = new DragEvent("popUpChildren", false, true, dragProxy);
			if (!e || dispatchEvent(e))	
				sm.popUpChildren.addChild(dragProxy);	
			
			if (!dragImage)
			{
				var dragManagerStyleDeclaration:CSSStyleDeclaration =
					getStyleManager(dragInitiator).getMergedStyleDeclaration("mx.managers.DragManager");
				var dragImageClass:Class =
					dragManagerStyleDeclaration.getStyle("defaultDragImageSkin");
				dragImage = new dragImageClass();
				dragProxy.addChild(DisplayObject(dragImage));
				proxyWidth = dragInitiator.width;
				proxyHeight = dragInitiator.height;
			}
			else
			{
				dragProxy.addChild(DisplayObject(dragImage));
				if (dragImage is ILayoutManagerClient )
					UIComponentGlobals.layoutManager.validateClient(ILayoutManagerClient (dragImage), true);
				if (dragImage is IUIComponent)
				{
					proxyWidth = (dragImage as IUIComponent).getExplicitOrMeasuredWidth();
					proxyHeight = (dragImage as IUIComponent).getExplicitOrMeasuredHeight();
				}
				else
				{
					proxyWidth = dragImage.measuredWidth;
					proxyHeight = dragImage.measuredHeight;
				}
			}

			if (dragInitiator is ILayoutDirectionElement &&
				ILayoutDirectionElement(dragInitiator).layoutDirection == LayoutDirection.RTL)
				dragProxy.layoutDirection = LayoutDirection.RTL;
			
			dragImage.setActualSize(proxyWidth, proxyHeight);
			dragProxy.setActualSize(proxyWidth, proxyHeight);

			dragProxy.alpha = imageAlpha;
			
			dragProxy.allowMove = allowMove;
			
			var concatenatedMatrix:Matrix = 
				MatrixUtil.getConcatenatedMatrix(DisplayObject(dragInitiator), 
					DisplayObject(sandboxRoot));
      
			concatenatedMatrix.tx = 0;
			concatenatedMatrix.ty = 0;

			var m:Matrix = dragImage.transform.matrix;
			if (m)
			{
				concatenatedMatrix.concat(dragImage.transform.matrix);
				dragImage.transform.matrix = concatenatedMatrix;
			}

			var nonNullTarget:Object = mouseEvent.target;
			if (nonNullTarget == null)
				nonNullTarget = dragInitiator;
			
			var point:Point = new Point(mouseEvent.localX, mouseEvent.localY);
			point = DisplayObject(nonNullTarget).localToGlobal(point);
			point = DisplayObject(sandboxRoot).globalToLocal(point);
			var mouseX:Number = point.x;
			var mouseY:Number = point.y;

			var proxyOrigin:Point = DisplayObject(dragInitiator).localToGlobal(new Point(-xOffset, -yOffset));
			proxyOrigin = DisplayObject(sandboxRoot).globalToLocal(proxyOrigin);

			dragProxy.xOffset = mouseX - proxyOrigin.x;
			dragProxy.yOffset = mouseY - proxyOrigin.y;

			dragProxy.x = proxyOrigin.x;
			dragProxy.y = proxyOrigin.y;

			dragProxy.startX = dragProxy.x;
			dragProxy.startY = dragProxy.y;

			if (dragImage is DisplayObject) 
				DisplayObject(dragImage).cacheAsBitmap = true;
			
			
			var delegate:Object = dragProxy.automationDelegate;
			if (delegate)
				delegate.recordAutomatableDragStart(dragInitiator, mouseEvent);
		}

		public function acceptDragDrop(target:IUIComponent):void
		{

			if (dragProxy)
				dragProxy.target = target as DisplayObject;
			
			if (hasEventListener("acceptDragDrop"))
				dispatchEvent(new Request("acceptDragDrop", false, false, target));
			
		}

		public function showFeedback(feedback:String):void
		{

			if (dragProxy)
			{
				if (feedback == DragManager.MOVE && !dragProxy.allowMove)
					feedback = DragManager.COPY;
				
				dragProxy.action = feedback;
			}
			
			if (hasEventListener("showFeedback"))
				dispatchEvent(new Request("showFeedback", false, false, feedback));
			
		}
		

		public function getFeedback():String
		{
			if (hasEventListener("getFeedback"))
			{
				var request:Request = new Request("getFeedback", false, true);
				if (!dispatchEvent(request))
				{
					return request.value as String;
				}
			}

			return dragProxy ? dragProxy.action : DragManager.NONE;
		}

		public function endDrag():void
		{
			var e:Event;
			if (hasEventListener("endDrag"))
			{
				e = new Event("endDrag", false, true);
			}
			
			if (!e || dispatchEvent(e))
			{
				if (dragProxy)
				{
					sm.popUpChildren.removeChild(dragProxy);	
					
					if (dragProxy.numChildren > 0)
						dragProxy.removeChildAt(0);	
					dragProxy = null;
				}
			}
			
			dragInitiator = null;
			bDoingDrag = false;
			
		}

		static private function getStyleManager(dragInitiator:IUIComponent):IStyleManager2
		{
			if (dragInitiator is IFlexModule)
				return StyleManager.getStyleManager(IFlexModule(dragInitiator).moduleFactory);
			
			return StyleManager.getStyleManager(sm as IFlexModuleFactory);
		}

		private function sm_mouseDownHandler(event:MouseEvent):void
		{
			mouseIsDown = true;
		}

		private function sm_mouseUpHandler(event:MouseEvent):void
		{
			mouseIsDown = false;
		}
		
		
	}
}
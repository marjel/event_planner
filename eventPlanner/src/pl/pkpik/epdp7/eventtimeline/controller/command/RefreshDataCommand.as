package pl.pkpik.epdp7.eventtimeline.controller.command
{

    import flash.system.System;
    import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
    import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.EventsDataProxy;
    import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftAssignmentVO;
	
	public class RefreshDataCommand extends SimpleCommand
	{
		override public function execute( note:INotification ) :void
		{

            var edp:EventsDataProxy = facade.retrieveProxy(EventsDataProxy.NAME) as EventsDataProxy;
			var xml:XML = note.getBody() as XML;
			var list:XMLList = xml..ShortShiftAssignment as XMLList;
            var shiftAssVO:ShiftAssignmentVO;
            var xmlNode:XML;
            var shiftAssignments:VectorCollection = edp.eventsData.shiftAssignments;
            var newAssignments:Vector.<ShiftAssignmentVO> = new <ShiftAssignmentVO>[];
			for each(xmlNode in list){
                shiftAssVO = edp.getShiftAssignmentByID(int(xmlNode.shiftAssignmentId));
                if(shiftAssVO==null){
                    shiftAssVO = new ShiftAssignmentVO();
                    shiftAssVO.id = xmlNode.shiftAssignmentId;
                    shiftAssignments.addItem(shiftAssVO);
                }
                if(xmlNode.isShiftNight != undefined){
                    shiftAssVO.isShiftNight = (xmlNode.isShiftNight == "true") ? true : false;
                }
                if(xmlNode.shiftId != undefined){
                    shiftAssVO.shiftReference = int(xmlNode.shiftId);
                }
                if(xmlNode.extMinuteAfter != undefined){
                    shiftAssVO.extMinuteAfter = int(xmlNode.extMinuteAfter);
                }
                if(xmlNode.extMinuteBefore != undefined){
                    shiftAssVO.extMinuteBefore = int(xmlNode.extMinuteBefore);
                }
                shiftAssVO.employeeReference = (xmlNode.employeeId != undefined) ? int(xmlNode.employeeId) : Number.MIN_VALUE;

                if(xmlNode.isShiftBlocked != undefined){
                    shiftAssVO.isShiftBlocked =  (xmlNode.isShiftBlocked == "true") ? true : false;
                }
                newAssignments.push(shiftAssVO);

			}
            //sendNotification(ApplicationCommands.DATA_REFRESHED, newAssignments)
            callFunctionLater(new ActionData(sendNotification, this, [ApplicationCommands.DATA_REFRESHED, newAssignments], 1, -1, "Start Data Refreshing..."));
            System.disposeXML(xml);
		}
	}
}
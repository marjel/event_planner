package pl.pkpik.epdp7.eventtimeline.controller.command
{

    import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
    import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
    import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
    import pl.pkpik.epdp7.eventtimeline.controller.vo.QueueData;
    import pl.pkpik.epdp7.eventtimeline.service.DataService;

    public final class UIRefreshedCommand extends SimpleCommand
	{

		override public function execute( note:INotification ) :void
		{
            executeAction(DataService.getInstance());
		}

        private function executeAction(ds:DataService):void
        {
            var actions:VectorCollection = ds.actions;
            var l:int = actions.length;
            if(l){
                var a:QueueData = actions.getItemAt(0) as QueueData;
                if(ds.current==null){
                    ds.current = a;
                }
                if(ds.current==a){
                    ds.current.action.call(ds.current.scope, ds.current.data, true);
                    actions.removeItem(ds.current);
                    ds.current = null;
                }else{
                    ds.current.action.call(ds.current.scope, ds.current.data, false);
                }
            }else if(ds.current!=null){
                ds.current.action.call(ds.current.scope, ds.current.data, true);
                actions.removeItem(ds.current);
                ds.current = null;
                callFunctionLater(new ActionData(sendNotification, this, [ApplicationCommands.UI_REFRESHED],
                        1, -1, "<font face='DinBold' color='#000099'>" + "next" + "</font>"));
            }else{
                sendNotification(ApplicationCommands.QUEUE_EXECUTED);
            }
        }
	}
}
package pl.pkpik.epdp7.eventtimeline.controller.command
{
	import org.puremvc.as3.patterns.command.MacroCommand;
	
	public class ApplicationStartupCommand extends MacroCommand
	{
		override protected function initializeMacroCommand() :void
		{
			addSubCommand( ModelPrepareCommand );
			addSubCommand( ViewPrepareCommand );

		}
	}
}
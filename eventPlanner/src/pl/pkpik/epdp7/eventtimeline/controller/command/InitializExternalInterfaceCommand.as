package pl.pkpik.epdp7.eventtimeline.controller.command
{
	import flash.external.ExternalInterface;
	
	import mx.core.FlexGlobals;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
	import pl.pkpik.epdp7.eventtimeline.service.DataService;
	import pl.pkpik.epdp7.eventtimeline.view.mediator.EventTimelineMediator;

	import spark.components.Application;
	
	public class InitializExternalInterfaceCommand extends SimpleCommand
	{
		override public function execute( note:INotification ) :void    
		{
			addCallbacks();

		}
		
		public function enableApplication(value:Boolean):void
		{
			sendNotification(ApplicationCommands.ENABLE_APPLICATION, value);
		}

        public function sendHideBrokenRulesNotification():void
        {
            sendNotification(ApplicationCommands.HIDE_BROKEN_RULES);
        }
        public function sendNavigateToBrokenRuleNotification(id:int):void
        {

            sendNotification(ApplicationCommands.NAVIGATE_TO_BROKEN_RULE, id);
        }
        public function sendsSrollToDateNotification(date:Object):void
        {
            sendNotification(ApplicationCommands.SCROLL_TO_DATE, date);
        }
        public function sendScrollToEmployeeNotification(emploeeId:int):void
        {
            sendNotification(ApplicationCommands.SCROLL_TO_EMPLOYEE, emploeeId);
        }
        public function sendScrollToShiftNotification(shiftId:int):void
        {
            sendNotification(ApplicationCommands.SCROLL_TO_SHIFT, shiftId);
        }



		private function addCallbacks():void {
			var _pid:String = Application(FlexGlobals.topLevelApplication).parameters["pid"];
			var mainMediator:EventTimelineMediator = facade.retrieveMediator(EventTimelineMediator.NAME) as EventTimelineMediator;
			if (_pid) {
				if (ExternalInterface.available) {
					var dataService: DataService = DataService.getInstance();
					ExternalInterface.addCallback("setMeasurement", dataService.setMeasurement);
					ExternalInterface.addCallback("refreshData", dataService.refreshData);
					ExternalInterface.addCallback("refreshFreeDays", dataService.refreshFreeDays);
					ExternalInterface.addCallback("updatePartialDayOff", dataService.updatePartialDayOff);
					ExternalInterface.addCallback("setData", dataService.setData);
					ExternalInterface.addCallback("setAdditionalData", dataService.setAdditionalData);
					ExternalInterface.addCallback("setBrokenRules", dataService.setBrokenRules);
					ExternalInterface.addCallback("enableApplication", enableApplication);
                    ExternalInterface.addCallback("scrollToShift", sendScrollToShiftNotification);
                    ExternalInterface.addCallback("navigateToBrokenRule", sendNavigateToBrokenRuleNotification);
                    ExternalInterface.addCallback("hideBrokenRules", sendHideBrokenRulesNotification);
                    ExternalInterface.addCallback("scrollToEmployee", sendScrollToEmployeeNotification);
                    ExternalInterface.addCallback("scrollToDate", sendsSrollToDateNotification);
                    ExternalInterface.addCallback("changeEmployeeBlocked", mainMediator.changeEmployeeBlocked);
                    ExternalInterface.addCallback("changeDateBlocked", mainMediator.changeDateBlocked);
					if(_pid == "X"){
						ExternalInterface.call("initializeFlash");
					}else{
						ExternalInterface.call("vaadin.workersSchedule['setFlashInitialized_" + _pid + "']");
					}
				} else {
					sendNotification( ApplicationCommands.EVENTS_DATA_LOAD);
					//Alert.show("Zewnętrzny interfejs niedostępny (ExternalInterface unavailable).", "Błąd");
				}
				
			} else {
				sendNotification( ApplicationCommands.EVENTS_DATA_LOAD);
				//Alert.show("Nie przekazano parametru PID", "Błąd");
			}
		}
	}
}
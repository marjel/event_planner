package pl.pkpik.epdp7.eventtimeline.controller.command
{

    import flash.system.System;

    import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
    import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
    import pl.pkpik.epdp7.eventtimeline.model.factory.DayOffVOFactory;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.EventsDataProxy;
    import pl.pkpik.epdp7.eventtimeline.model.vo.DayOffVO;
    import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;
	
	public class RefreshOffDaysCommand extends SimpleCommand
	{

        private var employeesCollectionToUpdate:Vector.<EmployeeVO>;

		override public function execute( note:INotification ) :void
		{
			
			var edp:EventsDataProxy = facade.retrieveProxy(EventsDataProxy.NAME) as EventsDataProxy;

			var xml:XML = XML(note.getBody());
			var list:XMLList = xml..EmployeeIdHolderForDayOff as XMLList;
			var xmlNode:XML;
			var dayOffList:XMLList;
			var dayOffNode:XML;
			var dayOffVO:DayOffVO;
            var dayOffCollection:VectorCollection;
            var employeeVO:EmployeeVO;

            employeesCollectionToUpdate = new Vector.<EmployeeVO>();

			for each(xmlNode in list){
                employeeVO = edp.getEmployeeVOById(xmlNode.id);
                if(employeeVO==null){
                    continue;
                }
                dayOffCollection = employeeVO.daysOff;
                dayOffList = xmlNode..DayOff as XMLList;
                dayOffVO = null;
				for each(dayOffNode in dayOffList){
                    dayOffVO = edp.getDayOffVOById(dayOffNode.id);
					if(dayOffVO){
                            dayOffVO.init(dayOffNode.id, dayOffNode.startMinute, dayOffNode.endMinute,
                            dayOffNode.startDate, dayOffNode.endDate, dayOffNode.reasonName, dayOffNode.reasonShortcut);
					}else{
                        dayOffVO = DayOffVOFactory.GetVO(dayOffNode);
                        dayOffCollection.addItem(dayOffVO);
                    }
				}
                if(dayOffVO){
                    employeesCollectionToUpdate.push(employeeVO);
                }
			}
            callFunctionLater(new ActionData(sendNotification, this, [ApplicationCommands.OFF_DAYS_REFRESHED, employeesCollectionToUpdate], 1, -1, "Days Off Refreshing..."));
            System.disposeXML(xml);
			
		}
	}
}
package pl.pkpik.epdp7.eventtimeline.controller.command
{

	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
    import pl.pkpik.epdp7.eventtimeline.model.factory.PlannerSolutionDataFactory;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.EventsDataProxy;

    public class SetEventsDataCommand extends SimpleCommand
	{
		override public function execute( note:INotification ) :void
		{
            (facade.retrieveProxy(EventsDataProxy.NAME) as EventsDataProxy).setData(PlannerSolutionDataFactory.GetPlannerSolution(note.getBody() as XML));

		}
		
	}
}
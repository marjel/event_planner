package pl.pkpik.epdp7.eventtimeline.controller.command {
    import flash.external.ExternalInterface;

    import mx.core.FlexGlobals;

    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.command.SimpleCommand;

    import pl.pkpik.epdp7.eventtimeline.model.helper.SelectionActionDataParser;

    import pl.pkpik.epdp7.eventtimeline.view.vo.SelectedItemObject;

    import spark.components.Application;

    public class SelectObjectCommand extends SimpleCommand {
        override public function execute( note:INotification ) :void
        {


            var actionData:SelectedItemObject = note.getBody() as SelectedItemObject;

            if (ExternalInterface.available) {
                var _pid:String = Application(FlexGlobals.topLevelApplication).parameters["pid"];
                var selectedObjArray:Array = SelectionActionDataParser.GetSelectionData(actionData.data);
                if (_pid == "X") {
                    ExternalInterface.call("selectObject",
                            actionData.type, selectedObjArray);
                } else {
                    ExternalInterface.call("vaadin.workersSchedule['selectObject_" + _pid + "']",
                            actionData.type, selectedObjArray);
                }
            } else {
                //	Alert.show("You clicked on the menu item '" +
                //event.currentTarget.caption + "'");
            }

        }
    }
}

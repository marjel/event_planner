package pl.pkpik.epdp7.eventtimeline.controller.command
{

	import flash.system.System;

	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
	import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
	import pl.pkpik.epdp7.eventtimeline.model.factory.FreeDayVOFactory;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.EventsDataProxy;
    import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;
    import pl.pkpik.epdp7.eventtimeline.model.vo.FreeDayVO;
	
	public class RefreshFreeDaysCommand extends SimpleCommand
	{

		override public function execute( note:INotification ) :void
		{
			
			var edp:EventsDataProxy = facade.retrieveProxy(EventsDataProxy.NAME) as EventsDataProxy;

			var xml:XML = XML(note.getBody());
			var list:XMLList = xml..EmployeeIdHolder as XMLList;
			var xmlNode:XML;
			var freeDayList:XMLList;
			var freeDayNode:XML;
			var freeDayVO:FreeDayVO;
			var employeeVO:EmployeeVO;
			for each(xmlNode in list){

                employeeVO = edp.getEmployeeVOById(int(xmlNode.id));
				freeDayList = xmlNode..FreeDay as XMLList;
				for each(freeDayNode in freeDayList){
					freeDayVO = edp.getFreeDayVOById(freeDayNode.id);
					if(freeDayVO){
						if(freeDayNode.isAssigned != undefined)
							freeDayVO.isAssigned = (freeDayNode.isAssigned == "true") ? true : false;
						if(freeDayNode.reason != undefined)
							freeDayVO.reason = freeDayNode.reason;
						if(freeDayNode.isBlocked != undefined)
							freeDayVO.isBlocked = (freeDayNode.isBlocked == "true") ? true : false;
						if(freeDayNode.assignedDate != undefined)
							freeDayVO.assignedDte = freeDayNode.assignedDate;
						if(freeDayNode.date != undefined)
							freeDayVO.dte = freeDayNode.date;
						if(freeDayNode.assignedDateStartMinute != undefined)
							freeDayVO.assignedDateStartMinute = freeDayNode.assignedDateStartMinute;
					}else{
                        if(employeeVO){
                            freeDayVO = FreeDayVOFactory.GetVO(freeDayNode);
                            employeeVO.freeDays.addItem(freeDayVO);
                        }

                    }

				}
				
			}


			callFunctionLater(new ActionData(sendNotification, this, [ApplicationCommands.FREE_DAYS_REFRESHED], 1, -1, "Free Days Refreshing..."));
			System.disposeXML(xml);
		}
	}
}
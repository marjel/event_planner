package pl.pkpik.epdp7.eventtimeline.controller.command
{

	import flash.system.System;

	import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
	import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;
	import pl.pkpik.epdp7.eventtimeline.model.proxy.EventsDataProxy;
	import pl.pkpik.epdp7.eventtimeline.model.vo.EmployeeVO;
	import pl.pkpik.epdp7.eventtimeline.model.vo.MeasurementVO;
	import pl.pkpik.epdp7.eventtimeline.model.vo.ShiftDateVO;
	
	public class SetMeasurementCommand extends SimpleCommand
	{

		override public function execute( note:INotification ) :void
		{
			
			var xml:XML = note.getBody() as XML;
			var edp:EventsDataProxy = facade.retrieveProxy(EventsDataProxy.NAME) as EventsDataProxy;
			var xmlList:XMLList = xml..DateShiftMeasurment;
			var xmlType:XML;
			var valueList:XMLList;
			var xmlItem:XML;
			var shiftDateVO:ShiftDateVO;
			var employeeVO:EmployeeVO;
			var measurementVO:MeasurementVO;
			
			var shiftDates:VectorCollection = edp.eventsData.shiftDates;
			var employees:VectorCollection = edp.eventsData.employees;
			
			for each(var sd:ShiftDateVO in shiftDates){
				sd.measurements.removeAll();
			}
			
			for each(var e:EmployeeVO in employees){
				e.measurements.removeAll();
			}
			
			for each(xmlType in xmlList){
				valueList = xmlType..entry;
				for each(xmlItem in valueList){
					shiftDateVO = edp.getShiftDateVOById(xmlItem.ShiftDateId);
					if(shiftDateVO){
						measurementVO = new MeasurementVO();
						measurementVO.color = int(xmlType.Color);
						measurementVO.shortcut = xmlType.ShortName || "";
						measurementVO.name = xmlType.Name;
						measurementVO.value = xmlItem.Value;
						shiftDateVO.measurements.addItem(measurementVO);
					}
				}
			}
			
			xmlList = xml..EmployeeMeasurment;
			for each(xmlType in xmlList){
				valueList = xmlType..entry;
				for each(xmlItem in valueList){
					
					if(String(xmlItem.EmployeeId).length>0){
						employeeVO = edp.getEmployeeVOById(parseInt(xmlItem.EmployeeId));
						if(employeeVO){
							measurementVO = new MeasurementVO();
							measurementVO.color = ((xmlItem.DisplayInfo!=undefined) && !isNaN(xmlItem.DisplayInfo.BackgroundColor))
                                    ? int(xmlItem.DisplayInfo.BackgroundColor) : int(xmlType.Color);
							measurementVO.shortcut = xmlType.ShortName;
							measurementVO.name = xmlType.Name;
							measurementVO.value = xmlItem.Value;
                            employeeVO.measurements.addItem(measurementVO);

						}
					}
				}
			}
			System.disposeXML(xml);
			callFunctionLater(new ActionData(sendNotification, this, [ApplicationCommands.MEASUREMENT_ALREADY_SET], 1, -1, "Measurement Data Refreshing..."));
		}
	}
}







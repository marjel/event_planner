package pl.pkpik.epdp7.eventtimeline.controller.command
{

    import flash.system.System;

    import org.apache.flex.collections.VectorCollection;
    import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;

    import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;
import pl.pkpik.epdp7.eventtimeline.controller.vo.ActionData;

import pl.pkpik.epdp7.eventtimeline.model.proxy.BrokenRulesDataProxy;
    import pl.pkpik.epdp7.eventtimeline.model.vo.BrokenRuleVO;

    public class SetBrokenRulesCommand extends SimpleCommand
	{

		override public function execute( note:INotification ) :void
		{
            var vecDataCollection:VectorCollection = new VectorCollection(new Vector.<BrokenRuleVO>());
            (facade.retrieveProxy(BrokenRulesDataProxy.NAME) as BrokenRulesDataProxy).setData(vecDataCollection);
            var dataItem:BrokenRuleVO;
			var xml:XML = (note.getBody() as XML);
			var list:XMLList = xml..BrokenRuleShort as XMLList;
			var xmlNode:XML;
            var insideList:XMLList;
            var idNode:XML;
            for each(xmlNode in list){
                dataItem = new BrokenRuleVO();
                dataItem.name = xmlNode.name;
                dataItem.id = int(xmlNode.id);
                dataItem.weight = xmlNode.weight;
                dataItem.isHard = xmlNode.isHard;
                insideList = xmlNode.employeeList.long;
                if(insideList != null && insideList.length()){
                    for each(idNode in insideList){
                        dataItem.employeeList.addItem(int(idNode));
                    }
                }
                insideList = xmlNode.shiftAssignmentList.long;
                if(insideList != null && insideList.length()){
                    for each(idNode in insideList){
                        dataItem.shiftAssignmentList.addItem(int(idNode));
                    }
                }
                vecDataCollection.addItem(dataItem);

            }
            System.disposeXML(xml);
            callFunctionLater(new ActionData(sendNotification, this, [ApplicationCommands.BROKEN_RULES_ALREADY_SET], 1, -1, "Broken Rules Refreshing..."));
		}
	}
}
package pl.pkpik.epdp7.eventtimeline.controller.command {

    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.command.SimpleCommand;
    import pl.pkpik.epdp7.eventtimeline.model.factory.AdditionalPlannerSolutionDataFactory;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.AdditionalEventsDataProxy;

    public class SetAdditionalEventsDataCommand extends SimpleCommand
    {
        override public function execute( note:INotification ) :void
        {
            var edp:AdditionalEventsDataProxy = facade.retrieveProxy(AdditionalEventsDataProxy.NAME) as AdditionalEventsDataProxy;
            if(edp.eventsCollection.length<2){
                edp.eventsCollection.addItem(AdditionalPlannerSolutionDataFactory.GetVO(note.getBody() as XML));
            }

        }
    }
}

package pl.pkpik.epdp7.eventtimeline.controller.command
{
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import pl.pkpik.epdp7.eventtimeline.view.mediator.ApplicationMediator;
	
	public final class ViewPrepareCommand extends SimpleCommand
	{
		override public function execute( note:INotification ):void    
		{
			facade.registerMediator( new ApplicationMediator( note.getBody() as EventPlanner ) );
		}
	}
}
package pl.pkpik.epdp7.eventtimeline.controller.command {
import flash.external.ExternalInterface;

import mx.core.FlexGlobals;

import org.puremvc.as3.interfaces.INotification;
import org.puremvc.as3.patterns.command.SimpleCommand;

import spark.components.Application;

public class LoadBrokenRulesCommand extends SimpleCommand {
    override public function execute( note:INotification ) :void
    {
        if (ExternalInterface.available) {
            var _pid:String = Application(FlexGlobals.topLevelApplication).parameters["pid"];
            if(_pid == "X"){
                ExternalInterface.call("setBrokenRules");
            }else{
                ExternalInterface.call("vaadin.workersSchedule['setBrokenRules" + _pid + "']");
            }
        }
    }
}
}

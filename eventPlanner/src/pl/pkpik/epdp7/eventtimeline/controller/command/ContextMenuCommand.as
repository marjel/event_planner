package pl.pkpik.epdp7.eventtimeline.controller.command
{
	import flash.external.ExternalInterface;

    import mx.core.FlexGlobals;

    import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;

    import pl.pkpik.epdp7.eventtimeline.controller.constant.ApplicationCommands;

    import pl.pkpik.epdp7.eventtimeline.view.vo.ContextMenuVO;

    import spark.components.Application;

    //import spark.components.Alert;
	
	public class ContextMenuCommand extends SimpleCommand
	{
		override public function execute( note:INotification ) :void    
		{


            var actionData:ContextMenuVO = note.getBody() as ContextMenuVO;

            if (ExternalInterface.available) {
                var _pid:String = Application(FlexGlobals.topLevelApplication).parameters["pid"];
                var l:uint = (actionData.selections == null) ? 0 : actionData.selections.length;
                if(actionData.type=="version"){
                    return;
                }
                if(actionData.type=="global"){
                    sendNotification(ApplicationCommands.RUN_APPLICATION_BUTTON_COMMAND);
                    return;
                }
                if (_pid == "X") {
                    ExternalInterface.call("contextMenuSelect",
                            actionData.type, actionData.name, l ? actionData.selections : actionData.data);
                } else {
                    ExternalInterface.call("vaadin.workersSchedule['contextMenuSelect_" + _pid + "']",
                            actionData.type, actionData.name, l ? actionData.selections : actionData.data);
                }
            } else {
                //	Alert.show("You clicked on the menu item '" +
                //event.currentTarget.caption + "'");
            }
			
		}
	}
}
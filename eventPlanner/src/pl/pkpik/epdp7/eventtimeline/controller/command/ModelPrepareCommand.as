package pl.pkpik.epdp7.eventtimeline.controller.command
{
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;

    import pl.pkpik.epdp7.eventtimeline.model.proxy.BrokenRulesDataProxy;

    import pl.pkpik.epdp7.eventtimeline.model.proxy.ConfigProxy;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.ContextMenuDataProxy;
    import pl.pkpik.epdp7.eventtimeline.model.proxy.EventsDataProxy;
	import pl.pkpik.epdp7.eventtimeline.model.proxy.LanguagesProxy;
	import pl.pkpik.epdp7.eventtimeline.model.proxy.LocaleProxy;
	import pl.pkpik.epdp7.eventtimeline.model.proxy.ResourcesMonitorProxy;
	import pl.pkpik.epdp7.eventtimeline.model.proxy.AdditionalEventsDataProxy;

	public class ModelPrepareCommand extends SimpleCommand
	{
		override public function execute( note:INotification ):void
		{
			facade.registerProxy(new ResourcesMonitorProxy());
			facade.registerProxy(new LanguagesProxy());
			facade.registerProxy(new ConfigProxy());
			facade.registerProxy(new LocaleProxy());
			facade.registerProxy(new EventsDataProxy());
			facade.registerProxy(new AdditionalEventsDataProxy());
            facade.registerProxy(new BrokenRulesDataProxy());
            facade.registerProxy(new ContextMenuDataProxy())

		}
	}
}
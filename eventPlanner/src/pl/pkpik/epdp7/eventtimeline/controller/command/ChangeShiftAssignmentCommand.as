package pl.pkpik.epdp7.eventtimeline.controller.command
{
	import flash.external.ExternalInterface;
	
	import mx.core.FlexGlobals;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	import pl.pkpik.epdp7.eventtimeline.view.component.ShiftRectangle;
	
	import spark.components.Application;
	
	public class ChangeShiftAssignmentCommand extends SimpleCommand
	{
		override public function execute( note:INotification ) :void    
		{
			var rect:ShiftRectangle = note.getBody() as ShiftRectangle;
			var _pid:String = Application(FlexGlobals.topLevelApplication).parameters["pid"];	
			if (_pid) {
				if (ExternalInterface.available) {
					if(_pid == "X"){
						ExternalInterface.call("changeShiftAssignment", rect.data.id, rect.employee ? rect.employee.id : "null");
					}else{
						ExternalInterface.call("vaadin.workersSchedule['changeShiftAssignment_" + _pid + "']", rect.data.id, rect.employee ? rect.employee.id : "null");
					}
				}
			} 
		}
	}
}
package pl.pkpik.epdp7.eventtimeline.controller.constant
{
	public class ApplicationCommands
	{
		
		public static const OPEN_POPUP:String = "OpenPopup";
		public static const STARTUP:String 	= "Startup";
		
		public static const ENABLE_APPLICATION:String = "EnableApplication";

		public static const REFRESH_DATA:String = "RefreshData";
		public static const DATA_REFRESHED:String = "DataRefreshed";

        public static const REFRESH_FREE_DAYS:String = "RefreshFreeDays";
		public static const FREE_DAYS_REFRESHED:String = "FreeDaysRefreshed";

        public static const REFRESH_OFF_DAYS:String = "RefreshOffDays";
        public static const OFF_DAYS_REFRESHED:String = "OffDaysRefreshed";
		
		public static const SET_MEASUREMENT:String = "SetMeasurement";
		public static const MEASUREMENT_ALREADY_SET:String = "MeasurementAlreadySet";
			
		public static const CHANGE_SHIFT_ASSIGNMENT:String = "ChangeShiftAssignment";
		public static const SET_SHIFT_ASSIGNMENT:String = "SetShiftAssignment";

		public static const SET_EVENTS_DATA:String = "SetEventsData";
		public static const EVENTS_DATA_LOAD:String = "EventsDataLoad";
		public static const EVENTS_DATA_LOADED:String = "EventsDataLoaded";

        public static const SET_ADDITIONAL_EVENTS_DATA:String = "SetAdditionalEventsData";
        public static const ADDITIONAL_EVENTS_DATA_LOADED:String = "AdditionalEventsDataLoaded";

		public static const EXTERNAL_INTERFACE_INIT:String = "ExternalInterfaceInit";
		
		public static const EXECUTE_CONTEXT_MENU:String = "ExecuteContextMenu";

        public static const ZOOM_VIEW:String = "ZoomView";

        public static const SHOW_EMPLOYEE_MEASUREMENT:String = "ShowEmployeeMeasurement";
        public static const HIDE_EMPLOYEE_MEASUREMENT:String = "HideEmployeeMeasurement";

        public static const SHOW_DATE_MEASUREMENT:String = "ShowDateMeasurement";
        public static const HIDE_DATE_MEASUREMENT:String = "HideDateMeasurement";

        public static const SHOW_NOTIFICATION:String = "ShowNotification";

        public static const SET_CONTEXT_MENU:String = "SetContextMenu";

        public static const EXPAND_UNASSIGNED_SHIFTS_PANEL:String = "ExpandUnassignedShiftsPanel";
        public static const COLLAPSE_UNASSIGNED_SHIFTS_PANEL:String = "CollapseUnassignedShiftsPanel";

        public static const EXPAND_VIRTUAL_EMPLOYEE_PANEL:String = "ExpandVirtualEmployeePanel";
        public static const COLLAPSE_VIRTUAL_EMPLOYEE_PANEL:String = "CollapseVirtualEmployeePanel";

        public static const SCROLL_TO_SHIFT:String = "ScrollToShift";
        public static const SCROLL_TO_DATE:String = "ScrollToDate";
        public static const SCROLL_TO_EMPLOYEE:String = "ScrollToEmployee";

        public static const CHANGE_ZOOM:String = "ChangeZoom";

        public static const SELECT_OBJECT:String = "SelectObject";

        public static const LOAD_BROKEN_RULES:String = "LoadBrokenRules";
        public static const SET_BROKEN_RULES:String = "SetBrokenRules";
        public static const BROKEN_RULES_ALREADY_SET:String = "BrokenRulesAlreadySet";
        public static const NAVIGATE_TO_BROKEN_RULE:String = "NavigateToBrokenRule";
        public static const HIDE_BROKEN_RULES:String = "HideBrokenRules";
        public static const SHOW_BROKEN_RULES:String = "ShowBrokenRules";
        public static const ENABLE_BROKEN_RULES_BUTTON:String = "EnableBrokenRulesButton";


        public static const UI_REFRESHED:String = "UIRefreshed";

        public static const RUN_APPLICATION_BUTTON_COMMAND:String = "RunApplicationButtonCommand";
        public static const QUEUE_EXECUTED:String = "QueueExecuted";

        public static const SHOW_DATA_PROCESSING_INFO:String = "ShowDataProcessingInfo";
        public static const HIDE_DATA_PROCESSING_INFO:String = "HideDataProcessingInfo";

        public static const ACTIONS_START_EXECUTE:String = "ActionStartExecute";
        public static const ACTION_EXECUTED:String = "ActionExecuted";
        public static const ACTIONS_STOP_EXECUTE:String = "ActionsStopExecuted";

	}
}


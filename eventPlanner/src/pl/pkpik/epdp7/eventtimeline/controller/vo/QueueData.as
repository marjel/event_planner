package pl.pkpik.epdp7.eventtimeline.controller.vo {

    public final class QueueData{

        private var _scope:Object;
        private var _action:Function;
        private var _data:String;
        private var _id:int;

        function QueueData(o:Object=null, a:Function=null, d:String=null, i:int=-1)
        {
            _scope = o;
            _action = a;
            _data = d;
            _id = i;
        }

        public function get action():Function
        {
            return _action;
        }

        public function get data():String
        {
            return _data;
        }
        public function get scope():Object {
            return _scope;
        }
        public function get isFake():Boolean{
            return _scope==null;
        }

        public function toString():String {
            return "QueueData{_scope=" + String(_scope) + ",_action=" + String(_action) + ",_data=" + String(_data.length) + "}";
        }

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }
    }
}

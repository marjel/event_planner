package pl.pkpik.epdp7.eventtimeline.controller.vo {

    public final class ActionData {

        private var _index:int;
        private var _scope:*;
        private var _action:Function;
        private var _arguments:Array;
        private var _counter:int;
        private var _description:String=null;
        private var injectedCounter:int;
        private var _isWaiting:Boolean = false;


        public function get notyficationBody():ActionData {
            var data:ActionData =  new ActionData();
            return data.init.call(data, this.action, this.scope, this.arguments, this.injectedCounter, this.index, this._description);
        }

        public function get isWaiting():Boolean {
            return _isWaiting;
        }

        public function set isWaiting(value:Boolean):void {
            _isWaiting = value;
        }

        public function ActionData(f:Function=null, o:*=null, a:Array=null, c:uint=1, i:int=-1, d:String=null)
        {
            if(arguments.length){
                this.init.apply(this, arguments);
            }
        }

        public function init(f:Function, o:*=null, a:Array=null, c:uint=1, i:int=-1, d:String=null):ActionData
        {
            _scope = o;
            _action = f;
            _arguments = a;
            injectedCounter = _counter = c;
            _index = i;
            _description = d;
            return this;
        }
        public function get action():Function
        {
            return _action;
        }

        public function get arguments():Array
        {
            return _arguments;
        }
        public function get scope():* {
            return _scope;
        }

        public function get counter():int {
            return _counter;
        }

        public function set counter(value:int):void {
            _counter = value;
        }
        public function toString():String {
            return "ActionData{index=" + String(index) + ",_scope=" + String(_scope) + ",_arguments=" + String(_arguments) + ",_counter=" + String(_counter) + ",_isWaiting=" + String(_isWaiting) + "}";
        }

        public function get index():int {
            return _index;
        }

        public function set index(value:int):void {
            _index = value;
        }

        public function get description():String {
            return _description || scope.toString();
        }

        public function set description(value:String):void {
            _description = value;
        }
    }
}

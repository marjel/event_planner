package pl.pkpik.epdp7.eventtimeline.controller.event {
    import flash.events.Event;

    public class SelectionEvent extends Event {

        public var data:Object;

        public static const CHANGE_SELECTION:String = "ChangeSelection";

        public function SelectionEvent(type:String, data:Object, bubbles:Boolean = false,cancelable:Boolean = false)
        {
            this.data = data;
            super(type, bubbles, cancelable);
        }
        override public function clone():Event
        {
            return new SelectionEvent(type, data, bubbles, cancelable);
        }
    }
}

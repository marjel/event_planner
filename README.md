# README #

### Repository short description ###

* RIA Erp customizable  application
* Apache Flex 4.14
* For test built in IntelliJ IDEA env
* version 2.00


### Website link ###

Have a look: [Test Web Version of Event Planner](http://sh189254.website.pl/planner/)

*Probably it is not a final version of this project - so please pay attention on Your browser's cached data (in current test version a part of the data is saved on a local machine)*

## Exampleas of javascript commands sets: ##
### *Please use Your browser's javascript console* ###


```
#!javascript

* testQueue();
* testShortData();
* testRefresh();
* testBug();
```


### The other javascript commands: ###


```
#!javascript

* scrollToDate(id); /*date VO id*/
* scrollToEmployee(id); /*employee VO id*/
* scrollToShift(id); /*shift VO id*/
* enableApplication(value) /*true || false*/
```


## Need more details? ##
### contact via email with the repo owner - project architect, designer and developer: ###
* [marjel](mailto:marjel73@gmail.com) [marjel73@gmail.com]